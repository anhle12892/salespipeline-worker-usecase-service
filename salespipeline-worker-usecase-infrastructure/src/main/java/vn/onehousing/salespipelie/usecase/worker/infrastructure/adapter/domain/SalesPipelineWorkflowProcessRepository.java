package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain;

import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain.mapper.ISalesPipelineWorkflowProcessMapper;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection.IMongoSalesPipelineWorkflowInstanceRepository;

//@ReadingConverter
@Repository
@AllArgsConstructor
public class SalesPipelineWorkflowProcessRepository implements ISalesPipelineWorkflowProcessRepository {

    private final IMongoSalesPipelineWorkflowInstanceRepository repository;
    private final ISalesPipelineWorkflowProcessMapper mapper;

    @Override
    public SalesPipelineWorkflowInstance create(SalesPipelineWorkflowInstance data) {
        var resp = repository.insert(mapper.to(data));
        return mapper.from(resp);
    }

    @Override
    public SalesPipelineWorkflowInstance update(SalesPipelineWorkflowInstance data) {
        var resp = repository.save(mapper.to(data));
        return mapper.from(resp);
    }

    @Override
    public SalesPipelineWorkflowInstance getByLeadUuId(String leadUuId) {
        var resp = repository.getByLeadUuid(leadUuId);
        return mapper.from(resp);
    }

    @Override
    public SalesPipelineWorkflowInstance getByOpportunityUuid(String opportunityUuId) {
        var resp = repository.getByOpportunityUuid(opportunityUuId);
        return mapper.from(resp);
    }

    @Override
    public SalesPipelineWorkflowInstance getByProcessInstanceId(String processInstanceId) {
        var res = repository.getByProcessInstanceId(processInstanceId);
        return mapper.from(res);
    }

    @Override
    public SalesPipelineWorkflowInstance getById(String id) {
        var resp = repository.getById(new ObjectId(id));
        return mapper.from(resp);
    }

}
