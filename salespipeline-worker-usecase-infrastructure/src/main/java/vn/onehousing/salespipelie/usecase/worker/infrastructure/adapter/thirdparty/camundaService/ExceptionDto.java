package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.camundaService;

import lombok.Data;

@Data
public class ExceptionDto {
    protected String type;
    protected String message;
}
