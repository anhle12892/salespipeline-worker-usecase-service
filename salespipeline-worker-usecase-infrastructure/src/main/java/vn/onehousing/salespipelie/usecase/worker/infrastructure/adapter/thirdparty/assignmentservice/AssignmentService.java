package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.assignmentservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.IAssignmentService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.request.AssignRequest;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.AssignResponse;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.RevokedDataDto;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.RevokedResponse;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.UserDto;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;
import java.util.List;

@Service
@Slf4j
public class AssignmentService implements IAssignmentService {

    private final WebClient webClient;
    private final ObjectMapper objectMapper;

    public AssignmentService(@Lazy WebClient assignmentServiceClient, ObjectMapper objectMapper) {
        this.webClient = assignmentServiceClient;
        this.objectMapper = objectMapper;
    }

//    @Override
//    public UserDto assign(List<String> policyNames, Map<String, Object> data) throws JsonProcessingException {
//        String path = "/v1/assignments/assign";
//        data.put(VariableName.POLICY_NAMES, policyNames);
//        log.info("AssignmentService - " + objectMapper.writeValueAsString(data));
//        Mono<AssignResponse> monoResp = this.webClient.post()
//            .uri(path).body(Mono.just(data), Map.class)
//            .exchangeToMono(clientResponse -> clientResponse.bodyToMono(AssignResponse.class))
//            .doOnError(
//                error -> log.error("AssignmentService - assign - error", error)
//            );
//        AssignResponse resp = monoResp.block();
//        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
//            throw new RequestThirdPartyException("AssignmentService", "assign", resp.getMeta().toString());
//        }
//        return resp.getData();
//    }

    @Override
    public List<RevokedDataDto> revoke(String leadUUid) {
        try {
            String path = "/v1/leads?lead_uuid=" + leadUUid;
            Mono<RevokedResponse> monoResp = this.webClient.get()
                .uri(path)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(RevokedResponse.class))
                .doOnError(
                    error -> log.error("AssignmentService - revoked - error", error)
                );
            RevokedResponse resp = monoResp.block();
            if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
                log.error("AssignmentService lead_uuid: {} - revoked - error", leadUUid, resp.getMeta().toString());
            }
            return resp.getData();
        } catch (Exception ex) {
            log.error("AssignmentService lead_uuid: {} - revoked - error", leadUUid, ex);
        }
        return null;
    }

    @Override
    public UserDto getAgent(String assignedUserUuid) {
        try {
            String path = "/v1/assignments/agents/" + assignedUserUuid;
            Mono<AssignResponse> monoResp = this.webClient.get()
                .uri(path)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(AssignResponse.class))
                .doOnError(
                    error -> log.error("AssignmentService - getAgent - error", error)
                );
            AssignResponse resp = monoResp.block();
            if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
                log.error("AssignmentService: getAgent - error {}", resp.getMeta().toString());
            }
            return resp.getData();
        } catch (Exception ex) {
            log.error("AssignmentService - getAgent - error", ex);
        }
        return null;
    }

    @Override
    public UserDto getRolesByUserUuid(String userUuid) {
        try {
            String path = "/v1/assignments/users/" + userUuid;
            Mono<AssignResponse> monoResp = this.webClient.get()
                    .uri(path)
                    .exchangeToMono(clientResponse -> clientResponse.bodyToMono(AssignResponse.class))
                    .doOnError(
                            error -> log.error("AssignmentService - getAgent - error", error)
                    );
            AssignResponse resp = monoResp.block();
            if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
                log.error("AssignmentService: getRolesByUserUuid - error {}", resp.getMeta().toString());
            }
            log.info("[getRolesByUserUuid] successful {}", userUuid);
            return resp.getData();
        } catch (Exception ex) {
            log.error("AssignmentService - getRolesByUserUuid - error", ex);
        }
        return null;
    }

    @Override
    public UserDto assign(AssignRequest request) throws JsonProcessingException {
        String path = "/v1/assignments/assign";
//        log.info("AssignmentService - assign - " + objectMapper.writeValueAsString(request));
        Mono<AssignResponse> monoResp = this.webClient.post()
                .uri(path).body(Mono.just(request), AssignRequest.class)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(AssignResponse.class))
                .doOnError(
                        error -> {
                            log.error("AssignmentService.assign - lead_uuid: [{}] - error", request.getLeadUuid(), error);
                        }
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        AssignResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdPartyException("AssignmentService", "assign", resp.getMeta().toString());
        }
        return resp.getData();
    }

    @Override
    public UserDto assignMultiUser(AssignRequest request) throws JsonProcessingException {
        String path = "/v1/assignments/assign-multiple";
//        log.info("AssignmentService - assign - " + objectMapper.writeValueAsString(request));
        Mono<AssignResponse> monoResp = this.webClient.post()
                .uri(path).body(Mono.just(request), AssignRequest.class)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(AssignResponse.class))
                .doOnError(
                        error -> {
                            log.error("AssignmentService.assign - lead_uuid: [{}] - error", request.getLeadUuid(), error);
                        }
                )
                .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        AssignResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode())) {
            throw new RequestThirdPartyException("AssignmentService", "assign", resp.getMeta().toString());
        }
        return resp.getData();
    }
}
