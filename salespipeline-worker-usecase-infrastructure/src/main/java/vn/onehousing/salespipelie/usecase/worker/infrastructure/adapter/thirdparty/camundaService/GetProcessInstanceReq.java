package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.camundaService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
public class GetProcessInstanceReq {
    public String businessKey;
    public List<Variable> variables;
    public List<Sorting> sorting;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Variable{
    public String name;
    public String operator;
    public String value;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Sorting{
    public String sortBy;
    public String sortOrder;
}