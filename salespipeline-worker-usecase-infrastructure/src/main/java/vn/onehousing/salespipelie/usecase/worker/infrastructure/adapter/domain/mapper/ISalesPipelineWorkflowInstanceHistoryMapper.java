package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstanceHistory;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection.MongoSalesPipelineWorkflowInstanceHistory;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.mapper.IObjectIdMapper;

@Mapper(
        componentModel = "spring",
        uses = {
                IObjectIdMapper.class
        }
)
public interface ISalesPipelineWorkflowInstanceHistoryMapper {
    SalesPipelineWorkflowInstanceHistory from(MongoSalesPipelineWorkflowInstanceHistory data);

    MongoSalesPipelineWorkflowInstanceHistory to(SalesPipelineWorkflowInstanceHistory data);
}
