package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.ISalesPipelineService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request.CreateSalesPipelineReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request.UpdateSalesPipelineReq;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;

@Slf4j
@Service
public class SalesPipelineService implements ISalesPipelineService {

    private final WebClient webClient;

    public SalesPipelineService(@Lazy WebClient salesPipelineServiceClient) {
        this.webClient = salesPipelineServiceClient;
    }

    @Override
    public SalesPipeline create(CreateSalesPipelineReq req) {
        Mono<CreateSalesPipelineBaseResp> monoResp = this.webClient
            .post()
            .uri("/v1/salespipelines")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), CreateSalesPipelineReq.class)
            .exchangeToMono(response -> {
                return response.bodyToMono(CreateSalesPipelineBaseResp.class);
            })
            .doOnError(
                error -> log.error("SalesPipelineService - update - lead_uuid: [{}] - error ", req.getLeadUuid(), error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        CreateSalesPipelineBaseResp resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineService", "create", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public void update(String salespipelineUuid, UpdateSalesPipelineReq req) {
        Mono<BaseResponse> monoResp = this.webClient
            .put()
            .uri("/v1/salespipelines/" + salespipelineUuid)
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), UpdateSalesPipelineReq.class)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponse.class);
            })
            .doOnError(
                error -> log.error("SalesPipelineService - update - sales_pipeline_uuid: [{}] - error", salespipelineUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        BaseResponse resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineService", "update", resp.getMeta().toString());

        return;
    }
}
