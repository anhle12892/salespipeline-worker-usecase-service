package vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.mapper;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.mapstruct.Mapper;

import java.util.Objects;

@Mapper(componentModel = "spring")
public interface IObjectIdMapper {
    default ObjectId from(String id) {
        return StringUtils.isEmpty(id) ? new ObjectId() : new ObjectId(id);
    }

    default String to(ObjectId id) {
        return Objects.isNull(id) ? null : id.toString();
    }
}
