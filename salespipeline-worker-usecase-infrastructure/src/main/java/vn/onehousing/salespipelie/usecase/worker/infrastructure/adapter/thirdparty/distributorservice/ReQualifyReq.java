package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.distributorservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReQualifyReq {
    @JsonProperty("leadIdentity")
    String leadIdentity;

    @JsonProperty("dropStatus")
    String dropStatus;
}
