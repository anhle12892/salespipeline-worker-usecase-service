package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineService;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalespipelineAggregateView;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

public class SalespipelineAggregateViewResponse extends BaseResponse<SalespipelineAggregateView> {
}
