package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.opportunityService;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.Opportunity;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

public class OpportunityBaseResponse extends BaseResponse<Opportunity> {
}
