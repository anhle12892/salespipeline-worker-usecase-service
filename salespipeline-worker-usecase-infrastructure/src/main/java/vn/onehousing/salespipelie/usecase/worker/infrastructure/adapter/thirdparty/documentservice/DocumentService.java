package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.documentservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import vn.onehousing.common.elasticsearch.service.RestHighLevelClientWrapper;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.documentservice.IDocumentService;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Note;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DocumentService implements IDocumentService {

    private final RestHighLevelClientWrapper documentServiceEsClient;
    private final ObjectMapper objectMapper;

    public DocumentService(@Lazy RestHighLevelClientWrapper documentServiceEsClient, ObjectMapper objectMapper) {
        this.documentServiceEsClient = documentServiceEsClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<List<Note>> getNotesBy(List<String> parentUUIDs, String parentType) throws IOException {
        QueryBuilder parentUUIDQueryBuilder = QueryBuilders.termsQuery("parent_uuid", parentUUIDs);
        TermQueryBuilder parentTypeQueryBuilde = new TermQueryBuilder("parent_type", parentType);

        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(parentUUIDQueryBuilder);
        query.must(parentTypeQueryBuilde);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = documentServiceEsClient.getRestHighLevelClient().search(searchRequest, RequestOptions.DEFAULT);
        if (searchResponse.getHits() != null
                && searchResponse.getHits().getHits() != null
                && searchResponse.getHits().getHits().length > 0) {
            log.info("DocumentServiceClient.getNotesBy - lead_uuid: [{}] - note size: [{}]", parentUUIDs, searchResponse.getHits().getHits().length);
            List<Note> notes = new ArrayList<>();
            for (SearchHit hit : searchResponse.getHits().getHits()) {
                Note note = objectMapper.readValue(hit.getSourceAsString(), Note.class);
                notes.add(note);
            }
            return Optional.of(notes);
        }
        return Optional.empty();
    }
}
