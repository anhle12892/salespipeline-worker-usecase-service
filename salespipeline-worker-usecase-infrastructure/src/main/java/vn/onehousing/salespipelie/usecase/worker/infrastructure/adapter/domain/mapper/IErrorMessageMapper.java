package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ErrorMessage;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection.MongoErrorMessage;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.mapper.IObjectIdMapper;

@Mapper(
        componentModel = "spring",
        uses = {
                IObjectIdMapper.class
        }
)
public interface IErrorMessageMapper {
    ErrorMessage from(MongoErrorMessage data);
    MongoErrorMessage to(ErrorMessage data);
}
