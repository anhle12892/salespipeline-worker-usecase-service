package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineUsecaseService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.CreateAccountResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.CreateNoteReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Note;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalespipelineAggregateView;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineService.SalespipelineAggregateViewResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SalesPipelineUsecaseService implements ISalesPipelineUsecaseService {

    private final WebClient webClient;

    public SalesPipelineUsecaseService(@Lazy WebClient salesPipelineUsecaseServiceClient) {
        this.webClient = salesPipelineUsecaseServiceClient;
    }

    @Override
    public CreateAccountResp createAccount(String leadUuid, String salespipelineUuid) {
        Map<String, String> req = new HashMap();
        req.put("lead_uuid", leadUuid);
        req.put("sales_pipeline_uuid", salespipelineUuid);

        Mono<CreateAccountBasicResp> responseEntityMono = this.webClient
            .post()
            .uri("/v1/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), Map.class)
            .exchangeToMono(response -> response.bodyToMono(CreateAccountBasicResp.class))
            .doOnError(
                error -> log.error("SalesPipelineUsecaseService - createAccount - lead_uuid: [{}] - error ", leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        var resp = responseEntityMono.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineUsecaseService", "createAccount", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public CreateAccountResp createOpportunity(String leadUuid, String salespipelineUuid) {
        Map<String, String> req = new HashMap();
        req.put("lead_uuid", leadUuid);
        req.put("sales_pipeline_uuid", salespipelineUuid);

        Mono<CreateAccountBasicResp> responseEntityMono = this.webClient
            .post()
            .uri("/v1/opportunities")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), Map.class)
            .exchangeToMono(response -> response.bodyToMono(CreateAccountBasicResp.class))
            .doOnError(
                error -> log.error("SalesPipelineUsecaseService - createOpportunity - lead_uuid: [{}] - error ", leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        var resp = responseEntityMono.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineUsecaseService", "createOpportunity", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public SalesPipeline createSalesPipeline(String leadUuid) {
        Map<String, String> req = new HashMap();
        req.put("lead_uuid", leadUuid);

        Mono<CreateSalespipelineResp> responseEntityMono = this.webClient
            .post()
            .uri("/v1/salespipelines")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(req), Map.class)
            .exchangeToMono(response -> response.bodyToMono(CreateSalespipelineResp.class))
            .doOnError(
                error -> log.error("SalesPipelineUsecaseService - createSalesPipeline - lead_uuid: [{}] - error", leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        var resp = responseEntityMono.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineUsecaseService", "createSalesPipeline", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public SalesPipeline updateLead(String leadUuid, UpdateSalesPipelineLeadData data) {

        Mono<CreateSalespipelineResp> responseEntityMono = this.webClient
            .put()
            .uri("/v1/leads/" + leadUuid)
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(data), UpdateSalesPipelineLeadData.class)
            .exchangeToMono(response -> response.bodyToMono(CreateSalespipelineResp.class))
            .doOnError(
                error -> log.error("SalesPipelineUsecaseService - updateLead - lead_uuid: [{}] - error", leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        var resp = responseEntityMono.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineUsecaseService", "updateLead", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Note createNote(String leadUuid, CreateNoteReq data) {
        log.info("[SalesPipelineUsecaseService] create note leadUuid {} with request {}", leadUuid, data);
        Mono<CreateNoteResp> responseEntityMono = this.webClient
            .post()
            .uri("/v1/leads/" + leadUuid + "/notes")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(data), CreateNoteReq.class)
            .exchangeToMono(response -> response.bodyToMono(CreateNoteResp.class))
            .doOnError(
                error -> log.error("SalesPipelineUsecaseService - createNote - lead_uuid: [{}] - error", leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        var resp = responseEntityMono.block();

        log.info("[SalesPipelineUsecaseService] create note leadUuid {} with result {}", leadUuid, resp);

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineUsecaseService", "createNote", resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public SalespipelineAggregateView getByLeadUuid(String leadUuid) {
        Mono<SalespipelineAggregateViewResponse> monoResp = this.webClient.get()
            .uri("/v1/leads/" + leadUuid)
            .retrieve()
            .bodyToMono(SalespipelineAggregateViewResponse.class)
            .doOnError(
                error -> log.error("SalesPipelineService - getByLeadUuid - lead_uuid: [{}] - error ", leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(10, Duration.ofSeconds(1)));

        SalespipelineAggregateViewResponse resp = monoResp.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("SalesPipelineService", "getByLeadUuid", resp.getMeta().toString());

        return resp.getData();
    }
}
