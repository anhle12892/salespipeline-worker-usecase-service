package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.opportunityService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.IOpportunityService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.request.UpdateOpportunityReq;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Opportunity;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;

@Slf4j
@Service
public class OpportunityService implements IOpportunityService {
    private final WebClient webClient;

    public OpportunityService(@Lazy WebClient opportunityServiceClient) {
        this.webClient = opportunityServiceClient;
    }

    @Override
    public void update(String uuid, UpdateOpportunityReq req) {
        String path = String.format("/v1/opportunities/%s", uuid);

        Mono<BaseResponse> responseEntityMono = this.webClient
            .put()
            .uri(path)
            .body(Mono.just(req), UpdateOpportunityReq.class)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponse.class);
            })
            .doOnError(
                error -> log.error("OpportunityService - update - opp_uuid: [{}] - error ",uuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        BaseResponse resp = responseEntityMono.block();

        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("OpportunityService","update",resp.getMeta().toString());
        return;
    }

    @Override
    public Opportunity getByUuid(String uuid) {
        String path = String.format("/v1/opportunities/%s", uuid);

        Mono<OpportunityBaseResponse> responseEntityMono = this.webClient
            .get()
            .uri(path)
            .exchangeToMono(response -> {
                return response.bodyToMono(OpportunityBaseResponse.class);
            })
            .doOnError(
                error -> log.error("OpportunityService - getByUuid - opp_uuid: [{}] - error ",uuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        OpportunityBaseResponse resp = responseEntityMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("OpportunityService","getByUuid",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public Opportunity getByLeadUuid(String leadUuid) {
        String path = String.format("/v1/opportunities?lead_uuid=%s", leadUuid);

        Mono<OpportunityBaseResponse> responseEntityMono = this.webClient
            .get()
            .uri(path)
            .exchangeToMono(response -> {
                return response.bodyToMono(OpportunityBaseResponse.class);
            })
            .doOnError(
                error -> log.error("OpportunityService - getByLeadUuid - lead_uuid: [{}] - error ",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        OpportunityBaseResponse resp = responseEntityMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("OpportunityService","getByLeadUuid",resp.getMeta().toString());

        return resp.getData();
    }
}
