package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineUsecaseService;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.Note;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

public class CreateNoteResp extends BaseResponse<Note> {
}
