package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineService;

import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

@Data
public class CreateSalesPipelineBaseResp extends BaseResponse<SalesPipeline> {
}
