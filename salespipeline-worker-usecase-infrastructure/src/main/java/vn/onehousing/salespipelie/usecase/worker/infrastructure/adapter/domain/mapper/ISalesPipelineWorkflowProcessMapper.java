package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection.MongoSalesPipelineWorkflowInstance;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.mapper.IObjectIdMapper;

@Mapper(
        componentModel = "spring",
        uses = {
                IObjectIdMapper.class
        }
)
public interface ISalesPipelineWorkflowProcessMapper {
    SalesPipelineWorkflowInstance from(MongoSalesPipelineWorkflowInstance data);

    MongoSalesPipelineWorkflowInstance to(SalesPipelineWorkflowInstance data);
}
