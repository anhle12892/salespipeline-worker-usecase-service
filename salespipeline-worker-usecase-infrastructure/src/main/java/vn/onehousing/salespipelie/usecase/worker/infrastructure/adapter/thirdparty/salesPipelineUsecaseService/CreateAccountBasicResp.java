package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.salesPipelineUsecaseService;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.CreateAccountResp;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

public class CreateAccountBasicResp extends BaseResponse<CreateAccountResp> {
}
