package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.camundaService;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.EvaluateReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.ProcessInstance;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.config.CamundaExceptionProperties;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.CannotFindTaskException;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;
import java.util.*;

@Service
@Slf4j
public class CamundaService implements IWorkflowService {

    private final WebClient webClient;
    private final ObjectMapper objectMapper;
    private final CamundaExceptionProperties properties;

    public CamundaService(@Lazy WebClient camundaServiceClient, ObjectMapper objectMapper, CamundaExceptionProperties properties) {
        this.webClient = camundaServiceClient;
        this.objectMapper = objectMapper;
        this.properties = properties;
    }

    @Override
    public ProcessInstance start(String definitionKey, CompleteTaskReq req) {
        try {
            final WebClient.RequestBodySpec request = this.webClient
                    .post()
                    .uri(String.format("/process-definition/key/%s/start", definitionKey))
                    .contentType(MediaType.APPLICATION_JSON);
            if (req != null && req.getVariables() != null && !req.getVariables().isEmpty()) {
                request.bodyValue(req);
            }
            Mono<ProcessInstance> monoResp = request
                    .retrieve()
                    .bodyToMono(ProcessInstance.class)
                    .doOnError(
                            error -> log.error("CamundaService - start - error", error)
                    )
                    .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
            return monoResp.block();
        } catch (Exception ex) {
            throw new RequestThirdPartyException(
                    "CamundaService",
                    "start",
                    ex.getMessage()
            );
        }
    }

    @Override
    public List<Task> getListTaskByProcessInstanceId(String processInstanceId) {
        try {
            Mono<Task[]> monoResp = this.webClient
                    .get()
                    .uri(String.format("/task?processInstanceId=%s", processInstanceId))
                    .retrieve()
                    .bodyToMono(Task[].class)
                    .doOnError(
                            error -> log.error("CamundaService - getListTaskByProcessInstanceId - error ", error)
                    )
                    .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
            Task[] tasks = monoResp.block();
            log.info("CamundaService.getListTaskByProcessInstanceId - processInstanceId: [{}] - response: [{}]",
                    processInstanceId,
                    objectMapper.writeValueAsString(tasks)
            );
            return Arrays.asList(tasks);
        } catch (Exception ex) {
            throw new RequestThirdPartyException("CamundaService", "getListTaskByProcessInstanceId", ex.getMessage());
        }
    }

//    @Override
//    public Task[] getListTaskBusinessKey(String businessKey) {
//        try {
//            Mono<Task[]> monoResp = this.webClient
//                    .get()
//                    .uri(String.format("/task?processInstanceId=%s", businessKey))
//                    .retrieve()
//                    .bodyToMono(Task[].class)
//                    .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
//            return monoResp.block();
//        } catch (Exception ex) {
//            throw new RequestThirdPartyException("CamundaService","getListTaskByProcessInstanceId",ex.getMessage());
//        }
//    }

//    @Override
//    public ProcessInstance startProcessInstanceByMessage(StartMessageReq req) {
//        try {
//            Mono<MessageWithProcessInstance[]> responseEntityMono = this.webClient
//                    .post()
//                    .uri("/message")
//                    .contentType(MediaType.APPLICATION_JSON)
//                    .bodyValue(req)
//                    .retrieve()
//                    .bodyToMono(MessageWithProcessInstance[].class)
//                    .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
//            var resp = responseEntityMono.block();
//            if (resp != null && resp.length > 0)
//                return resp[0].getProcessInstance();
//            return null;
//        } catch (Exception ex) {
//            throw new RequestThirdPartyException("CamundaService","startProcessInstanceByMessage",ex.getMessage());
//        }
//    }

    @Override
    public ProcessInstance[] getListProcessInstanceByLeadUuid(String leadUuid) {
        GetProcessInstanceReq req = new GetProcessInstanceReq();
        req.setVariables(Collections.singletonList(new Variable("lead_uuid", "eq", leadUuid)));
        req.setSorting(Collections.singletonList(new Sorting("instanceId", "asc")));
        return getListProcessInstance(req);
    }

    private ProcessInstance[] getListProcessInstance(GetProcessInstanceReq req) {
        try {
            Mono<ProcessInstance[]> responseEntityMono = this.webClient
                    .post()
                    .uri("/process-instance")
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(req)
                    .retrieve()
                    .bodyToMono(ProcessInstance[].class)
                    .doOnError(
                            error -> log.error("CamundaService - getListProcessInstanceByLeadUuid - error", error)
                    )
                    .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
            var resp = responseEntityMono.block();
            if (resp != null && resp.length > 0)
                return resp;
            return null;
        } catch (Exception ex) {
            throw new RequestThirdPartyException("CamundaService", "getListProcessInstance", "request: [" + req.toString() + "]");
        }
    }

    @Override
    public ProcessInstance[] getListProcessInstanceByBusinessKey(String businessKey) {
        GetProcessInstanceReq req = new GetProcessInstanceReq();
        req.setBusinessKey(businessKey);
        return getListProcessInstance(req);
    }

    @Override
    public List<String> getSaleRoles(EvaluateReq req) throws Exception {
        var monoResp = this.webClient.post()
                .uri("/decision-definition/key/Decision_Agent_Roles_Needed/evaluate")
                .body(BodyInserters.fromValue(req))
                .exchangeToMono(
                        response -> {
                            return response.bodyToMono(List.class);
                        }
                );
        List<Map<String, Map<String, Object>>> resp = monoResp.block();
        log.info(
                "WorkflowService.isFromTCA - request: [{}] - response: [{}]",
                objectMapper.writeValueAsString(req),
                objectMapper.writeValueAsString(resp)
        );
        if (Objects.nonNull(resp) && resp.size() > 0) {
            Map<String, Object> data = resp.get(0).entrySet().iterator().next().getValue();
            String[] roles = StringUtils.split(String.valueOf(data.get("value")), ",");
            return Arrays.asList(roles);
        }
        return Collections.emptyList();
    }

    @Override
    public void doCompleteTask(String taskId, CompleteTaskReq req) throws Exception {
        String data = "";
        if (req.variables != null) {
            data = objectMapper.writeValueAsString(req.getVariables());
        }
        try {
            Mono<ExceptionDto> responseEntityMono = this.webClient
                    .post()
                    .uri(String.format("/task/%s/complete", taskId))
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(req)
                    .exchangeToMono(clientResponse -> {
                        if (clientResponse.statusCode().isError()) {
                            return clientResponse.bodyToMono(ExceptionDto.class);
                        }
                        return Mono.empty();
                    });
            final Optional<ExceptionDto> exceptionDtoOpt = responseEntityMono.blockOptional();
            if (exceptionDtoOpt.isPresent()) {
                final ExceptionDto exceptionDto = exceptionDtoOpt.get();
                final String messageError = StringUtils.replace(properties.getNotFoundMessage(), "{taskId}", taskId);
                final String message = exceptionDto.getMessage();
                final String exception = message + " - " + data;
                if (messageError.equalsIgnoreCase(message)) {
                    throw new CannotFindTaskException(exception);
                }
                throw new RuntimeException(exception);
            }
            log.info("CamundaService - doCompleteTask - data: [{}] - success", data);
        } catch (CannotFindTaskException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RequestThirdPartyException("CamundaService", String.format("doCompleteTask [%s], [%s]", taskId, data), ex.getMessage());
        }
    }

}
