package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadService;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request.UpdateLeadReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.response.LeadServiceResponse;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;

@Slf4j
@Service
public class LeadService implements ILeadService {

    private final WebClient webClient;

    public LeadService(@Lazy WebClient leadServiceClient) {
        this.webClient = leadServiceClient;
    }

    public void update(String leadUuid, UpdateLeadReq req) {
        String path = String.format("/v1/leads/%s", leadUuid);

        Mono<LeadServiceResponse> monoResp  = this.webClient
            .put()
            .uri(path)
            .body(Mono.just(req), UpdateLeadReq.class)
            .exchangeToMono(response -> {
                return response.bodyToMono(LeadServiceResponse.class);
            })
            .doOnError(
                error -> log.error("LeadService - update - lead_uuid: [{}] - error",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        LeadServiceResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadService","update",resp.getMeta().toString());
        return;
    }

    @Override
    public Lead getByUuid(String leadUuid) {
        Mono<LeadServiceResponse> monoResp = this.webClient
            .get()
            .uri("/v1/leads/" + leadUuid)
            .exchangeToMono(response -> {
                return response.bodyToMono(LeadServiceResponse.class);
            })
            .doOnError(
                error -> log.error("LeadService - getByUuid - lead_uuid: [{}] - error ", leadUuid,error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        LeadServiceResponse resp = monoResp.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadService","getByUuid",resp.getMeta().toString());
        return resp.getData();
    }

}
