package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ErrorMessage;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.IErrorMessageRepository;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain.mapper.IErrorMessageMapper;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection.IMongoErrorMessageRepository;

@Repository
@AllArgsConstructor
public class ErrorMessageRepository implements IErrorMessageRepository {

    private final IMongoErrorMessageRepository repository;
    private final IErrorMessageMapper mapper;

    @Override
    public ErrorMessage create(ErrorMessage data) {
        return mapper.from(repository.save(mapper.to(data)));
    }

    @Override
    public ErrorMessage update(ErrorMessage data) {
        return mapper.from(repository.save(mapper.to(data)));
    }

    @Override
    public ErrorMessage getByActivityAndHashCode(String activity, int hashCode) {
        return mapper.from(repository.getByActivityAndHashCode(activity,hashCode));
    }
}
