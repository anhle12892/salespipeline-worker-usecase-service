package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses;


import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.DuplicationLeadResp;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

import java.util.List;

public class BaseResponseDuplicationLeadResp extends BaseResponse<List<DuplicationLeadResp>> {
}
