package vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;
import java.util.Map;

@Data
@Document("error_messages")
public class MongoErrorMessage implements Persistable<ObjectId> {
    ObjectId id;

    String activity;

    Map<String,Object> message;

    @Indexed
    @Field("hash_code")
    int hashCode;

    @Field("retry_times")
    Integer retryTimes;

    @Transient
    boolean isNew = false;

    @CreatedDate
    @Field("created_date")
    Instant createdDate;

    @LastModifiedDate
    @Field("last_modified_date")
    Instant lastModifiedDate;

    @CreatedBy
    @Field("created_by")
    String createdBy;

    @LastModifiedBy
    @Field("last_modified_by")
    String lastModifiedBy;

    boolean isDeleted;
}
