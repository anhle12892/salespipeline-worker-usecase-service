package vn.onehousing.salespipelie.usecase.worker.infrastructure.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "camunda.exception.task")
public class CamundaExceptionProperties {
    private String notFoundMessage = "Cannot complete task {taskId}: Cannot find task with id {taskId}: task is null";
}
