package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.distributorservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.distributorservice.IDistributorService;

import java.time.Duration;
import java.util.Collections;
import java.util.Set;


@Slf4j
@Service
public class DistributorService implements IDistributorService {

    private final WebClient webClient;
    private final ObjectMapper objectMapper;

    public DistributorService(@Lazy WebClient distributorServiceClient, ObjectMapper objectMapper){
        this.webClient = distributorServiceClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public void sendToCCForPreQualify(String leadUuid) throws JsonProcessingException {

        Set<ReQualifyReq> req = Collections.singleton(new ReQualifyReq(leadUuid, null));

        Mono<String> monoResp  = this.webClient
            .post()
            .uri("/v1.2/distributor/salesforce-context/gateway/lc/prequalify/sf")
            .body(Mono.just(req), Set.class)
            .exchangeToMono(response -> response.bodyToMono(String.class))
            .doOnError(
                error -> log.error("DistributorService - sendToCCForPreQualify - lead_uuid: [{}] - error",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        String resp = monoResp.block();
        log.info("DistributorService - sendToCCForPreQualify - req: [{}] - resp: [{}]",objectMapper.writeValueAsString(req),resp);
    }
    
    @Override
    public void sendToCCForReQualifySuccess(String leadUuid, String dropStatus) throws JsonProcessingException {

        Set<ReQualifyReq> req = Collections.singleton(new ReQualifyReq(leadUuid, dropStatus));

        Mono<String> monoResp  = this.webClient
            .post()
            .uri("/v1.2/distributor/salesforce-context/gateway/lc/requalify/sf/success")
            .body(Mono.just(req), Set.class)
            .exchangeToMono(response -> response.bodyToMono(String.class))
            .doOnError(
                error -> log.error(
                    "DistributorService - sendToCCForReQualifySuccess - lead_uuid: [{}] - error",
                    leadUuid,
                    error
                )
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        String resp = monoResp.block();
        log.info("DistributorService - sendToCCForReQualifySuccess - req: [{}] - resp: [{}]",objectMapper.writeValueAsString(req),resp);
    }

    @Override
    public void sendToCCForReQualifyStop(String leadUuid, String dropStatus) throws JsonProcessingException {

        Set<ReQualifyReq> req = Collections.singleton(new ReQualifyReq(leadUuid,dropStatus));

        Mono<String> monoResp  = this.webClient
            .post()
            .uri("/v1.2/distributor/salesforce-context/gateway/lc/requalify/sf/failure")
            .body(Mono.just(req), Set.class)
            .exchangeToMono(response -> response.bodyToMono(String.class))
            .doOnError(
                error -> log.error(
                    "DistributorService - sendToCCForReQualifyStop - lead_uuid: [{}] - error",
                    leadUuid,
                    error
                )
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));
        String resp = monoResp.block();
        log.info("DistributorService - sendToCCForReQualifyStop - req: [{}] - resp: [{}]",objectMapper.writeValueAsString(req),resp);
    }
}
