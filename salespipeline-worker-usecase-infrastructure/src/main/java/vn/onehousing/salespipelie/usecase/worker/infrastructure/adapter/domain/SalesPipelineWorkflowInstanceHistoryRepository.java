package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowInstanceHistoryRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstanceHistory;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.domain.mapper.ISalesPipelineWorkflowInstanceHistoryMapper;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection.IMongoSalesPipelineWorkflowInstanceHistoryRepository;

//@ReadingConverter
@Repository
@AllArgsConstructor
public class SalesPipelineWorkflowInstanceHistoryRepository implements ISalesPipelineWorkflowInstanceHistoryRepository {

    private final IMongoSalesPipelineWorkflowInstanceHistoryRepository repository;
    private final ISalesPipelineWorkflowInstanceHistoryMapper mapper;

    @Override
    public SalesPipelineWorkflowInstanceHistory create(SalesPipelineWorkflowInstanceHistory data) {
        return mapper.from(
            repository.save(
                mapper.to(data)
            )
        );
    }
}
