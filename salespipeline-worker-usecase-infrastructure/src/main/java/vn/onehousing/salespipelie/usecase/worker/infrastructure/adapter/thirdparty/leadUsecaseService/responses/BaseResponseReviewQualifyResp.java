package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses;


import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.ReviewQualifyResp;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

public class BaseResponseReviewQualifyResp extends BaseResponse<ReviewQualifyResp> {
}
