package vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.Instant;

@Data
@Document("sales_pipeline_workflow_instance_histories")
public class MongoSalesPipelineWorkflowInstanceHistory implements Persistable<ObjectId> {
    ObjectId id;

    @Indexed
    @Field("sales_pipeline_workflow_instance_id")
    ObjectId salesPipelineWorkflowInstanceId;

    @Field("process_instance_id")
    String processInstanceId;

    @CreatedDate
    @Field("created_date")
    Instant createdDate;

    @LastModifiedDate
    @Field("last_modified_date")
    Instant lastModifiedDate;

    @CreatedBy
    @Field("created_by")
    String createdBy;

    @LastModifiedBy
    @Field("last_modified_by")
    String lastModifiedBy;

    @Transient
    boolean isNew = false;
}
