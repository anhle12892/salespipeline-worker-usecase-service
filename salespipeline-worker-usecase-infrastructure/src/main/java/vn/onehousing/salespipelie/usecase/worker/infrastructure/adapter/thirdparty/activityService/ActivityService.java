package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.activityService;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import vn.onehousing.common.elasticsearch.service.RestHighLevelClientWrapper;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.activityService.IActivityService;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ActivityService implements IActivityService {

    private final RestHighLevelClientWrapper taskActivityServiceEsClient;
    private final ObjectMapper objectMapper;

    public ActivityService(@Lazy RestHighLevelClientWrapper taskActivityServiceEsClient, ObjectMapper objectMapper) {
        this.taskActivityServiceEsClient = taskActivityServiceEsClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<List<Task>> getTaskBy(String entityUuid, String entityType) throws IOException {
        final TermQueryBuilder entityUuidQueryBuilder = new TermQueryBuilder("entity_uuid", entityUuid);
        final TermQueryBuilder entityTypeQueryBuilder = new TermQueryBuilder("entity_type", entityType);

        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(entityUuidQueryBuilder);
        query.must(entityTypeQueryBuilder);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = taskActivityServiceEsClient.getRestHighLevelClient().search(searchRequest, RequestOptions.DEFAULT);
        if (searchResponse.getHits() != null
                && searchResponse.getHits().getHits() != null
                && searchResponse.getHits().getHits().length > 0) {
            log.info("activityServiceEsClient.getNoteUuids - lead_uuid: [{}] - note size: [{}]"
                    , entityUuid, searchResponse.getHits().getHits().length);
            List<Task> tasks = new ArrayList<>();
            for (SearchHit hit : searchResponse.getHits().getHits()) {
                Task task = objectMapper.readValue(hit.getSourceAsString(), Task.class);
                tasks.add(task);
            }
            return Optional.of(tasks);
        }
        return Optional.empty();
    }
}
