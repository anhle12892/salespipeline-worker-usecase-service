package vn.onehousing.salespipelie.usecase.worker.infrastructure.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(CamundaExceptionProperties.class)
public class CamundaExceptionConfig {
    //
}
