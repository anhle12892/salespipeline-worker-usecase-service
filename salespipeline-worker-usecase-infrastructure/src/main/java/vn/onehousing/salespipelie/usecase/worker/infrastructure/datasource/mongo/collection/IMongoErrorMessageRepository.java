package vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface IMongoErrorMessageRepository extends MongoRepository<MongoErrorMessage, String> {
    MongoErrorMessage getByActivityAndHashCode(String activity, int hashCode);
}
