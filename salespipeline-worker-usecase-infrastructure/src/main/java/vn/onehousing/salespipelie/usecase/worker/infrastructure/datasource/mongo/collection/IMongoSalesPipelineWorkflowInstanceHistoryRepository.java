package vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface IMongoSalesPipelineWorkflowInstanceHistoryRepository
        extends MongoRepository<MongoSalesPipelineWorkflowInstanceHistory, String> {
}
