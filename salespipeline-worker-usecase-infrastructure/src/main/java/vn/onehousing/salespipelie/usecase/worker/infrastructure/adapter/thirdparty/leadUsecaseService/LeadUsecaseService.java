package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.ILeadUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.DuplicationLeadResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.NeedDuplicateCheckingResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.ReviewConvertResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.ReviewQualifyResp;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses.BaseResponseDuplicationLeadResp;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses.BaseResponseNeedDuplicateCheckingResp;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses.BaseResponseReviewConvertResp;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses.BaseResponseReviewQualifyResp;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.time.Duration;
import java.util.*;

@Slf4j
@Component
public class LeadUsecaseService implements ILeadUsecaseService {

    private final WebClient webClient;

    public LeadUsecaseService(@Lazy WebClient leadUsecaseServiceClient) {
        this.webClient = leadUsecaseServiceClient;
    }

    @Override
    public void assign(String leadUuid) {
        String path = String.format("/v1/leads/%s/assignments", leadUuid);
        Mono<BaseResponse> baseResponseMono = this.webClient
            .put()
            .uri(path)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponse.class);
            })
            .doOnError(
                error -> log.error("LeadUsecaseService - assign - lead_uuid: [{}] - error ",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        BaseResponse resp = baseResponseMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadUsecaseService","assign",resp.getMeta().toString());
        return;
    }

    @Override
    public ReviewQualifyResp reviewQualify(String leadUuid) {
        String path = String.format("/v1/leads/%s/review-qualify", leadUuid);
        Mono<BaseResponseReviewQualifyResp> baseResponseMono = this.webClient
            .get()
            .uri(path)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponseReviewQualifyResp.class);
            })
            .doOnError(
                error -> log.error("LeadUsecaseService - reviewQualify - lead_uuid: [{}] - error ",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        BaseResponseReviewQualifyResp resp = baseResponseMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadUsecaseService","reviewQualify",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public ReviewConvertResp reviewConvert(String leadUuid) {
        String path = String.format("/v1/leads/%s/review-convert", leadUuid);
        Mono<BaseResponseReviewConvertResp>  baseResponseMono = this.webClient
            .get()
            .uri(path)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponseReviewConvertResp.class);
            })
            .doOnError(
                error -> log.error("LeadUsecaseService - reviewConvert - lead_uuid: [{}] - error ",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        BaseResponseReviewConvertResp resp = baseResponseMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadUsecaseService","reviewConvert",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public NeedDuplicateCheckingResp needDuplicateChecking(String leadUuid) {

        String path = String.format("/v1/leads/%s/need-duplication-checking", leadUuid);
        Mono<BaseResponseNeedDuplicateCheckingResp> baseResponseMono = this.webClient
            .get()
            .uri(path)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponseNeedDuplicateCheckingResp.class);
            })
            .doOnError(
                    error -> log.error("LeadUsecaseService - needDuplicateChecking - lead_uuid: [{}] - error ",leadUuid, error)
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        BaseResponseNeedDuplicateCheckingResp resp = baseResponseMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadUsecaseService","needDuplicateChecking",resp.getMeta().toString());

        return resp.getData();
    }

    @Override
    public List<DuplicationLeadResp> getListDuplication(String leadUuid) {
        String path = String.format("/v1/leads/duplications", leadUuid);

        Map<String, String> maps = new HashMap<>();

        Mono<BaseResponseDuplicationLeadResp> baseResponseMono = this.webClient
            .post()
            .uri(path)
            .body(Mono.just(Collections.singletonMap("lead_uuid", leadUuid)), Map.class)
            .exchangeToMono(response -> {
                return response.bodyToMono(BaseResponseDuplicationLeadResp.class);
            })
            .doOnError(
                error -> log.error("LeadUsecaseService - getListDuplication - lead_uuid: [{}] - error: [{}]",leadUuid, Arrays.toString(error.getStackTrace()))
            )
            .retryWhen(Retry.fixedDelay(5, Duration.ofSeconds(1)));

        BaseResponseDuplicationLeadResp resp = baseResponseMono.block();
        if (!BaseResponse.OK_CODE.equals(resp.getMeta().getCode()))
            throw new RequestThirdPartyException("LeadUsecaseService","getListDuplication",resp.getMeta().toString());

        return resp.getData();
    }
}
