package vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.leadUsecaseService.responses;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.ReviewConvertResp;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

public class BaseResponseReviewConvertResp extends BaseResponse<ReviewConvertResp> {
}
