package vn.onehousing.salespipelie.usecase.worker.infrastructure.datasource.mongo.collection;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IMongoSalesPipelineWorkflowInstanceRepository extends MongoRepository<MongoSalesPipelineWorkflowInstance, String> {

    MongoSalesPipelineWorkflowInstance getById(ObjectId id);

    MongoSalesPipelineWorkflowInstance getByLeadUuid(String leadUuid);

    MongoSalesPipelineWorkflowInstance getByOpportunityUuid(String opportunityUuid);

    MongoSalesPipelineWorkflowInstance getByProcessInstanceId(String processInstanceId);
}
