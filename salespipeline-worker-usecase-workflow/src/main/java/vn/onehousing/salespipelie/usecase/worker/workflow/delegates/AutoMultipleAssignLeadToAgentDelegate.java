package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import joptsimple.internal.Strings;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.RevokedType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.IAssignmentService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc.IAutoAssignLeadToAgentUc;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.*;

@Slf4j
@Service
@AllArgsConstructor
public class AutoMultipleAssignLeadToAgentDelegate extends BaseDelegate {

    private final IAutoAssignLeadToAgentUc assignLeadToAgentUc;
    private final ObjectMapper objectMapper;
    private final ISalesPipelineUsecaseService salesPipelineService;
    private final IAssignmentService assignmentService;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("AutoMultipleAssignLeadToAgentDelegate");
        String leadUuid = (String) delegateExecution.getVariable(VariableName.LEAD_UUID);
        String leadStage = (String) delegateExecution.getVariable(VariableName.LEAD_STAGE);
        List<User> assignedUsers = !StringUtils.isEmpty((String) delegateExecution.getVariable(VariableName.ASSIGNED_USERS)) ?
                objectMapper.readValue((String) delegateExecution.getVariable(VariableName.ASSIGNED_USERS), new TypeReference<>() {
                }) : Collections.emptyList();
        log.info("AutoAssignLeadToAgentDelegate expectedUsers = {}", delegateExecution.getVariable(VariableName.EXPECTED_USERS));
        List<User> expectedUsers = new ArrayList<>(!StringUtils.isEmpty((String) delegateExecution.getVariable(VariableName.EXPECTED_USERS)) ?
                objectMapper.readValue((String) delegateExecution.getVariable(VariableName.EXPECTED_USERS), new TypeReference<>() {
                }) : Collections.emptyList());
        if (expectedUsers.isEmpty()) {
            String expectedUserUuid = (String) delegateExecution.getVariable(VariableName.LEAD_EXPECTED_ASSIGN_USER_UUID);
            if (!Strings.isNullOrEmpty(expectedUserUuid) && !"null".equals(expectedUserUuid)) {
                var userDto = assignmentService.getRolesByUserUuid(expectedUserUuid);
                var role = (Objects.nonNull(userDto) && !userDto.getRoles().isEmpty()) ? userDto.getRoles().get(0).getName() : null;
                expectedUsers.add(new User(expectedUserUuid, null, role));
            }
        }
        List<String> salesRoles = Objects.nonNull(delegateExecution.getVariable(VariableName.SALE_ROLES)) ?
                new ArrayList<>((Collection<String>) delegateExecution.getVariable(VariableName.SALE_ROLES)) : Collections.emptyList();
        String agentUuid = null;
        log.info("AutoMultipleAssignLeadToAgentDelegate Assign multi sales with assignUser = {} , expectedUsers = {}, roles = {}",
                assignedUsers, expectedUsers, salesRoles);
        try {
            assignLeadToAgentUc.processMultiAgent(leadUuid,
                    leadStage,
                    expectedUsers,
                    assignedUsers,
                    salesRoles,
                    null);
            var leadData = salesPipelineService.getByLeadUuid(leadUuid);
            agentUuid = (Objects.nonNull(leadData.getAssignedUsers()) && !leadData.getAssignedUsers().isEmpty()) ?
                    leadData.getAssignedUsers().get(0).getUserUuid() : null;
        } catch (Exception ex) {
            log.warn("AutoMultipleAssignLeadToAgentDelegate with lead_uuid {} error: ", leadUuid, ex);
        }
        Map<String, Object> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_ASSIGNED_USER_UUID, agentUuid);
        variableMap.put(VariableName.IS_MULTI_ASSIGNED, Objects.nonNull(agentUuid));
        variableMap.put(VariableName.REVOKED_TYPE, RevokedType.AUTO_ASSIGNED_FAILED);
        delegateExecution.setVariables(variableMap);
        logExistDelegate("AutoMultipleAssignLeadToAgentDelegate");
    }
}
