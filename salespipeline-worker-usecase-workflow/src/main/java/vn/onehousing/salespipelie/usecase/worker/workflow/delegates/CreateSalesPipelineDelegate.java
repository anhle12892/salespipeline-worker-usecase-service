package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.createsalepipelineuc.CreateSalesPipelineUcReq;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.createsalepipelineuc.ICreateSalespipelineUc;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Collections;

@Service
@AllArgsConstructor
public class CreateSalesPipelineDelegate extends BaseDelegate {

    private final ICreateSalespipelineUc createSalesPipelineUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {

        logEnterDelegate("CreateSalesPipelineDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        SalesPipeline salespipeline = createSalesPipelineUc.process(new CreateSalesPipelineUcReq(leadUuid));
        delegateExecution.setVariables(Collections.singletonMap(VariableName.SALESPIPELINE_UUID, salespipeline.getUuid()));

        logExistDelegate("CreateSalesPipelineDelegate");
    }

}
