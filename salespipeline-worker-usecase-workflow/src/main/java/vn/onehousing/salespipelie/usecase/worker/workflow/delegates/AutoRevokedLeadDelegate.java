package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.revokedlead.IAutoRevokedLeadUsecase;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Revoke;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class AutoRevokedLeadDelegate extends BaseDelegate {

    private final IAutoRevokedLeadUsecase autoRevokedLeadUsecase;

    @Override
    public void process(DelegateExecution delegateExecution) throws Exception {
        logEnterDelegate("AutoRevokedLeadDelegate");

        String leadUuid = (String) delegateExecution.getVariable(VariableName.LEAD_UUID);

        try {
            Revoke resp = autoRevokedLeadUsecase.revokedLead(leadUuid);

            Map<String, Object> variables = new HashMap<>();
            if(StringUtils.isNotEmpty(resp.getDropStatus())){
                variables.put(VariableName.DROP_STATUS,resp.getDropStatus());
            }
            variables.put(VariableName.IS_LEAD_REVOKED, resp.isRevoked());
            variables.put(VariableName.REVOKED_TYPE, resp.getRevokedType() == null ? "" : resp.getRevokedType());
            variables.put(VariableName.REVOKED_REASON,resp.getRevokedReason());
            delegateExecution.setVariables(variables);
        } catch (Exception ex) {
            log.error("AutoRevokedLeadDelegate lead_uuid: {} error: ", leadUuid, ex);
        }

        logExistDelegate("AutoRevokedLeadDelegate");
    }
}
