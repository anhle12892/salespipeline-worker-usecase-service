package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeprocessinstanceiduc.IChangeProcessInstanceId;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class SendMessageDelegate extends BaseDelegate {

    private final RuntimeService runtimeService;
    private final IChangeProcessInstanceId changeProcessInstanceId;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {

        Map<String, Object> allVariable = delegateExecution.getVariables();
        Map<String, Object> objectMap = (Map<String, Object>) allVariable.get(InputName.INPUT_TOPIC);
        String topicName = objectMap.get(VariableName.TOPIC_NAME).toString();
        allVariable.remove(InputName.INPUT_TOPIC);

        logEnterDelegate(String.format("SendMessageDelegate - topic: [%s]",topicName));

        String salesPipelineWorkflowId = allVariable.get(VariableName.SALES_PIPELINE_WORKFLOW_ID).toString();
        String leadUuid = allVariable.get(VariableName.LEAD_UUID).toString();
        String processInstanceId = allVariable.get(VariableName.PROCESS_INSTANCE_ID).toString();

        MessageCorrelationResult result = runtimeService.createMessageCorrelation(topicName)
                .processInstanceBusinessKey(leadUuid)
                .setVariables(allVariable)
                .processInstanceId(processInstanceId)
                .correlateWithResult();

        changeProcessInstanceId.process(salesPipelineWorkflowId,result.getProcessInstance().getProcessInstanceId());

        logExistDelegate("SendMessageDelegate");
    }
}
