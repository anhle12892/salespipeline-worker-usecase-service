package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.SendCcType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.sendtoccuc.ISendToCcUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

@Slf4j
@Component
@AllArgsConstructor
public class SendLeadStopToCcForReQualifyDelegate extends BaseDelegate {

    private final ISendToCcUc sendToCcUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws Exception {
        logEnterDelegate("SendLeadStopToCcForReQualifyDelegate");
        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String dropStatus = delegateExecution.getVariable(VariableName.DROP_STATUS).toString();
        sendToCcUc.process(leadUuid, SendCcType.RE_QUALIFY_STOP,dropStatus);
        logExistDelegate("SendLeadStopToCcForReQualifyDelegate");
    }
}
