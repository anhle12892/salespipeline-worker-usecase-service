package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc.ChangeLeadStatusReq;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc.IChangeLeadStatusUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.MissingVariableException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class ChangeLeadStatusDelegate extends BaseDelegate {

    private final IChangeLeadStatusUc changeLeadStatusUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws Exception {
        logEnterDelegate("ChangeLeadStatusDelegate");

        String leadId = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();

        Map<String, Object> maps = (Map<String, Object>) delegateExecution.getVariable(InputName.INPUT_CHANGE_DATA);

        String leadStatus = maps.get(VariableName.LEAD_STATUS).toString();
        // prepare for case fire event when lead qualify success or false
        boolean leadQualify = false;
        try {
            leadQualify = Boolean.parseBoolean(delegateExecution.getVariable(VariableName.LEAD_QUALIFY).toString());
        } catch (Exception ex) {

        }

        if (leadStatus.equals("BACK TO DROP STATUS")) {
            leadStatus = (String) delegateExecution.getVariable(VariableName.DROP_STATUS);
            if (StringUtils.isEmpty(leadStatus)) {
                throw new MissingVariableException(
                    String.format("ChangeLeadStatusDelegate.process - variable: [%s] - missing", VariableName.DROP_STATUS)
                );
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put(VariableName.REVOKED_REASON, getNullableVariable(VariableName.REVOKED_REASON, delegateExecution));
        data.put(VariableName.REVOKED_TYPE, getNullableVariable(VariableName.REVOKED_TYPE, delegateExecution));
        data.put(VariableName.LEAD_ASSIGNED_USER_UUID, getNullableVariable(VariableName.LEAD_ASSIGNED_USER_UUID, delegateExecution));
        data.put(VariableName.LEAD_QUALIFY, leadQualify);
        data.put(VariableName.LEAD_SOURCE, delegateExecution.getVariable(VariableName.LEAD_SOURCE));
        data.put(VariableName.LEAD_CHANNEL, delegateExecution.getVariable(VariableName.LEAD_CHANNEL));

        changeLeadStatusUc.process(
            new ChangeLeadStatusReq(
                leadId,
                leadStatus,
                data
            )
        );

        delegateExecution.setVariables(Collections.singletonMap(VariableName.LEAD_STATUS, leadStatus));
        logExistDelegate("ChangeLeadStatusDelegate");
    }

}
