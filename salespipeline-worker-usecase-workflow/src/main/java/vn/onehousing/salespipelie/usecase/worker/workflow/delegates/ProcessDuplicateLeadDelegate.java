package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.updateleadduplicateduc.IUpdateLeadDuplicatedUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class ProcessDuplicateLeadDelegate extends BaseDelegate {

    private final IUpdateLeadDuplicatedUc updateLeadDuplicatedUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {

        logEnterDelegate("ProcessDuplicateLeadDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String duplicatedLeadUuid = delegateExecution.getVariable(VariableName.DUPLICATED_LEAD_UUID).toString();
        String duplicatedLeadCode = delegateExecution.getVariable(VariableName.DUPLICATED_LEAD_CODE).toString();
        updateLeadDuplicatedUc.process(leadUuid, duplicatedLeadUuid, duplicatedLeadCode);

        logExistDelegate("ProcessDuplicateLeadDelegate");
    }
}
