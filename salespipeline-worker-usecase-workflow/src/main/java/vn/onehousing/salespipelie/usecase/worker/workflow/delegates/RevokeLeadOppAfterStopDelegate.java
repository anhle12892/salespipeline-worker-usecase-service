package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.revokeleadoppafterstopuc.IRevokeLeadOppAfterStopUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

@Slf4j
@Service
@AllArgsConstructor
public class RevokeLeadOppAfterStopDelegate extends BaseDelegate {

    private final IRevokeLeadOppAfterStopUc revokedLeadOppAfterStopUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws Exception {
        logEnterDelegate("RevokeLeadOppAfterStopDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String revokeType = getNullableVariable(VariableName.REVOKED_TYPE, delegateExecution);
        String revokeReason = getNullableVariable(VariableName.REVOKED_REASON, delegateExecution);
        revokedLeadOppAfterStopUc.process(leadUuid, revokeType, revokeReason);

        logExistDelegate("RevokeLeadOppAfterStopDelegate");
    }
}
