package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.RevokedType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadRevokedEvent;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class RemoveSalesFromLeadDelegate extends BaseDelegate {

    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ObjectMapper objectMapper;
    private final EventPublisher eventPublisher;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("RemoveSalesFromLeadDelegate");
        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        var leadView = salesPipelineUsecaseService.getByLeadUuid(leadUuid);
        List<User> assignedUsers = leadView.getAssignedUsers();
        List<String> salesRoles = objectMapper.convertValue(delegateExecution.getVariable(VariableName.SALE_ROLES), new TypeReference<>() {
        });
        String revokedType = RevokedType.AUTO_ASSIGNED_FAILED.name();
        if (salesRoles.isEmpty()) {
            return;
        }
        if (Objects.nonNull(assignedUsers) && !assignedUsers.isEmpty()) {
            var remainUsers = assignedUsers.stream()
                    .filter(user -> Objects.nonNull(user) &&
                            salesRoles.contains(user.getRole()))
                    .collect(Collectors.toList());
            UpdateSalesPipelineLeadData updateLead = UpdateSalesPipelineLeadData
                    .builder().assignedUsers(remainUsers).build();
            salesPipelineUsecaseService.updateLead(leadUuid, updateLead);
            //TO-DO Fire notification for removed sales
            assignedUsers.removeAll(remainUsers);
            assignedUsers.forEach(user -> {
                LeadRevokedEvent leadRevokedEvent = LeadRevokedEvent.builder()
                        .leadUuid(leadUuid)
                        .opportunityUuid(leadView.getOpportunityUuid())
                        .revokedUserUuid(user.getUserUuid())
                        .revokedType(revokedType)
                        .build();
                eventPublisher.publishEvent(leadRevokedEvent);
            });
        }


        logExistDelegate("RemoveSalesFromLeadDelegate");
    }
}
