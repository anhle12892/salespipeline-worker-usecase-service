package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.SendCcType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.sendtoccuc.ISendToCcUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class SendLeadToCcDelegate extends BaseDelegate {

    private ISendToCcUc sendToCcUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("SendLeadToCcDelegate");
        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        sendToCcUc.process(leadUuid, SendCcType.PRE_QUALIFY,null);
        logExistDelegate("SendLeadToCcDelegate");
    }
}
