package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc.IAutoAssignLeadToAgentUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class AutoAssignLeadToAgentDelegate extends BaseDelegate {

    private final IAutoAssignLeadToAgentUc assignLeadToAgentUc;
    private final ObjectMapper objectMapper;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("AutoAssignLeadToAgentDelegate");
        String leadUuid = (String) delegateExecution.getVariable(VariableName.LEAD_UUID);
        String expectedAssignUserUuid = (String) delegateExecution.getVariable(VariableName.LEAD_EXPECTED_ASSIGN_USER_UUID);
        String assignUserUuid = (String) delegateExecution.getVariable(VariableName.LEAD_ASSIGNED_USER_UUID);
        Map<String, String> policyNameMap = (Map) delegateExecution.getVariable(VariableName.POLICY_NAMES);
        List<String> policyNames = policyNameMap == null ? null : new ArrayList<>(policyNameMap.values());
        String agentUuid = null;
            try {
                agentUuid = assignLeadToAgentUc.process(leadUuid, policyNames, assignUserUuid, expectedAssignUserUuid, null);
            } catch (Exception ex) {
                log.warn("AutoAssignLeadToAgentDelegate with lead_uuid {} error: ", leadUuid, ex);
            }
        delegateExecution.setVariables(Collections.singletonMap(VariableName.LEAD_ASSIGNED_USER_UUID, agentUuid));
        logExistDelegate("AutoAssignLeadToAgentDelegate");
    }
}
