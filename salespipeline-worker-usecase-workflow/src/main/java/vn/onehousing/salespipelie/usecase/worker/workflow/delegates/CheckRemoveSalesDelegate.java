package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class CheckRemoveSalesDelegate extends BaseDelegate {
    private final ObjectMapper objectMapper;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("AutoAssignLeadToAgentDelegate");
        List<String> saleRoles = new ArrayList<>();
        if (delegateExecution.hasVariable(VariableName.SALE_ROLES)) ;
        saleRoles = objectMapper.readValue((String) delegateExecution.getVariable(VariableName.SALE_ROLES), new TypeReference<>() {
        });
        log.info("CheckRemoveSalesDelegate SaleRoles {}", saleRoles);
        delegateExecution.setVariables(Collections.singletonMap(VariableName.SALE_ROLES, saleRoles.isEmpty() ? "null" : saleRoles));
        logExistDelegate("AutoAssignLeadToAgentDelegate");
    }
}
