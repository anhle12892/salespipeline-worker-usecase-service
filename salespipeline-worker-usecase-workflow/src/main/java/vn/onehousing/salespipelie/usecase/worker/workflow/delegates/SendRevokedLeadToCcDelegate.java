package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.sendRevokedLeadToCCuc.ISendRevokedLeadToCc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.io.IOException;

@Slf4j
@Component
@AllArgsConstructor
public class SendRevokedLeadToCcDelegate extends BaseDelegate {
    private final ISendRevokedLeadToCc sendRevokedLeadToCc;

    @Override
    public void process(DelegateExecution delegateExecution) throws IOException {
        logEnterDelegate("SendRevokedLeadToCcDelegate");
        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String dropStatus = delegateExecution.getVariable(VariableName.DROP_STATUS).toString();
        String dropAssignedUserUuid = String.valueOf(delegateExecution.getVariable(VariableName.LEAD_ASSIGNED_USER_UUID));
        String revokedReason = "";
        if (delegateExecution.getVariable(VariableName.REVOKED_REASON) != null) {
            revokedReason = delegateExecution.getVariable(VariableName.REVOKED_REASON).toString();
        }
        sendRevokedLeadToCc.process(leadUuid, dropAssignedUserUuid, dropStatus, revokedReason);
        logExistDelegate("SendRevokedLeadToCcDelegate");
    }
}
