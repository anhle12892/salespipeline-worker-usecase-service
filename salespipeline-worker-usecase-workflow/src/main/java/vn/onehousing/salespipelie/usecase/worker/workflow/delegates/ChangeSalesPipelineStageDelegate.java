package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestage.ChangeSalesPipelineStageUcReq;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestage.IChangeSalesPipelineStageUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class ChangeSalesPipelineStageDelegate extends BaseDelegate {

    private final IChangeSalesPipelineStageUc changeSalesPipelineStageUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {

        logEnterDelegate("ChangeSalesPipelineStageDelegate");

        String salePipelineId = delegateExecution.getVariable(VariableName.SALESPIPELINE_UUID).toString();

        Map<String, Object> maps = (Map<String, Object>) delegateExecution.getVariable(InputName.INPUT_CHANGE_DATA);
        String salePipelineStage = maps.get(VariableName.SALESPIPELINE_STAGE).toString();

        changeSalesPipelineStageUc.process(new ChangeSalesPipelineStageUcReq(salePipelineId, salePipelineStage));
        logExistDelegate("ChangeSalesPipelineStageDelegate");
//        log.info("Service Task {} completed with lead_uuid {}", "change-sales-pipeline-stage", delegateExecution.getVariable(VariableName.LEAD_UUID));
    }
}
