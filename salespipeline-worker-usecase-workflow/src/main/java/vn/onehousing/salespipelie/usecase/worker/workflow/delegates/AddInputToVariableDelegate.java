package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class AddInputToVariableDelegate extends BaseDelegate {
    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("AddInputToVariableDelegate");
        Map<String,Object> maps = (Map<String,Object>)delegateExecution.getVariable(InputName.INPUT_DATA);
        if(maps.containsKey(VariableName.LEAD_QUALIFY)) {
            delegateExecution.setVariable(
                VariableName.LEAD_QUALIFY,
                Boolean.parseBoolean(maps.get(VariableName.LEAD_QUALIFY).toString())
            );
        }

        if(maps.containsKey(VariableName.IS_ASSIGNED)) {
            delegateExecution.setVariable(
                VariableName.IS_ASSIGNED,
                    Boolean.parseBoolean(maps.get(VariableName.IS_ASSIGNED).toString())
            );
        }

        if (maps.containsKey(VariableName.IS_CAN_QUALIFY_BY_CC)) {
            delegateExecution.setVariable(
                    VariableName.IS_CAN_QUALIFY_BY_CC,
                    Boolean.parseBoolean(maps.get(VariableName.IS_CAN_QUALIFY_BY_CC).toString())
            );
        }

        if (maps.containsKey(VariableName.AGENT_NEED_REVOKED)) {
            delegateExecution.setVariable(
                    VariableName.AGENT_NEED_REVOKED,
                    Boolean.parseBoolean(maps.get(VariableName.AGENT_NEED_REVOKED).toString())
            );
        }


        logExistDelegate("AddInputToVariableDelegate");
    }
}
