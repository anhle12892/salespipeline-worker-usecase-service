package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.createopportunityuc.ICreateOpportunityUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class CreateOpportunityDelegate extends BaseDelegate {

    private final ICreateOpportunityUc createOpportunityUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {

        logEnterDelegate("CreateOpportunityDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String salespipelineUuid = delegateExecution.getVariable(VariableName.SALESPIPELINE_UUID).toString();

//        log.info("Service Task {} starting with lead_uuid {}", "create-opportunity", leadUuid);

        var resp = createOpportunityUc.process(leadUuid, salespipelineUuid);

        Map<String, Object> variables = new HashMap<>();
        variables.put(VariableName.OPPORTUNITY_UUID, resp.getOpportunityUuid());
        variables.put(VariableName.OPPORTUNITY_CODE, resp.getOpportunityCode());

//        log.info("Service Task {} completed with lead_uuid {}", "create-opportunity", leadUuid);

        delegateExecution.setVariables(variables);
        logExistDelegate("CreateOpportunityDelegate");
    }
}
