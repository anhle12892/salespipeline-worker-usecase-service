package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.requestcheckduplicateuc.IRequestCheckDuplicateUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

@Slf4j
@Component
@AllArgsConstructor
public class CheckLeadDuplicateDelegate extends BaseDelegate {

    private final IRequestCheckDuplicateUc requestCheckDuplicateUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("CheckLeadDuplicateDelegate");
        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String duplicationRuleCode = delegateExecution.getVariable(VariableName.DUPLICATION_RULE_CODE).toString();
        requestCheckDuplicateUc.process(leadUuid,duplicationRuleCode);
        logExistDelegate("CheckLeadDuplicateDelegate");
    }
}
