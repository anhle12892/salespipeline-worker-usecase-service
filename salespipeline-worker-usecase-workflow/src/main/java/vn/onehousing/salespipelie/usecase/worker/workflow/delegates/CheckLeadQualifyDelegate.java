package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadqualifyuc.ICheckLeadQualifyUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Collections;

@Slf4j
@Component
@AllArgsConstructor
public class CheckLeadQualifyDelegate extends BaseDelegate {

    private final ICheckLeadQualifyUc checkLeadQualifyUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("CheckLeadQualifyDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();

        boolean isCanBeQualified = checkLeadQualifyUc.process(leadUuid);

        log.info(
            "lead_uuid [{}] - can be qualified: [{}]",
            leadUuid,
            isCanBeQualified
        );

        delegateExecution.setVariables(Collections.singletonMap(VariableName.LEAD_QUALIFY, isCanBeQualified));
        logExistDelegate("CheckLeadQualifyDelegate");
    }

}
