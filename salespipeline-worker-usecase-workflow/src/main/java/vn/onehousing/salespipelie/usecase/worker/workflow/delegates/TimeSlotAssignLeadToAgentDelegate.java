package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc.ITimeSlotAssignLeadToAgentUc;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.TimeSlotAssignLead;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class TimeSlotAssignLeadToAgentDelegate extends BaseDelegate {

    private final ITimeSlotAssignLeadToAgentUc timeSlotAssignLeadToAgentUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("TimeSlotAssignLeadToAgentDelegate");
        String leadUuid = (String) delegateExecution.getVariable(VariableName.LEAD_UUID);

        Map<String, Object> maps = (Map<String, Object>) delegateExecution.getVariable(InputName.INPUT_CHANGE_DATA);

        LocalDateTime now = LocalDateTime.now();
        log.info("TimeSlotAssignLeadToAgentDelegate lead_uuid {} - date time now: {}", leadUuid, now);
        TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
            .startTime(maps.get(VariableName.TimeSlot.START_TIME).toString())
            .endTime(maps.get(VariableName.TimeSlot.END_TIME).toString())
            .startDay(maps.get(VariableName.TimeSlot.START_DAY).toString())
            .endDay(maps.get(VariableName.TimeSlot.END_DAY).toString())
            .dateTimeNow(now)
            .build();

        boolean isAllowAssign = timeSlotAssignLeadToAgentUc.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);
        boolean isMultipleAssign = Boolean.parseBoolean(maps.get(VariableName.IS_MULTIPLE_ASSIGN).toString());
        delegateExecution.setVariables(Collections.singletonMap(VariableName.IS_ALLOW_ASSIGN, isAllowAssign));
        delegateExecution.setVariables(Collections.singletonMap(VariableName.IS_MULTIPLE_ASSIGN, isMultipleAssign));

        logExistDelegate("TimeSlotAssignLeadToAgentDelegate");
    }
}
