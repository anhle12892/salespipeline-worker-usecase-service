package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.SPLeadOpp;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestatusuc.ChangeSalesPipelineStatusReq;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestatusuc.IChangeSalesPipelineStatusUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.MissingVariableException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class ChangeSalesPipelineStatusDelegate extends BaseDelegate {

    private final IChangeSalesPipelineStatusUc changeSalesPipelineStatusUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws Exception {
        logEnterDelegate("ChangeLeadStatusDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String oppUuid = null;
        if (Objects.nonNull(delegateExecution.getVariable(VariableName.OPPORTUNITY_UUID))) {
            oppUuid = delegateExecution.getVariable(VariableName.OPPORTUNITY_UUID).toString();
        }

        Map<String, Object> maps = (Map<String, Object>) delegateExecution.getVariable(InputName.INPUT_CHANGE_DATA);

        String status = maps.get(VariableName.STATUS).toString();
        // prepare for case fire event when lead qualify success or false
        boolean leadQualify = false;
        try {
            leadQualify = Boolean.parseBoolean(delegateExecution.getVariable(VariableName.LEAD_QUALIFY).toString());
        } catch (Exception ex) {

        }

        if (status.equals("BACK TO DROP STATUS")) {
            status = (String) delegateExecution.getVariable(VariableName.DROP_STATUS);
            if (StringUtils.isEmpty(status)) {
                throw new MissingVariableException(
                    String.format("ChangeSalesPipelineStatusDelegate.process - variable: [%s] - missing", VariableName.DROP_STATUS)
                );
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put(VariableName.LEAD_ASSIGNED_USER_UUID, getNullableVariable(VariableName.LEAD_ASSIGNED_USER_UUID, delegateExecution));
        data.put(VariableName.LEAD_QUALIFY, leadQualify);
        data.put(VariableName.LEAD_SOURCE, delegateExecution.getVariable(VariableName.LEAD_SOURCE));
        data.put(VariableName.LEAD_CHANNEL, delegateExecution.getVariable(VariableName.LEAD_CHANNEL));

        String spLeadOpp = changeSalesPipelineStatusUc.process(
            new ChangeSalesPipelineStatusReq(
                leadUuid,
                oppUuid,
                status,
                data
            )
        );

        if (SPLeadOpp.LEAD.equals(spLeadOpp)) {
            delegateExecution.setVariables(Collections.singletonMap(VariableName.LEAD_STATUS, status));
        }
        if (SPLeadOpp.OPP.equals(spLeadOpp)) {
            delegateExecution.setVariables(Collections.singletonMap(VariableName.OPP_STATUS, status));
        }
        logExistDelegate("ChangeLeadStatusDelegate");
    }

}
