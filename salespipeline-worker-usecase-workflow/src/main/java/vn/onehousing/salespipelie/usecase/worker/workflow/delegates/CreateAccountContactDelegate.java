package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.createaccountcontactuc.ICreateAccountContactUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;


@Slf4j
@Component
@AllArgsConstructor
public class CreateAccountContactDelegate extends BaseDelegate {

    private final ICreateAccountContactUc createAccountContactOpportunityUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {

        logEnterDelegate("CreateAccountContactDelegate");

        String leadUuid = delegateExecution.getVariable(VariableName.LEAD_UUID).toString();
        String salespipelineUuid = delegateExecution.getVariable(VariableName.SALESPIPELINE_UUID).toString();

        var resp = createAccountContactOpportunityUc.process(leadUuid, salespipelineUuid);

//        log.info("Service Task {} completed with lead_uuid {}", "create-account-contact", leadUuid);
        logExistDelegate("CreateAccountContactDelegate");
    }
}
