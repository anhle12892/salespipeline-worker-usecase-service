package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;

import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class ReloadProcessInstanceIdDelegate extends BaseDelegate {

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("ReloadProcessInstanceIdDelegate");
        logExistDelegate("ReloadProcessInstanceIdDelegate");
    }

}
