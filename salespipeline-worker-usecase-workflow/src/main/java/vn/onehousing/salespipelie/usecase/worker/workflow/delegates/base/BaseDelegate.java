package vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeprocessinstanceiduc.IChangeProcessInstanceId;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.HousingException;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.MissingVariableException;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
public abstract class BaseDelegate implements JavaDelegate {

    @Autowired
    private IChangeProcessInstanceId iChangeProcessInstanceId;

    @Autowired
    private ObjectMapper objectMapper;

    private String jsonVariables;
    private DelegateExecution delegateExecution;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        try {

            this.delegateExecution = delegateExecution;
            this.jsonVariables = objectMapper.writeValueAsString(delegateExecution.getVariables());

            validateVariables(delegateExecution);

            // check whether or not process_instance_id have been changed
            boolean isChangedProcessInstanceId = checkChangedProcessInstanceId(delegateExecution);
            if (isChangedProcessInstanceId) {
                iChangeProcessInstanceId.process(
                    delegateExecution.getVariable(VariableName.SALES_PIPELINE_WORKFLOW_ID).toString(),
                    delegateExecution.getProcessInstanceId()
                );
                log.info(
                    "ProcessDefinitionId: {} - new ProcessInstanceId: {}",
                    delegateExecution.getProcessDefinitionId(),
                    delegateExecution.getProcessInstanceId()
                );
                delegateExecution.setVariables(Collections.singletonMap(VariableName.PROCESS_INSTANCE_ID, delegateExecution.getProcessInstanceId()));
            }

            process(delegateExecution);

        } catch (ProcessInstanceNotFoundException ex) {
            logException("ProcessInstanceNotFoundException", ex, delegateExecution);
            throw ex;
        } catch (RequestThirdPartyException ex) {
            logException("RequestThirdPartyException", ex, delegateExecution);
            throw ex;
        } catch (HousingException ex) {
            logException("HousingException", ex, delegateExecution);
            throw ex;
        } catch (NullPointerException ex) {
            logException("NullPointerException", ex, delegateExecution);
            throw ex;
        } catch (Exception ex) {
            logException("Exception", ex, delegateExecution);
            throw ex;
        }
    }

    private void logException(String name, Exception ex, DelegateExecution delegateExecution) {
        log.error("ProcessDefinitionId: [{}] - ProcessBusinessKey: [{}] - {}: [{}] - Variables: [{}]", delegateExecution.getProcessDefinitionId(), delegateExecution.getProcessBusinessKey(), name, ex, this.jsonVariables, ex);
    }

    public void logEnterDelegate(String name) {
        log.info("Start {} - Variables: [{}]", name, this.jsonVariables);
    }

    public void logExistDelegate(String name) throws JsonProcessingException {
        this.jsonVariables = objectMapper.writeValueAsString(delegateExecution.getVariables());
        log.info("Exist {} - Variables: [{}]", name, this.jsonVariables);
    }

//    private void logEnterServiceTask(DelegateExecution externalTask){
//
//        var data = externalTask.getVariables();
//
//        String leadUuid = "";
//        String oppUuid = "";
//
//        if(data.containsKey(VariableName.LEAD_UUID))
//            leadUuid = data.get(VariableName.LEAD_UUID).toString();
//
//        if(data.containsKey(VariableName.OPPORTUNITY_UUID))
//            oppUuid = data.get(VariableName.OPPORTUNITY_UUID).toString();
//
//        log.info(
//            "ProcessInstanceId: {}  - ProcessDefinitionId: {} - Lead_UUID: {} - Opp_UUID: {}",
//            externalTask.getProcessInstanceId(),
//            externalTask.getProcessDefinitionId(),
//            leadUuid,
//            oppUuid
//        );
//    }

    private void validateVariables(DelegateExecution externalTask) {

        Map<String, Object> allVariables = externalTask.getVariables();
        if (!allVariables.containsKey(InputName.INPUT_VALIDATION))
            return;

        List<String> keys = (List<String>) allVariables.get(InputName.INPUT_VALIDATION);

        for (String key : keys) {
            if (allVariables.containsKey(key))
                continue;
            throw new MissingVariableException(
                String.format("variable: [%s] - missing", externalTask.getProcessDefinitionId(), key)
            );

        }
    }

    public String getNullableVariable(String variableName, DelegateExecution delegateExecution) {
        try {
            return delegateExecution.getVariable(variableName).toString();
        } catch (Exception ex) {
            return "";
        }
    }

    private boolean checkChangedProcessInstanceId(DelegateExecution externalTask) {
        String cachedProcessInstanceId = externalTask.getVariable(VariableName.PROCESS_INSTANCE_ID).toString();
        String currentProcessInstanceId = externalTask.getProcessInstanceId();
        return !cachedProcessInstanceId.equals(currentProcessInstanceId);
    }

    public abstract void process(DelegateExecution delegateExecution) throws Exception;
}
