package vn.onehousing.salespipelie.usecase.worker.workflow.delegates;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.InputName;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc.ChangeOpportunityStatusUcReq;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc.IChangeOpportunityStatusUc;
import vn.onehousing.salespipelie.usecase.worker.workflow.delegates.base.BaseDelegate;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.MissingVariableException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class ChangeOpportunityStatusDelegate extends BaseDelegate {

    private final IChangeOpportunityStatusUc updateOpportunityStatusUc;

    @Override
    public void process(DelegateExecution delegateExecution) throws JsonProcessingException {
        logEnterDelegate("ChangeOpportunityStatusDelegate");

        String opportunityId = delegateExecution.getVariable(VariableName.OPPORTUNITY_UUID).toString();

        Map<String, Object> input = (Map<String, Object>) delegateExecution.getVariable(InputName.INPUT_CHANGE_DATA);

        String oppStatus = input.get(VariableName.OPP_STATUS).toString();
        if (oppStatus.equals("BACK TO DROP STATUS")) {
            oppStatus = (String) delegateExecution.getVariable(VariableName.DROP_STATUS);
            if (StringUtils.isEmpty(oppStatus)) {
                throw new MissingVariableException(
                    String.format("ChangeOpportunityStatusDelegate.process - variable: [%s] - missing", VariableName.DROP_STATUS)
                );
            }
        }

        Map<String, Object> data = new HashMap<>();
        data.put(VariableName.REVOKED_REASON, getNullableVariable(VariableName.REVOKED_REASON, delegateExecution));
        data.put(VariableName.REVOKED_TYPE, getNullableVariable(VariableName.REVOKED_TYPE, delegateExecution));
        data.put(VariableName.LEAD_ASSIGNED_USER_UUID, getNullableVariable(VariableName.LEAD_ASSIGNED_USER_UUID, delegateExecution));
        data.put(VariableName.LEAD_SOURCE, delegateExecution.getVariable(VariableName.LEAD_SOURCE));
        data.put(VariableName.LEAD_CHANNEL, delegateExecution.getVariable(VariableName.LEAD_CHANNEL));
        
        updateOpportunityStatusUc.process(
            new ChangeOpportunityStatusUcReq(
                delegateExecution.getVariable(VariableName.LEAD_UUID).toString(),
                opportunityId,
                oppStatus,
                data
            ));
        delegateExecution.setVariables(Collections.singletonMap(VariableName.OPPORTUNITY_STATUS, oppStatus));
        logExistDelegate("ChangeOpportunityStatusDelegate");
    }
}
