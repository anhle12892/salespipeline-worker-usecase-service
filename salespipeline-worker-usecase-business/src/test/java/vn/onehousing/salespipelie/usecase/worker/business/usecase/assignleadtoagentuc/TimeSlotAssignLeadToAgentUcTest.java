package vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.TimeSlotAssignLead;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TimeSlotAssignLeadToAgentUcTest {

	private TimeSlotAssignLeadToAgentUc timeSlotAssignLeadToAgentUcUnderTest;

	@BeforeEach
	void setUp() {
		timeSlotAssignLeadToAgentUcUnderTest = new TimeSlotAssignLeadToAgentUc();
	}

	@Test
	void testIsAllowAssignLeadToAgentInTimeSlot() {
		String leadUuid = "b826dd2c-81f4-454e-ae3b-81780fd51c00";
		String startTime = "08:00:00";
		String endTime = "21:00:00";
		String startDay = "MONDAY";
		String endDay = "SUNDAY";

		String str = "2021-10-22 11:30"; // 11:30 AM FRIDAY
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTimeNow = LocalDateTime.parse(str, formatter);

		TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
			.startTime(startTime)
			.endTime(endTime)
			.startDay(startDay)
			.endDay(endDay)
			.dateTimeNow(dateTimeNow)
			.build();

		boolean isAllowAssign = timeSlotAssignLeadToAgentUcUnderTest
			.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);

		assertTrue(isAllowAssign);
	}

	@Test
	void testIsAllowAssignLeadToAgentInTimeSlotEqualLastDayWeekDay() {
		String leadUuid = "b826dd2c-81f4-454e-ae3b-81780fd51c00";
		String startTime = "08:00:00";
		String endTime = "21:00:00";
		String startDay = "MONDAY";
		String endDay = "SUNDAY";

		String str = "2021-10-24 11:30"; // 11:30 AM SUNDAY
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTimeNow = LocalDateTime.parse(str, formatter);

		TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
			.startTime(startTime)
			.endTime(endTime)
			.startDay(startDay)
			.endDay(endDay)
			.dateTimeNow(dateTimeNow)
			.build();

		boolean isAllowAssign = timeSlotAssignLeadToAgentUcUnderTest
			.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);

		assertTrue(isAllowAssign);
	}

	@Test
	void testIsAllowAssignLeadToAgentInTimeSlotEqualStartDayWeekDay() {
		String leadUuid = "b826dd2c-81f4-454e-ae3b-81780fd51c00";
		String startTime = "08:00:00";
		String endTime = "21:00:00";
		String startDay = "MONDAY";
		String endDay = "SUNDAY";

		String str = "2021-10-18 11:30"; // 11:30 AM MONDAY
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTimeNow = LocalDateTime.parse(str, formatter);

		TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
			.startTime(startTime)
			.endTime(endTime)
			.startDay(startDay)
			.endDay(endDay)
			.dateTimeNow(dateTimeNow)
			.build();

		boolean isAllowAssign = timeSlotAssignLeadToAgentUcUnderTest
			.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);

		assertTrue(isAllowAssign);
	}

	@Test
	void testIsAllowAssignLeadToAgentInTimeSlotFalseDayOfWeekRange() {
		String leadUuid = "b826dd2c-81f4-454e-ae3b-81780fd51c00";
		String startTime = "08:00:00";
		String endTime = "21:00:00";
		String startDay = "MONDAY";
		String endDay = "FRIDAY";
		String str = "2021-10-24 11:30"; // 11:30 AM SUNDAY
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTimeNow = LocalDateTime.parse(str, formatter);

		TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
			.startTime(startTime)
			.endTime(endTime)
			.startDay(startDay)
			.endDay(endDay)
			.dateTimeNow(dateTimeNow)
			.build();

		boolean isAllowAssign = timeSlotAssignLeadToAgentUcUnderTest
			.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);

		assertFalse(isAllowAssign);
	}

	@Test
	void testIsAllowAssignLeadToAgentInTimeSlotFalseTimeRange() {
		String leadUuid = "b826dd2c-81f4-454e-ae3b-81780fd51c00";
		String startTime = "08:00:00";
		String endTime = "21:00:00";
		String startDay = "MONDAY";
		String endDay = "FRIDAY";
		String str = "2021-10-20 21:30"; // 21:30 PM WEDNESDAY
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTimeNow = LocalDateTime.parse(str, formatter);

		TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
			.startTime(startTime)
			.endTime(endTime)
			.startDay(startDay)
			.endDay(endDay)
			.dateTimeNow(dateTimeNow)
			.build();

		boolean isAllowAssign = timeSlotAssignLeadToAgentUcUnderTest
			.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);

		assertFalse(isAllowAssign);
	}

	@Test
	void testIsAllowAssignLeadToAgentInTimeSlotFalseFromat() {
		String leadUuid = "b826dd2c-81f4-454e-ae3b-81780fd51c00";
		String startTime = "fasfd";
		String endTime = "asfsadf";
		String startDay = "safd";
		String endDay = "safdsa";
		String str = "2021-10-20 21:30"; // 21:30 PM WEDNESDAY
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateTimeNow = LocalDateTime.parse(str, formatter);

		TimeSlotAssignLead timeSlotAssignLead = TimeSlotAssignLead.builder()
			.startTime(startTime)
			.endTime(endTime)
			.startDay(startDay)
			.endDay(endDay)
			.dateTimeNow(dateTimeNow)
			.build();

		boolean isAllowAssign = timeSlotAssignLeadToAgentUcUnderTest
			.isAllowAssignLeadToAgentInTimeSlot(leadUuid, timeSlotAssignLead);

		assertFalse(isAllowAssign);
	}
}


