package vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleaduc.IGetTaskByLeadUc;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleadud.IGetTasksByLeadUc;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.factory.ActivityHandlerFactory;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ErrorMessage;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.IErrorMessageRepository;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.CannotFindTaskException;

import java.time.Instant;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class CompleteUserTaskUc implements ICompleteUserTaskUc {

    private final ObjectMapper mapper;
    private final IGetTasksByLeadUc getListTaskByLeadUc;
    private final IGetTaskByLeadUc getTaskByLeadUc;
    private final IErrorMessageRepository errorMessageRepository;
    private final ActivityHandlerFactory activityHandlerFactory;


    @Override
    public Object process(String activity, Map<String, Object> data) throws Exception {

        String leadUuid = data.get(VariableName.LEAD_UUID).toString();

        Task task = getTaskByLeadUc.process(leadUuid, activity);
        if (task == null) {
            throw new CannotFindTaskException(
                    String.format(
                            "cannot find task - lead_uuid: [%s] - activity: [%s]",
                            leadUuid,
                            activity
                    )
            );
        }

        log.info("start process activity [{}] - lead_uuid: [{}] - data: [{}] - time: [{}]", activity, leadUuid, mapper.writeValueAsString(data), Instant.now().toEpochMilli());
        IActivityHandler handler = activityHandlerFactory.getFactory().get(activity);
        var resp = handler.handler(activity, task, data);
        log.info("end process activity [{}] - lead_uuid: [{}] - time: [{}]", activity, leadUuid, Instant.now().toEpochMilli());

        return resp;
    }

//    @Override
//    public Object process(String activity, Map<String,Object> data) throws Exception {
//
//        String leadUuid = data.get(VariableName.LEAD_UUID).toString();
//        Map<String, Task> tasks = getListTaskByLeadUc.process(leadUuid);
//
//        if(tasks.size() == 0 || !tasks.containsKey(activity)){
//            // save message for next time execute
//            saveErrorMessage(activity,data,null);
//            throw new CannotFindTaskException(
//                String.format(
//                    "cannot find task - lead_uuid: [%s] - activity: [%s]",
//                    leadUuid,
//                    activity
//                )
//            );
//        }
//
//        if(!activityHandlerFactory.getFactory().containsKey(activity)){
//            // save message for next time execute
//            saveErrorMessage(activity,data,null);
//            throw new MissingHandlerException(
//                String.format(
//                    "cannot find handler -  lead_uuid: [%s] - activity: [%s]",
//                    leadUuid,
//                    activity
//                )
//            );
//        }
//
//        try{
//            Task task = tasks.get(activity);
//            log.info("start process activity [{}] - lead_uuid: [{}] - data: [{}] - time: [{}]",activity,leadUuid,mapper.writeValueAsString(data), Instant.now().toEpochMilli());
//            IActivityHandler handler = activityHandlerFactory.getFactory().get(activity);
//            var resp = handler.handler(activity, task, data);
//            log.info("end process activity [{}] - lead_uuid: [{}] - time: [{}]",activity,leadUuid,Instant.now().toEpochMilli());
//            // remove error message if exist
//            removeErrorMessage(activity,data);
//
//            return resp;
//
//        }catch (Exception ex){
//            saveErrorMessage(activity,data,ex);
//            throw ex;
//        }
//    }

    private void saveErrorMessage(String activity, Map<String, Object> data, Exception ex) throws JsonProcessingException {
        int hashCode = data.hashCode();
        ErrorMessage errorMessage = errorMessageRepository.getByActivityAndHashCode(activity, hashCode);
        if (errorMessage == null) {
            errorMessageRepository.create(
                    ErrorMessage
                            .builder()
                            .activity(activity)
                            .message(data)
                            .hashCode(hashCode)
                            .retryTimes(0)
                            .isDeleted(false)
                            .build()
            );
        } else {
            errorMessage.setRetryTimes(errorMessage.getRetryTimes() + 1);
            errorMessageRepository.update(errorMessage);
        }
        if (ex != null) {
            log.error("activity: [{}] - data: [{}] - error: [{}]", activity, mapper.writeValueAsString(data), ex.getStackTrace().toString());
        } else {
            log.error("activity: [{}] - data: [{}]", activity, mapper.writeValueAsString(data));
        }
    }

    private void removeErrorMessage(String activity, Map<String, Object> data) throws JsonProcessingException {
//        log.error("activity: [{}] - data: [{}] - process error",activity,mapper.writeValueAsString(data));
        int hashCode = data.hashCode();
        ErrorMessage errorMessage = errorMessageRepository.getByActivityAndHashCode(activity, hashCode);
        if (errorMessage == null)
            return;

        errorMessage.setRetryTimes(errorMessage.getRetryTimes() + 1);
        errorMessage.setDeleted(true);
        errorMessageRepository.update(errorMessage);
    }
}
