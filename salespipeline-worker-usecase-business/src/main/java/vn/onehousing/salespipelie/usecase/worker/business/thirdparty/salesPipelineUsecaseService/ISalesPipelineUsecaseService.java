package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.Note;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalespipelineAggregateView;

public interface ISalesPipelineUsecaseService {
    CreateAccountResp createAccount(String leadUuid, String salespipelineUuid);

    CreateAccountResp createOpportunity(String leadUuid, String salespipelineUuid);

    SalesPipeline createSalesPipeline(String leadUuid);

    SalesPipeline updateLead(String leadUuid, UpdateSalesPipelineLeadData data);

    Note createNote(String leadUuid, CreateNoteReq data);

    SalespipelineAggregateView getByLeadUuid(String leadUuid);
}
