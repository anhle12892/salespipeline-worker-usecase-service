package vn.onehousing.salespipelie.usecase.worker.domain.entity;

public interface ISalesPipelineWorkflowInstanceHistoryRepository {
    SalesPipelineWorkflowInstanceHistory create(SalesPipelineWorkflowInstanceHistory data);
}
