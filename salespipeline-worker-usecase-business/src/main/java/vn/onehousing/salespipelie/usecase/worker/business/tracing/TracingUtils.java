package vn.onehousing.salespipelie.usecase.worker.business.tracing;

import net.vinid.core.constant.Headers;
import net.vinid.core.context.ThreadContextHolder;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class TracingUtils {

    public static final String CORRELATION_ID = "Correlation-ID";

    private TracingUtils() {
        //
    }

    @NotNull
    public static Map<String, Variable> createMapVariableFromContext() {
        Map<String, Variable> variableStart = new HashMap<>();
        ofNullable(ThreadContextHolder.getTracingContext().getCorrelationId())
                .ifPresent(id -> variableStart.put(CORRELATION_ID, new Variable(DataType.STRING, id)));
        addTracingContextToVariables(variableStart);
        return variableStart;
    }

    public static void addTracingContextToVariables(Map<String, Variable> variableStart) {
        if (variableStart != null) {
            ofNullable(ThreadContextHolder.getTracingContext().getCorrelationId())
                    .ifPresent(id -> variableStart.put(Headers.CORRELATION_HEADER, new Variable(DataType.STRING, id)));
            ofNullable(ThreadContextHolder.getTracingContext().getUserId())
                    .ifPresent(id -> variableStart.put(Headers.NEW_USER_ID_HEADER, new Variable(DataType.STRING, id)));
            ofNullable(ThreadContextHolder.getTracingContext().getCaller())
                    .ifPresent(id -> variableStart.put(Headers.NEW_SERVICE_CLIENT_NAME_HEADER, new Variable(DataType.STRING, id)));
            ofNullable(ThreadContextHolder.getTracingContext().getDeviceId())
                    .ifPresent(id -> variableStart.put(Headers.NEW_DEVICE_ID_HEADER, new Variable(DataType.STRING, id)));
            ofNullable(ThreadContextHolder.getTracingContext().getDeviceSessionId())
                    .ifPresent(id -> variableStart.put(Headers.NEW_DEVICE_SESSION_ID_HEADER, new Variable(DataType.STRING, id)));
            ofNullable(ThreadContextHolder.getTracingContext().getMerchantCode())
                    .ifPresent(id -> variableStart.put(Headers.NEW_MERCHANT_CODE_HEADER, new Variable(DataType.STRING, id)));
        }
    }

    public static void setTracingContext(Map<String, Object> variables) {
        Optional.ofNullable(variables.get(Headers.CORRELATION_HEADER))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().setCorrelationId(id.toString()));
        Optional.ofNullable(variables.get(CORRELATION_ID))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().getMetadata().put(CORRELATION_ID, id.toString()));
        Optional.ofNullable(variables.get(Headers.NEW_USER_ID_HEADER))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().setUserId(id.toString()));
        Optional.ofNullable(variables.get(Headers.NEW_SERVICE_CLIENT_NAME_HEADER))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().setCaller(id.toString()));
        Optional.ofNullable(variables.get(Headers.NEW_DEVICE_ID_HEADER))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().setDeviceId(id.toString()));
        Optional.ofNullable(variables.get(Headers.NEW_DEVICE_SESSION_ID_HEADER))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().setDeviceSessionId(id.toString()));
        Optional.ofNullable(variables.get(Headers.NEW_MERCHANT_CODE_HEADER))
                .ifPresent(id -> ThreadContextHolder.getTracingContext().setMerchantCode(id.toString()));
    }
}
