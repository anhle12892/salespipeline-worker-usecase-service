package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;

import java.util.Map;

@Data
public class CompleteTaskReq {
    public Map<String, Variable> variables;
    @JsonProperty("businessKey")
    public String businessKey;

    public CompleteTaskReq(Map<String, Variable> variables){
        this.variables = variables;
    }

    public CompleteTaskReq(String businessKey,Map<String, Variable> variables){
        this.businessKey = businessKey;
        this.variables = variables;
    }

}
