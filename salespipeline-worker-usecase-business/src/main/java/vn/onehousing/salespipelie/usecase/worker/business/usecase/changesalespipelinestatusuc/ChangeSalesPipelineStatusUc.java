package vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestatusuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.SPLeadOpp;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc.ChangeLeadStatusReq;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc.IChangeLeadStatusUc;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc.ChangeOpportunityStatusUc;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc.ChangeOpportunityStatusUcReq;

@Service
@AllArgsConstructor
public class ChangeSalesPipelineStatusUc implements IChangeSalesPipelineStatusUc {

    private ChangeOpportunityStatusUc changeOpportunityStatusUc;
    private IChangeLeadStatusUc changeLeadStatusUc;

    @Override
    public String process(ChangeSalesPipelineStatusReq req) throws JsonProcessingException {
        String status = req.getStatus();
        String oppUuid = req.getOppUuid();
        String leadUuid = req.getLeadUuid();

        if (StringUtils.isNotBlank(oppUuid)) {
            changeOpportunityStatusUc.process(
                new ChangeOpportunityStatusUcReq(
                    leadUuid,
                    oppUuid,
                    status,
                    req.data
                )
            );
            return SPLeadOpp.OPP;
        } else {
            changeLeadStatusUc.process(
                new ChangeLeadStatusReq(
                    leadUuid,
                    status,
                    req.data
                )
            );
            return SPLeadOpp.LEAD;
        }
    }
}
