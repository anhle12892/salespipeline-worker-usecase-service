package vn.onehousing.salespipelie.usecase.worker.business.usecase.createopportunityuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;

@Service
@AllArgsConstructor
public class CreateOpportunityUc implements ICreateOpportunityUc {

    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ISalesPipelineWorkflowProcessRepository salesPipelineWorkflowProcessRepository;

    @Override
    public SalesPipelineWorkflowInstance process(String leadUuid, String salesPipelineUuid) {
        SalesPipelineWorkflowInstance instance = salesPipelineWorkflowProcessRepository.getByLeadUuId(leadUuid);
        if (instance == null)
            throw new ProcessInstanceNotFoundException(String.format("not found SalesPipelineWorkflowInstance with lead_uuid: [%s]", leadUuid));

        var resp = salesPipelineUsecaseService.createOpportunity(leadUuid, salesPipelineUuid);

        instance.setOpportunityUuid(resp.getOpportunityUuid());
        instance.setOpportunityCode(resp.getOpportunityCode());

        salesPipelineWorkflowProcessRepository.update(instance);

        return instance;
    }
}
