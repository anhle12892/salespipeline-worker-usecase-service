package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.IOpportunityService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class AgentCompleteStageOppHandler implements IActivityHandler {

    private final ObjectMapper objectMapper;
    private final IWorkflowService workflowService;
    private final IOpportunityService opportunityService;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {
        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper, data);
        List<String> saleRoles = completeTaskData.getData().getSaleRoles();
        Map<String, Variable> variableMap = new HashMap<>();
        if (!saleRoles.isEmpty()) {
            variableMap.put(VariableName.SALE_ROLES, new Variable(DataType.STRING, objectMapper.writeValueAsString(saleRoles)));
        }
        var opp = opportunityService.getByLeadUuid(completeTaskData.getLeadUuid());
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, completeTaskData.getLeadUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.DROP_STATUS, new Variable(DataType.STRING, opp.getStatus()));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        log.info("completeTask - IActivityHandler: [AgentCompleteStageHandler] - Lead_UUID: [{}]", completeTaskData.getLeadUuid());
        return null;
    }
}
