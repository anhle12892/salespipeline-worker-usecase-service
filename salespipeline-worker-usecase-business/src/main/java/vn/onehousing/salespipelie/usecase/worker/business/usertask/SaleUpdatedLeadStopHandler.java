package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.RevokedType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadStoppedEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class SaleUpdatedLeadStopHandler implements IActivityHandler {

    private final ObjectMapper objectMapper;
    private final IWorkflowService workflowService;
    private final ILeadService leadService;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final EventPublisher eventPublisher;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper, data);

        Map<String, Variable> variableMap = new HashMap<>();

        Lead lead = leadService.getByUuid(completeTaskData.getLeadUuid());
        List<User> assignedUsers = lead.getAssignedUsers();
        if (CollectionUtils.isEmpty(assignedUsers)) {
            assignedUsers = Collections.singletonList(
                User.builder()
                    .userUuid(lead.getAssignedUserUuid())
                    .userCode(lead.getAssignedUserCode())
                    .build()
            );
        }
        variableMap.put(VariableName.DROP_STATUS, new Variable(DataType.STRING, lead.getStatus()));
        completeTaskData.getData().setUnassignedReason(RevokedType.AGENT_STOP.name());

        var resp = salesPipelineUsecaseService.updateLead(completeTaskData.getLeadUuid(), completeTaskData.getData());

        variableMap.put(
            VariableName.LEAD_STATUS,
            new Variable(DataType.STRING, completeTaskData.getData().getStatus())
        );
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, completeTaskData.getLeadUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.REVOKED_TYPE, new Variable(DataType.STRING, RevokedType.AGENT_STOP.name()));
        variableMap.put(VariableName.REVOKED_REASON, new Variable(DataType.STRING, RevokedType.AGENT_STOP.getLabel()));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));

        for (User user : assignedUsers) {
            eventPublisher.publishEvent(
                new LeadStoppedEvent(
                    resp.getLeadUuid(),
                    lead.getSource(),
                    lead.getChannel(),
                    user.getUserUuid()
                )
            );
        }

        log.info("completeTask - IActivityHandler: [SaleUpdatedLeadStopHandler] - Lead_UUID: [{}]", completeTaskData.getLeadUuid());
        return resp;
    }
}
