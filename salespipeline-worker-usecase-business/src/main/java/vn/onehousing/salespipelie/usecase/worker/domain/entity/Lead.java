package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Lead {
    String leadUuid;

    String leadName;

    String leadCode;

    String assignedUserUuid;

    String assignedUserCode;

    String campaignUuid;

    String campaignCode;

    String status;

    String email;

    Boolean isConverted;

    String referredByUserUuid;

    String referredByUserCode;

    String attributeSetUuid;

    BigDecimal score;

    String priority;

    String unqualifiedReason;

    String leadProfileUuid;

    String accountUuid;

    String accountCode;

    String contactUuid;

    String contactCode;

    String opportunityUuid;

    String opportunityCode;

    String attributeSetId;

    String source;

    String channel;

    String gender;

    String duplicatedLeadUuid;

    String duplicatedLeadCode;

    String expectedAssignedUserCode;

    String expectedAssignUserUuid;

    PhoneNumber phoneNumber;

    List<AttributeValue> attributes;

    List<User> assignedUsers;

    List<User> expectedUsers;
}
