package vn.onehousing.salespipelie.usecase.worker.business.usecase.startsalespipelineworkflowuc;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.context.ThreadContextHolder;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.*;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.ProcessInstance;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.tracing.TracingUtils;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.ICompleteUserTaskUc;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.LeadHaveBeenProcessedException;

import java.time.Instant;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class StartSalePipelineWorkflowUc implements IStartSalePipelineWorkflowUc {

    private final IWorkflowService workflowService;
    private final ISalesPipelineWorkflowProcessRepository repository;
    private final ICompleteUserTaskUc completeUserTaskUc;
    private final ObjectMapper objectMapper;

    @Override
    public void process(Map<String,Object> data) throws Exception {

        Lead lead = objectMapper.convertValue(data, Lead.class);

        log.info("StartSalePipelineWorkflowUc.process - start create workflow - lead_uuid: [{}] - time: [{}]",lead.getLeadUuid(), Instant.now().toEpochMilli());

        if(lead.getStatus().equals(LeadStatus.STOP)
            || lead.getStatus().equals(LeadStatus.SUCCESS)){
            log.warn(
                "not start sales-pipelines workflow with lead_uuid: [{}] - status: [{}]",
                lead.getLeadUuid(),
                lead.getStatus()
            );
            return;
        }

        SalesPipelineWorkflowInstance oldInstance = repository.getByLeadUuId(lead.getLeadUuid());
        if (oldInstance != null)
            throw new LeadHaveBeenProcessedException(
                    String.format(
                            "lead_uuid: [%s] - already started workflow, instance_id: [%s]",
                            lead.getLeadUuid(),
                            oldInstance.getId()
                    )
            );

        SalesPipelineWorkflowInstance salesPipelineWorkflowInstance = repository.create(
                SalesPipelineWorkflowInstance
                        .builder()
                        .leadCode(lead.getLeadCode())
                        .leadUuid(lead.getLeadUuid())
                        .correlationId(ThreadContextHolder.getTracingContext().getCorrelationId())
                        .build()
        );

        Map<String, Variable> variableStart = TracingUtils.createMapVariableFromContext();
        ProcessInstance instance = workflowService.start(
                ProcessKey.SALES_PIPELINE_WORKFLOW,
                new CompleteTaskReq(lead.getLeadUuid(),variableStart)
        );
        log.info("StartSalePipelineWorkflowUc.process - success create process instance: [{}]",objectMapper.writeValueAsString(instance));
        data.put(VariableName.PROCESS_INSTANCE_ID,instance.getId());
        data.put(VariableName.SALES_PIPELINE_WORKFLOW_ID,salesPipelineWorkflowInstance.getId());

        salesPipelineWorkflowInstance.setProcessInstanceId(instance.getId());
        repository.update(salesPipelineWorkflowInstance);

        completeUserTaskUc.process(TaskDefinitionKey.ACTIVITY_LEAD_ENTERED,data);

        log.info("StartSalePipelineWorkflowUc.process - end create workflow - lead_uuid: [{}] - time: [{}]",lead.getLeadUuid(), Instant.now().toEpochMilli());
    }

}
