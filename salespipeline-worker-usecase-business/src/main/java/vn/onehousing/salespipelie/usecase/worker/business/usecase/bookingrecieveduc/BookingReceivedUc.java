package vn.onehousing.salespipelie.usecase.worker.business.usecase.bookingrecieveduc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.ICompleteUserTaskUc;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalespipelineAggregateView;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class BookingReceivedUc implements IBookingReceivedUc {

    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ICompleteUserTaskUc completeUserTaskUc;

    @Override
    public void process(String leadUuid, String status, String activity) throws Exception {
        SalespipelineAggregateView data = salesPipelineUsecaseService.getByLeadUuid(leadUuid);
        if ("OPPORTUNITY".equals(data.getType())) {
            log.warn("BookingReceivedUc.process - lead_uuid: [{}] - already convert from lead to opp", leadUuid);
            return;
        }

        Map<String, Object> map = new HashMap<>();
        map.put(VariableName.STATUS, status);
        map.put(VariableName.LEAD_UUID, leadUuid);

        completeUserTaskUc.process(activity, map);
    }

}
