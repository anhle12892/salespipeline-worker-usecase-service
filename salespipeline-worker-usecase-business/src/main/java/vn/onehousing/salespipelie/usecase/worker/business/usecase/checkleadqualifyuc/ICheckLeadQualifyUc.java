package vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadqualifyuc;

public interface ICheckLeadQualifyUc {
    boolean process(String leadUuid);
}
