package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.NoteSource;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.IOpportunityService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.CreateNoteReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.RevokeRequest;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Opportunity;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadRevokedEvent;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.InvalidDataException;

import java.util.*;

@Slf4j
@Service
@AllArgsConstructor
public class OPSManualRevokedOppHandler implements IActivityHandler {

    private final IWorkflowService workflowService;
    private final ObjectMapper objectMapper;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final EventPublisher eventPublisher;
    private final IOpportunityService opportunityService;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        String leadUuid = data.get("lead_uuid").toString();
        RevokeRequest req = objectMapper.convertValue(data, RevokeRequest.class);
        if (StringUtils.isEmpty(req.getRevokedType())) {
            throw new InvalidDataException("revoked_type is not null");
        }

        Map<String, Variable> variableMap = new HashMap<>();

        Opportunity opp = opportunityService.getByLeadUuid(leadUuid);
        List<User> assignedUsers = opp.getAssignedUsers();
        if (CollectionUtils.isEmpty(assignedUsers)) {
            assignedUsers = Collections.singletonList(
                User.builder()
                    .userUuid(opp.getAssignedUserUuid())
                    .userCode(opp.getAssignedUserCode())
                    .build()
            );
        }

        variableMap.put(VariableName.DROP_STATUS, new Variable(DataType.STRING, opp.getStatus()));

        UpdateSalesPipelineLeadData salesPipelineLeadData = UpdateSalesPipelineLeadData.builder()
            .unassignedReason(req.getRevokedType())
            .assignedUserUuid("")
            .assignedUserCode("")
            .assignedUsers(new ArrayList<>())
            .build();
        var resp = salesPipelineUsecaseService.updateLead(leadUuid, salesPipelineLeadData);

        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, leadUuid));
        variableMap.put(VariableName.OPPORTUNITY_UUID, new Variable(DataType.STRING, opp.getOpportunityUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.REVOKED_TYPE, new Variable(DataType.STRING, req.getRevokedType()));
        variableMap.put(VariableName.REVOKED_REASON, new Variable(DataType.STRING, req.getRevokedReason()));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));

        for (User user : assignedUsers) {
            eventPublisher.publishEvent(
                LeadRevokedEvent.builder()
                    .leadUuid(opp.getLeadUuid())
                    .opportunityUuid(opp.getOpportunityUuid())
                    .revokedReason(req.getRevokedReason())
                    .revokedType(req.getRevokedType())
                    .channel(opp.getChannel())
                    .source(opp.getSource())
                    .revokedUserUuid(user.getUserUuid())
                    .build());
        }

        if (StringUtils.isNotEmpty(req.getRevokedReason())) {
            salesPipelineUsecaseService.createNote(leadUuid, buildBodyCreateNote(opp.getLeadUuid(), req.getRevokedReason()));
        }
        log.info("completeTask - IActivityHandler: [OPSManualRevokedOppHandler] - Lead_UUID: [{}]", resp.getLeadUuid());
        return resp;
    }

    private CreateNoteReq buildBodyCreateNote(String leadUuid, String revokedReason) {
        return CreateNoteReq.builder()
            .parentUuid(leadUuid)
            .parentType("LEAD")
            .ownerType(NoteSource.BACK_OFFICE)
            .content(revokedReason)
            .build();
    }
}
