package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.documentservice;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.Note;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IDocumentService {
    Optional<List<Note>> getNotesBy(List<String> parentUUID, String parentType) throws IOException;
}
