package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.Instant;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Note {
    Long id;
    String noteUuid;
    String noteCode;
    String ownerUuid;
    String parentUuid;
    String ownerType;
    String ownerEmail;
    String title;
    String content;
    Instant createdDate;
    String createdBy;
    Instant lastModifiedDate;
    String lastModifiedBy;
}
