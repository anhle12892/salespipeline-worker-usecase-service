package vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.v2;

import java.util.Map;

public interface ICompleteUserTaskV2Uc {
    Object process(String activity, Map<String, Object> data) throws Exception;
}
