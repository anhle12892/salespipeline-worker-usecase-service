package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class VariableName {
    public static final String LEAD_UUID = "lead_uuid";
    public static final String LEAD_CODE = "lead_code";
    public static final String LEAD_EXPECTED_ASSIGN_USER_UUID = "lead_expected_assign_user_uuid";
    public static final String LEAD_ASSIGNED_USER_UUID = "lead_assigned_user_uuid";
    public static final String LEAD_STATUS = "lead_status";
    public static final String OPP_STATUS = "opportunity_status";
    public static final String STATUS = "status";
    public static final String LEAD_SOURCE = "lead_source";
    public static final String LEAD_CHANNEL = "lead_channel";
    public static final String VALID_EXPECTED_AGENT = "valid_expected_agent";
    public static final String LEAD_ACCOUNT_UUID = "lead_account_uuid";
    public static final String DUPLICATED_LEAD_UUID = "duplicated_lead_uuid";
    public static final String DUPLICATED_LEAD_CODE = "duplicated_lead_code";
    public static final String OPPORTUNITY_UUID = "opportunity_uuid";
    public static final String OPPORTUNITY_CODE = "opportunity_code";
    public static final String OPPORTUNITY_WON = "opportunity_won";
    public static final String OPPORTUNITY_STATUS = "opportunity_status";
    public static final String OPPORTUNITY_IS_SIGN_CONTRACT = "opportunity_is_sign_contract";
    public static final String SALESPIPELINE_UUID = "salespipeline_uuid";
    public static final String SALESPIPELINE_STAGE = "salespipeline_stage";
    public static final String PROCESS_INSTANCE_ID = "process_instance_id";
    public static final String SALES_PIPELINE_WORKFLOW_ID = "sales_pipeline_workflow_id";
    public static final String IS_ASSIGNED = "is_assigned";
    public static final String IS_MULTI_ASSIGNED = "is_multi_assigned";
    public static final String USER_TASK = "user_task";
    public static final String IS_OPP_REVOKED = "is_opp_revoked";
    public static final String IS_LEAD_REVOKED = "is_lead_revoked";
    public static final String REVOKED_TYPE = "revoked_type";
    public static final String REVOKED_REASON = "revoked_reason";
    public static final String BOOKING_CODE = "booking_code";
    public static final String IS_CAN_QUALIFY_BY_CC = "is_can_qualify_by_cc";

    public static final String DUPLICATION_RULE_CODE = "duplication_rule_code";

    public static final String LEAD_DUPLICATE_NEED_CHECK = "lead_duplicate_need_check";
    public static final String LEAD_DUPLICATED = "lead_duplicated";
    public static final String LEAD_QUALIFY = "lead_qualify";
    public static final String LEAD_NEXT_STEP = "lead_next_step";
    public static final String TOPIC_NAME = "topic_name";
    public static final String LEAD_CHECK_DUPLICATE_RESPONSE_STATUS = "lead_check_duplicate_response_status";

    public static final String POLICY_NAMES = "policy_names";
    public static final String IS_ALLOW_ASSIGN = "is_allow_assign";
    public static final String DROP_STATUS = "drop_status";

    public static final String ASSIGNED_USERS = "assigned_users";
    public static final String EXPECTED_USERS = "expected_users";
    public static final String LEAD_STAGE = "lead_stage";
    public static final String SALE_ROLES = "sale_roles";
    public static final String AGENT_NEED_REVOKED = "agent_need_revoked";
    public static final String IS_MULTIPLE_ASSIGN = "is_multiple_assign";

    public static class TimeSlot {
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
        public static final String START_DAY = "start_day";
        public static final String END_DAY = "end_day";
    }
}
