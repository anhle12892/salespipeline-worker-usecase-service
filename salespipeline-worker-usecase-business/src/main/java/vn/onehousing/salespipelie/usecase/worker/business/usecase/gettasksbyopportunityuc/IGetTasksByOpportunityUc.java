package vn.onehousing.salespipelie.usecase.worker.business.usecase.gettasksbyopportunityuc;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;

import java.util.Map;

public interface IGetTasksByOpportunityUc {
    Map<String, Task> process(String opportunityUuid);
}
