package vn.onehousing.salespipelie.usecase.worker.business.usecase.revokeleadoppafterstopuc;

public interface IRevokeLeadOppAfterStopUc {
    void process(String leadUuid, String revokedType, String revokedReason);
}
