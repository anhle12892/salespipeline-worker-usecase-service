package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.Builder;
import lombok.Data;
import net.vinid.core.event.AbstractEvent;

@Data
@Builder
public class LeadRevokedEvent extends AbstractEvent {
    String leadUuid;
    String opportunityUuid;
    String revokedUserUuid;
    String revokedType;
    String revokedReason;
    String source;
    String channel;
}
