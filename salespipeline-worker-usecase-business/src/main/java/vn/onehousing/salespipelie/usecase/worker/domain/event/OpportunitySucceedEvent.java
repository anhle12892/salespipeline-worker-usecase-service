package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.vinid.core.event.AbstractEvent;

@Data
@AllArgsConstructor
public class OpportunitySucceedEvent extends AbstractEvent {
    String leadUuid;
    String opportunityUuid;
    String source;
    String channel;
    String assignedUserUuid;
}
