package vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestage;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ChangeSalesPipelineStageUcReq {
    @Getter
    String salesPipelinesUuid;
    @Getter
    String saleStage;
}
