package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class LeadEnteredHandler implements IActivityHandler {

    private final ObjectMapper objectMapper;
    private final IWorkflowService workflowService;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        Lead lead = objectMapper.convertValue(data, Lead.class);

        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.SALES_PIPELINE_WORKFLOW_ID, new Variable(DataType.STRING, data.get(VariableName.SALES_PIPELINE_WORKFLOW_ID).toString()));
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, lead.getLeadUuid()));
        variableMap.put(VariableName.LEAD_CODE, new Variable(DataType.STRING, lead.getLeadCode()));
        if (StringUtils.isNotBlank(lead.getExpectedAssignUserUuid())) {
            variableMap.put(VariableName.LEAD_EXPECTED_ASSIGN_USER_UUID, new Variable(DataType.STRING, lead.getExpectedAssignUserUuid()));
        } else {
            variableMap.put(VariableName.LEAD_EXPECTED_ASSIGN_USER_UUID, new Variable(DataType.STRING, "null"));
        }
        if (StringUtils.isNotBlank(lead.getAssignedUserUuid())) {
            variableMap.put(VariableName.LEAD_ASSIGNED_USER_UUID, new Variable(DataType.STRING, lead.getAssignedUserUuid()));
        } else {
            variableMap.put(VariableName.LEAD_ASSIGNED_USER_UUID, new Variable(DataType.STRING, "null"));
        }

        if (Objects.nonNull(lead.getAssignedUsers())) {
            variableMap.put(VariableName.ASSIGNED_USERS, new Variable(DataType.STRING, objectMapper.writeValueAsString(lead.getAssignedUsers())));
        } else {
            variableMap.put(VariableName.ASSIGNED_USERS, new Variable(DataType.STRING, ""));
        }
        if (Objects.nonNull(lead.getExpectedUsers())) {
            variableMap.put(VariableName.EXPECTED_USERS, new Variable(DataType.STRING, objectMapper.writeValueAsString(lead.getExpectedUsers())));
        } else {
            variableMap.put(VariableName.EXPECTED_USERS, new Variable(DataType.STRING, ""));
        }

        variableMap.put(VariableName.PROCESS_INSTANCE_ID, new Variable(DataType.STRING, data.get(VariableName.PROCESS_INSTANCE_ID).toString()));
        variableMap.put(VariableName.LEAD_ACCOUNT_UUID, new Variable(DataType.STRING, lead.getAccountUuid()));
        variableMap.put(VariableName.LEAD_SOURCE, new Variable(DataType.STRING, lead.getSource()));
        variableMap.put(VariableName.LEAD_CHANNEL, new Variable(DataType.STRING, lead.getChannel()));
        variableMap.put(VariableName.LEAD_STATUS, new Variable(DataType.STRING, lead.getStatus()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));

        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        return null;
    }
}
