package vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadduplicateduc;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.DuplicationLeadResp;

public interface ICheckLeadDuplicatedUc {
    DuplicationLeadResp process(String leadUuid);
}
