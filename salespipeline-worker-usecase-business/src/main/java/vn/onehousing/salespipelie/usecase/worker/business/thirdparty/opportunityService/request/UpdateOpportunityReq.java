package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@Builder
public class UpdateOpportunityReq {
    private String name;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    @JsonProperty("is_sign_contract")
    private Boolean isSignContract;
    private String status;
}
