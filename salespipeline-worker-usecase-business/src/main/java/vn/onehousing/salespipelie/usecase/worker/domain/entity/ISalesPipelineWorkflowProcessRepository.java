package vn.onehousing.salespipelie.usecase.worker.domain.entity;

public interface ISalesPipelineWorkflowProcessRepository {
    SalesPipelineWorkflowInstance create(SalesPipelineWorkflowInstance data);

    SalesPipelineWorkflowInstance update(SalesPipelineWorkflowInstance data);

    SalesPipelineWorkflowInstance getByLeadUuId(String leadUuId);

    SalesPipelineWorkflowInstance getByOpportunityUuid(String opportunityUuId);

    SalesPipelineWorkflowInstance getByProcessInstanceId(String processInstanceId);

    SalesPipelineWorkflowInstance getById(String id);
}
