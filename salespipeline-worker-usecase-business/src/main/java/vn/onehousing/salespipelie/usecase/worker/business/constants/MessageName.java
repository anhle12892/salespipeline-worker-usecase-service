package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class MessageName {
    public static final String MESSAGE_SALE_UPDATED_LEAD = "Message_Sale_Updated_Lead";
    public static final String MESSAGE_LEAD_DEAD = "Message_Lead_Dead";
    public static final String MESSAGE_BOOKED_TICKET = "Message_Booked_Ticket";
    public static final String MESSAGE_OPPORTUNITY_WON = "Message_Opportunity_Won";
    public static final String MESSAGE_OPPORTUNITY_LOST = "Message_Opportunity_Lost";
    public static final String MESSAGE_OPPORTUNITY_DEAD = "Message_Opportunity_Dead";
    public static final String MESSAGE_REVOKED_SALE_FROM_LEAD = "Message_Revoked_Sale_From_Lead";
    public static final String MESSAGE_LEAD_ENTERED = "Message_Lead_Entered";
}
