package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vinid.core.event.AbstractEvent;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeadRevokedCCEvent extends AbstractEvent {
    private String leadUuid;
    private String name;
    private String phone_number;
    private Project project;
    private AsssignedAgent agent;
    private String previousStatus;

    private String email;
    private String gender;
    private String source;
    private String channel;
    private String demand;
    private List<LeadNote> notes = new ArrayList<>();
    private String userRevokedReason;
}
