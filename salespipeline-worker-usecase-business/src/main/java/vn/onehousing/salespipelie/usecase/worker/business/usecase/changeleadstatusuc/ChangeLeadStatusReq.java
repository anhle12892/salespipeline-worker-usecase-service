package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@AllArgsConstructor
public class ChangeLeadStatusReq {
    @Getter
    String leadUuid;
    @Getter
    String status;
    @Getter
    Map<String, Object> data;
}
