package vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.IAssignmentService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.request.AssignRequest;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.UserDto;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.EvaluateReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.AttributeValue;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.InvalidDataException;

import java.util.*;

@Slf4j
@Service
@AllArgsConstructor
public class AutoAssignLeadToAgentUc implements IAutoAssignLeadToAgentUc {

    private static final String DEFAULT_STAGE = "1";
    private final ILeadService leadService;
    private final IAssignmentService assignmentService;
    private final IWorkflowService workflowService;
    private final ObjectMapper oMapper = new ObjectMapper();

    @Override
    public String process(String leadUuid, List<String> policyNames, String assignUserUuid, String expectedAssignUserUuid,
                          String assignedByUser) throws JsonProcessingException {

        Lead lead = leadService.getByUuid(leadUuid);

        var request = AssignRequest.builder();
        request.leadUuid(leadUuid);
        request.leadSource(lead.getSource());
        request.leadChannel(lead.getChannel());
        request.assignedUserUuid(assignUserUuid);
        request.expectedAssignUserUuid(expectedAssignUserUuid);
        if (Objects.nonNull(assignedByUser)) {
            request.assignedByUser(assignedByUser);
        }

        if (lead.getAttributes() != null) {
            Optional<AttributeValue> optionalProjectUuid = lead.getAttributes().stream().filter(x -> x.getName().equals("project_uuid")).findAny();
            optionalProjectUuid.ifPresent(attributeValue -> request.projectUuid(attributeValue.getValue().toString()));
        }
        request.policyNames(policyNames);

        UserDto userDto = assignmentService.assign(request.build());
        if (userDto == null || StringUtils.isBlank(userDto.getUuid())) {
            return null;
        }
        return userDto.getUuid();
    }

    @Override
    public void processMultiAgent(String leadUuid, String stage,
                                  List<User> expectedAssignUsers,
                                  List<User> assignedByUsers,
                                  List<String> saleRoles,
                                  String assignedByUser) throws Exception {
        Lead lead = leadService.getByUuid(leadUuid);

        var request = AssignRequest.builder();
        request.leadUuid(leadUuid);
        request.leadSource(lead.getSource());
        request.leadChannel(lead.getChannel());
        if (lead.getAttributes() != null) {
            Optional<AttributeValue> optionalProjectUuid = lead.getAttributes().stream().filter(x -> x.getName().equals("project_uuid")).findAny();
            optionalProjectUuid.ifPresent(attributeValue -> request.projectUuid(attributeValue.getValue().toString()));
        }
        if (Objects.nonNull(assignedByUser)) {
            request.assignedByUser(assignedByUser);
        }
        //request.policyNames(policyNames);
        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_SOURCE, new Variable(DataType.STRING, lead.getSource()));
        variableMap.put(VariableName.LEAD_CHANNEL, new Variable(DataType.STRING, lead.getChannel()));
        variableMap.put(VariableName.LEAD_STAGE, new Variable(DataType.STRING, Strings.isBlank(stage) ? DEFAULT_STAGE : stage));
        EvaluateReq evaluateReq = new EvaluateReq(variableMap);

        if (!assignedByUsers.isEmpty()) {
            assignedByUsers.forEach(user -> {
                request.assignedUserUuid(user.getUserUuid());
                if (StringUtils.isNotEmpty(user.getRole())) {
                    request.agentRole(user.getRole());
                } else {
                    // TO-DO get role need to assign
                    String assignedRole = null;
                    request.agentRole(assignedRole);
                }
                try {
                    assignmentService.assignMultiUser(request.build());
                } catch (Exception e) {
                    log.error("AutoAssignLeadToAgentUc.processMultiAgent Error when assign agent to leadUuid with body = {}", request, e);
                    e.printStackTrace();
                }
            });
        } else if (!expectedAssignUsers.isEmpty()) {
            var roles = workflowService.getSaleRoles(evaluateReq);
            var assignedData = filterUsers(expectedAssignUsers, roles, lead.getAssignedUsers());
            if (Objects.nonNull(assignedData) && !assignedData.isEmpty()) {
                assignedData.forEach((key, value) -> {
                    request.agentRole(key);
                    request.expectedAssignUserUuid(value);
                    try {
                        assignmentService.assignMultiUser(request.build());
                    } catch (Exception e) {
                        log.error("AutoAssignLeadToAgentUc.processMultiAgent Error when assign agent to leadUuid with body = {}", request, e);
                        e.printStackTrace();
                    }
                });
            }
        } else {
            // UseCase the first time assigned
            if (saleRoles.isEmpty()) {
                saleRoles = workflowService.getSaleRoles(evaluateReq);
            }
            var assignedData = filterUsers(Collections.emptyList(), saleRoles, lead.getAssignedUsers());
            if (Objects.nonNull(assignedData) && !assignedData.isEmpty()) {
                assignedData.keySet().forEach(role -> {
                    request.agentRole(role);
                    try {
                        assignmentService.assignMultiUser(request.build());
                    } catch (Exception e) {
                        log.error("AutoAssignLeadToAgentUc.processMultiAgent Error when assign agent to leadUuid with body = {}", request, e);
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    private Map<String, String> filterUsers(List<User> expectedUsers, List<String> roles,
                                            List<User> assignedUsers) {
        Map<String, String> assignedData = new HashMap<>();
        assignedUsers = Objects.nonNull(assignedUsers) ? assignedUsers : Collections.emptyList();
        if (roles.isEmpty())
            return null;
        if (!expectedUsers.isEmpty()) {
            var invalidUser = expectedUsers
                    .stream()
                    .filter(user -> Objects.nonNull(user)
                            && (StringUtils.isBlank(user.getRole())
                            || StringUtils.isBlank(user.getUserUuid()))).findFirst();
            if (invalidUser.isPresent())
                throw new InvalidDataException("Expected users have empty role or uuid!");
        }
        for (String role : roles) {
            var expectedUserByRole = expectedUsers
                    .stream()
                    .filter(user -> Objects.nonNull(user)
                            && role.equals(user.getRole())).findFirst();
            var assignedUserByRole = assignedUsers
                    .stream()
                    .filter(user -> Objects.nonNull(user)
                            && role.equals(user.getRole())).findFirst();

            if (assignedUserByRole.isEmpty()) {
                if (expectedUserByRole.isPresent())
                    assignedData.put(role, expectedUserByRole.get().getUserUuid());
                else
                    assignedData.put(role, null);
            }
        }
        return assignedData;
    }
}
