package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.request;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;

import java.util.List;

@Data
@Builder
public class AssignRequest {
    List<String> policyNames;
    String leadUuid;
    String leadSource;
    String leadChannel;
    String assignedUserUuid;
    String expectedAssignUserUuid;
    String projectUuid;
    String assignedByUser;
    String agentRole;
    List<User> assignedUsers;
    List<User> expectedUsers;

}
