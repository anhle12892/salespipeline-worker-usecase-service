package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadStatus;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadBookingEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class SaleBookingTicketHandler implements IActivityHandler {

    private final IWorkflowService workflowService;
    private final ObjectMapper objectMapper;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ILeadService leadService;
    private final EventPublisher eventPublisher;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper, data);

        String leadUuid = completeTaskData.getLeadUuid();
        Lead lead = leadService.getByUuid(leadUuid);
        if (lead == null || LeadStatus.REVOKED.equals(lead.getStatus())) {
            return null;
        }

        String bookingCode = "";
        if (data.containsKey(VariableName.BOOKING_CODE)) {
            bookingCode = Objects.isNull(data.get(VariableName.BOOKING_CODE)) ? "" : data.get(VariableName.BOOKING_CODE).toString();
        }

        var resp = salesPipelineUsecaseService.updateLead(leadUuid, completeTaskData.getData());

        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, leadUuid));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.BOOKING_CODE, new Variable(DataType.STRING, bookingCode));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        eventPublisher.publishEvent(new LeadBookingEvent(leadUuid, bookingCode));
        log.info("completeTask - IActivityHandler: [SaleBookingTicketHandler] - Lead_UUID: [{}]", resp.getLeadUuid());
        return resp;
    }
}
