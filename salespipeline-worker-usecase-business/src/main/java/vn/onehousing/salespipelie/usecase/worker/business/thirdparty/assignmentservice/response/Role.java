package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Role {
    String uuid;
    String name;
    String code;
    String agentType;
    String agentLevel;
}
