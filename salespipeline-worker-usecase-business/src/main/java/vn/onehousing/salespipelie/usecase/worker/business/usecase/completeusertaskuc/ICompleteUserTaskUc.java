package vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc;

import java.util.Map;

public interface ICompleteUserTaskUc {
    Object process(String activity, Map<String,Object> data) throws Exception;
}
