package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class District {
    @JsonProperty("district_id")
    private String districtId;
    @JsonProperty("district_name")
    private String districtName;
    @JsonProperty("district_code")
    private String districtCode;
}
