package vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleaduc.IGetTaskByLeadUc;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleadud.IGetTasksByLeadUc;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.factory.v2.ActivityHandlerV2Factory;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.IErrorMessageRepository;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.CannotFindTaskException;

import java.time.Instant;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class CompleteUserTaskV2Uc implements ICompleteUserTaskV2Uc {

    private final ObjectMapper mapper;
    private final IGetTasksByLeadUc getListTaskByLeadUc;
    private final IGetTaskByLeadUc getTaskByLeadUc;
    private final IErrorMessageRepository errorMessageRepository;
    private final ActivityHandlerV2Factory activityHandlerFactory;


    @Override
    public Object process(String activity, Map<String, Object> data) throws Exception {

        String leadUuid = data.get(VariableName.LEAD_UUID).toString();

        Task task = getTaskByLeadUc.process(leadUuid, activity);
        if (task == null) {
            throw new CannotFindTaskException(
                    String.format(
                            "cannot find task - lead_uuid: [%s] - activity: [%s]",
                            leadUuid,
                            activity
                    )
            );
        }

        log.info("start process activity [{}] - lead_uuid: [{}] - data: [{}] - time: [{}]", activity, leadUuid, mapper.writeValueAsString(data), Instant.now().toEpochMilli());
        IActivityHandler handler = activityHandlerFactory.getFactory().get(activity);
        var resp = handler.handler(activity, task, data);
        log.info("end process activity [{}] - lead_uuid: [{}] - time: [{}]", activity, leadUuid, Instant.now().toEpochMilli());

        return resp;
    }
}
