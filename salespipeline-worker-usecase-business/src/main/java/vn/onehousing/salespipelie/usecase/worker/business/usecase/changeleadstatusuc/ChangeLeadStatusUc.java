package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadStatus;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request.UpdateLeadReq;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadQualifyResponseEvent;

import java.time.Instant;

@Slf4j
@Service
@AllArgsConstructor
public class ChangeLeadStatusUc implements IChangeLeadStatusUc {

    private final ILeadService leadService;
    private final EventPublisher eventPublisher;

    @Override
    public void process(ChangeLeadStatusReq req) throws JsonProcessingException {
        var requestBuilder = UpdateLeadReq.builder().status(req.getStatus());
        if (req.getStatus().equals(LeadStatus.STOP) || req.getStatus().equals(LeadStatus.SUCCESS)) {
            requestBuilder.closedDate(Instant.now());
        }
        leadService.update(
            req.getLeadUuid(),
            requestBuilder.build()
        );

        // fire event when update lead qualify result
        if (LeadStatus.QUALIFIED.equals(req.getStatus())) {
            boolean leadQualify = Boolean.parseBoolean(req.getData().get(VariableName.LEAD_QUALIFY).toString());
            LeadQualifyResponseEvent message = new LeadQualifyResponseEvent(req.getLeadUuid(), leadQualify);
            eventPublisher.publishEvent(message);
        }

        if (LeadStatus.STOP.equals(req.getStatus())) {
            boolean leadQualify = Boolean.parseBoolean(req.getData().get(VariableName.LEAD_QUALIFY).toString());
            if (leadQualify == false) {
                eventPublisher.publishEvent(new LeadQualifyResponseEvent(req.getLeadUuid(), leadQualify));
            }
        }

//        if (LeadStatus.REVOKED.equals(req.getStatus())) {
//
//            String revokeType = req.getData().get(VariableName.REVOKED_TYPE).toString();
//            String revokeReason = req.getData().get(VariableName.REVOKED_REASON).toString();
//            String assignedUserUuid = req.getData().get(VariableName.LEAD_ASSIGNED_USER_UUID).toString();
//            String source = req.getData().get(VariableName.LEAD_SOURCE).toString();
//            String channel = req.getData().get(VariableName.LEAD_CHANNEL).toString();
//
//            eventPublisher.publishEvent(
//                LeadRevokedEvent.builder()
//                    .leadUuid(req.getLeadUuid())
//                    .revokedUserUuid(assignedUserUuid)
//                    .revokedType(revokeType)
//                    .revokedReason(revokeReason)
//                    .source(source)
//                    .channel(channel)
//                    .build()
//            );
//        }
        
    }
}
