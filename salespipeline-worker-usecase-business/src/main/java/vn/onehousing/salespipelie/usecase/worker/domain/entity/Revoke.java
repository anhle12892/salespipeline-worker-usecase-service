package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Revoke {
    private boolean isRevoked;
    private String revokedType;
    private String revokedReason;
    private String dropStatus;

}
