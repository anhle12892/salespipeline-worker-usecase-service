package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.vinid.core.event.AbstractEvent;

@Data
@AllArgsConstructor
public class LeadStoppedEvent extends AbstractEvent {
    String leadUuid;
    String assignedUserUuid;
    String source;
    String channel;
}
