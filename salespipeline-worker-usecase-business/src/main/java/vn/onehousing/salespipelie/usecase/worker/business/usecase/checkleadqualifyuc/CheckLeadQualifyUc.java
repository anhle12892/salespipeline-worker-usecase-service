package vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadqualifyuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.ILeadUsecaseService;

@Service
@AllArgsConstructor
public class CheckLeadQualifyUc implements ICheckLeadQualifyUc {

    private final ILeadUsecaseService leadUsecaseService;

    @Override
    public boolean process(String leadUuid) {
        var res = leadUsecaseService.reviewQualify(leadUuid);
        return res.isQualified();
    }

}
