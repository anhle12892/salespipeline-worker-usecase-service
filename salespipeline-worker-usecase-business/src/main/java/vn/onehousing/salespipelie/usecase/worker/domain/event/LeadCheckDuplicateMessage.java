package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.*;
import lombok.experimental.FieldDefaults;
import net.vinid.core.event.AbstractEvent;

import java.util.List;

@ToString(callSuper = true)
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class LeadCheckDuplicateMessage extends AbstractEvent {
    String leadUuid;
    List<String> ruleCodes;
}
