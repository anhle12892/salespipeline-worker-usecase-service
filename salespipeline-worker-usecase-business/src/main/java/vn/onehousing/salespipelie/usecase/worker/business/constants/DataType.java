package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class DataType {
    public static final String STRING = "string";
    public static final String BOOLEAN = "boolean";
}
