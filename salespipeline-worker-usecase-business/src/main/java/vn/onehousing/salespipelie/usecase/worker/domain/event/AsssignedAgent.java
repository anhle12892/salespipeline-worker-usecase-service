package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AsssignedAgent {
    private String name;
    private String code;
    private String phoneNumber;
}
