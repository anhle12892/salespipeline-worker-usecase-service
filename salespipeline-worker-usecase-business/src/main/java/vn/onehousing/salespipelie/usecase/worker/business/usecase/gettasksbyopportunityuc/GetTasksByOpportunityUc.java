package vn.onehousing.salespipelie.usecase.worker.business.usecase.gettasksbyopportunityuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class GetTasksByOpportunityUc implements IGetTasksByOpportunityUc {

    private final IWorkflowService workflowService;
    private final ISalesPipelineWorkflowProcessRepository repository;

    @Override
    public Map<String, Task> process(String opportunityUuid) {
        SalesPipelineWorkflowInstance salesPipelineWorkflowInstance = repository.getByOpportunityUuid(opportunityUuid);
        if (salesPipelineWorkflowInstance == null)
            throw new ProcessInstanceNotFoundException(
                    String.format("not found SalesPipelineWorkflowInstance with opportunity_uuid: [%s]", opportunityUuid)
            );
        final List<Task> tasks = workflowService.getListTaskByProcessInstanceId(salesPipelineWorkflowInstance.getProcessInstanceId());
        log.info("opportunity_uuid: [{}], received task [{}]", opportunityUuid, tasks);

        if(tasks.size() == 0)
            return new HashMap<>();

        return tasks.stream().collect(Collectors.toMap(x -> x.getTaskDefinitionKey(), x -> x));
    }
}
