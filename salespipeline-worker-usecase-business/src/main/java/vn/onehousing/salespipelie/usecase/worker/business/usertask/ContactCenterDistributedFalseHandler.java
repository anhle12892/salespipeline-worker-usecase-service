package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class ContactCenterDistributedFalseHandler  implements IActivityHandler {

    private final ObjectMapper objectMapper;
    private final IWorkflowService workflowService;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;

    @Override
    public Object handler(String userTask, Task task, Map<String,Object> data) throws Exception {

        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper,data);

        var resp = salesPipelineUsecaseService.updateLead(completeTaskData.getLeadUuid(),completeTaskData.getData());

        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_UUID,new Variable(DataType.STRING,completeTaskData.getLeadUuid()));
        variableMap.put(VariableName.USER_TASK,new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.LEAD_STATUS, new Variable(DataType.STRING, completeTaskData.getData().getStatus()));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        log.info("completeTask - IActivityHandler: [ContactCenterDistributedFalseHandler] - Lead_UUID: [{}]", completeTaskData.getLeadUuid());
        return resp;
    }
}
