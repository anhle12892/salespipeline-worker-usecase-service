package vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleadud;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.ProcessInstance;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeprocessinstanceiduc.IChangeProcessInstanceId;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class GetTasksByLeadUc implements IGetTasksByLeadUc {

    private final IWorkflowService workflowService;
    private final IChangeProcessInstanceId changeProcessInstanceId;
    private final ISalesPipelineWorkflowProcessRepository repository;

    @Override
    public Map<String, Task> process(String leadUuid) {

        SalesPipelineWorkflowInstance salesPipelineWorkflowInstance = repository.getByLeadUuId(leadUuid);
        if (salesPipelineWorkflowInstance == null){
            throw new ProcessInstanceNotFoundException(
                String.format("not found SalesPipelineWorkflowInstance with lead_uuid: [%s]", leadUuid)
            );
        }

        List<Task> tasks = workflowService.getListTaskByProcessInstanceId(salesPipelineWorkflowInstance.getProcessInstanceId());

        log.info(
            "GetTasksByLeadUc.process - lead_uuid: [{}] - process_instance_id: [{}] - task size: [{}]",
            leadUuid,
            salesPipelineWorkflowInstance.getProcessInstanceId(),
            tasks.size()
        );

        if(tasks != null && tasks.size() != 0){
            return tasks.stream().collect(Collectors.toMap(Task::getTaskDefinitionKey, x -> x));
        }

        tasks = new ArrayList<>();
        ProcessInstance[] allProcessInstance = workflowService.getListProcessInstanceByLeadUuid(leadUuid);
        if(allProcessInstance == null || allProcessInstance.length == 0){
            return new HashMap<>();
        }

        for (ProcessInstance processInstance :allProcessInstance) {
            List<Task> taskResult = workflowService.getListTaskByProcessInstanceId(
                processInstance.getId()
            );
            if(tasks == null || taskResult.size() == 0){
                continue;
            }
            changeProcessInstanceId.process(
                salesPipelineWorkflowInstance.getId(),
                processInstance.getId()
            );

            tasks.addAll(taskResult);
        }

        log.info(
            "GetTasksByLeadUc.process - lead_uuid: [{}] - process_instance_id: [{}] - task size: [{}]",
            leadUuid,
            salesPipelineWorkflowInstance.getProcessInstanceId(),
            tasks.size()
        );

        return tasks.stream().collect(Collectors.toMap(Task::getTaskDefinitionKey, x -> x));
    }
}
