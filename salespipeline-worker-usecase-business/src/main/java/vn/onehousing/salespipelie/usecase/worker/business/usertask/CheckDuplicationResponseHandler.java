package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.DuplicationLeadResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadDuplicateStatus;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class CheckDuplicationResponseHandler implements IActivityHandler {

    private final ObjectMapper mapper;
    private final IWorkflowService workflowService;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        DuplicationLeadResp duplicationLead = mapper.convertValue(data, DuplicationLeadResp.class);
        Map<String, Variable> variableMap = new HashMap<>();

        boolean isDuplicated = false;

        if (LeadDuplicateStatus.LEAD_DUPLICATED.equals(duplicationLead.getStatus())) {
            if (CollectionUtils.isEmpty(duplicationLead.getData()) || duplicationLead.getData().get(0) == null) {
                log.error(
                        "[CheckDuplicationResponseHandler] - message from rule service is invalid, when status is LEAD_DUPLICATED, data must have value that is the leads that duplicated, message: [{}]",
                        mapper.writeValueAsString(data)
                );
                variableMap.put(VariableName.LEAD_CHECK_DUPLICATE_RESPONSE_STATUS, new Variable(DataType.STRING, LeadDuplicateStatus.LEAD_NOT_READY_TO_CHECK));
                workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
                return null;
            }
        }

        if (duplicationLead.getStatus() != null && duplicationLead.getStatus() == LeadDuplicateStatus.LEAD_DUPLICATED) {
            isDuplicated = true;
            variableMap.put(VariableName.DUPLICATED_LEAD_UUID, new Variable(DataType.STRING, duplicationLead.getData().get(0).getLeadUuid()));
            variableMap.put(VariableName.DUPLICATED_LEAD_CODE, new Variable(DataType.STRING, duplicationLead.getData().get(0).getLeadCode()));
        }
        log.info("lead_uuid [{}] is duplicated [{}]", duplicationLead.getLeadUuid(), isDuplicated);
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, duplicationLead.getLeadUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.LEAD_DUPLICATED, new Variable(DataType.BOOLEAN, isDuplicated));
        variableMap.put(VariableName.LEAD_CHECK_DUPLICATE_RESPONSE_STATUS, new Variable(DataType.STRING, duplicationLead.getStatus()));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        log.info(
                "completeTask - IActivityHandler: [CheckDuplicationResponseHandler] - Lead_UUID: [{}] - Duplication_Status: [{}]",
                duplicationLead.getLeadUuid(),
                duplicationLead.getStatus()
        );

        return null;
    }
}
