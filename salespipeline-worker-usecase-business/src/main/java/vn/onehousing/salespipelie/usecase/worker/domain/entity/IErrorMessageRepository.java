package vn.onehousing.salespipelie.usecase.worker.domain.entity;

public interface IErrorMessageRepository {
    ErrorMessage create(ErrorMessage data);
    ErrorMessage update(ErrorMessage data);
    ErrorMessage getByActivityAndHashCode(String activity,int hashCode);
}
