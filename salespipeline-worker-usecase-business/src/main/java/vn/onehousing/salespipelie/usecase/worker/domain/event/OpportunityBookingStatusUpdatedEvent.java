package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OpportunityBookingStatusUpdatedEvent {
    private String leadUuid;
    private String opportunityStatus;
    private String bookingCode;
    private String activity;
}
