package vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestatusuc;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IChangeSalesPipelineStatusUc {
    String process(ChangeSalesPipelineStatusReq req) throws JsonProcessingException;
}
