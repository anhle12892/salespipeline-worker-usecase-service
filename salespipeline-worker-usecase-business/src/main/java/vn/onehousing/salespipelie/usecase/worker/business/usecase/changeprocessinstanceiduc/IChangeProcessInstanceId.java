package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeprocessinstanceiduc;

public interface IChangeProcessInstanceId {
    void process(String oldId, String newId);
}
