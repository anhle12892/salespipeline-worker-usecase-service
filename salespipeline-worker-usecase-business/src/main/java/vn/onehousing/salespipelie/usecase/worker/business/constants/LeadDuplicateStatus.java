package vn.onehousing.salespipelie.usecase.worker.business.constants;

public enum LeadDuplicateStatus {
    LEAD_NOT_FOUND,
    LEAD_DUPLICATED,
    LEAD_NOT_DUPLICATE,
    LEAD_NOT_READY_TO_CHECK
}
