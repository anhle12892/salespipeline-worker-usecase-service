//package vn.onehousing.salespipelie.usecase.worker.business.usecase.sendmessageuc;
//
//import lombok.AllArgsConstructor;
//import org.springframework.stereotype.Service;
//import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
//import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.ProcessInstance;
//import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
//import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
//import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;
//
//@Service
//@AllArgsConstructor
//public class SendMessage implements ISendMessageUc {
//
//    private final ISalesPipelineWorkflowProcessRepository salesPipelineWorkflowProcessRepository;
//    private final IWorkflowService workflowService;
//    private final SendMessageReqMapper mapper;
//
//    @Override
//    public void process(SendMessageReq req) {
//        SalesPipelineWorkflowInstance instance = salesPipelineWorkflowProcessRepository
//                .getByProcessInstanceId(req.processInstanceId);
//        if (instance == null)
//            throw new ProcessInstanceNotFoundException(
//                    String.format(
//                            "not found SalesPipelineWorkflowInstance with instance_id: [%s]",
//                            req.getProcessInstanceId()
//                    )
//            );
//
//        var sendRequest = mapper.to(req);
//        sendRequest.setResultEnabled(true);
//        ProcessInstance newInstance = workflowService.startProcessInstanceByMessage(sendRequest);
//        instance.setProcessInstanceId(newInstance.getId());
//        salesPipelineWorkflowProcessRepository.update(instance);
//    }
//}
