package vn.onehousing.salespipelie.usecase.worker.business.usertask.factory.v2;

import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.v2.ContactCenterPreQualifiedLeadHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.v2.ContactCenterPreUnqualifiedLeadHandler;

import java.util.HashMap;
import java.util.Map;

@Service
public class ActivityHandlerV2Factory {

    private final Map<String, IActivityHandler> activityHandlerMap;

    public ActivityHandlerV2Factory(
            ContactCenterPreQualifiedLeadHandler contactCenterPreQualifiedLeadHandler,
            ContactCenterPreUnqualifiedLeadHandler contactCenterPreUnqualifiedLeadHandler
    ) {
        activityHandlerMap = new HashMap<>();
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CC_PRE_QUALIFIED_LEAD, contactCenterPreQualifiedLeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CC_PRE_UNQUALIFIED_LEAD, contactCenterPreUnqualifiedLeadHandler);
    }

    public Map<String, IActivityHandler> getFactory() {
        return this.activityHandlerMap;
    }
}
