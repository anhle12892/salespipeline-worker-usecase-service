package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class InputName {
    public static final String INPUT_CHANGE_DATA = "Input_ChangeData";
    public static final String INPUT_TOPIC = "Input_Topic";
    public static final String INPUT_DATA = "Input_Data";
    public static final String INPUT_VALIDATION = "Input_Validation";
}
