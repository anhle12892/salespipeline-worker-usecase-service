package vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestage;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.ISalesPipelineService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request.UpdateSalesPipelineReq;

@Service
@AllArgsConstructor
public class ChangeSalesPipelineStageUc implements IChangeSalesPipelineStageUc {

    private final ISalesPipelineService salesPipelineService;

    @Override
    public void process(ChangeSalesPipelineStageUcReq req) {
        salesPipelineService.update(
                req.getSalesPipelinesUuid(),
                UpdateSalesPipelineReq
                        .builder()
                        .saleStage(req.getSaleStage())
                        .build()
        );
    }
}
