package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SalesPipelineWorkflowInstance {
    String id;
    String leadUuid;
    String leadCode;
    String salespipelineUuid;
    String opportunityUuid;
    String opportunityCode;
    String processInstanceId;
    String correlationId;
}
