package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReviewConvertResp {
    String leadUuid;

    @JsonProperty("is_can_convert")
    boolean isCanConvert;
}
