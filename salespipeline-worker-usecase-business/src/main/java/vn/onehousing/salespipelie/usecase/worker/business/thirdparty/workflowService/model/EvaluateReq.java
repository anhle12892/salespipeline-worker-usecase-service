package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class EvaluateReq {
    Map<String, Variable> variables;
}
