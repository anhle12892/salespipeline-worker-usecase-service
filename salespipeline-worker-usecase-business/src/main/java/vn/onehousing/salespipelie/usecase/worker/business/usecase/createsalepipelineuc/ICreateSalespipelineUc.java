package vn.onehousing.salespipelie.usecase.worker.business.usecase.createsalepipelineuc;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;

public interface ICreateSalespipelineUc {
    SalesPipeline process(CreateSalesPipelineUcReq req);
}
