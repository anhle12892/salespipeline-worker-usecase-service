package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.request.AssignRequest;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.RevokedDataDto;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.UserDto;

import java.util.List;

public interface IAssignmentService {
    //    UserDto assign(List<String> policyNames, Map<String, Object> data) throws JsonProcessingException;
    UserDto assign(AssignRequest request) throws JsonProcessingException;

    UserDto assignMultiUser(AssignRequest request) throws JsonProcessingException;

    List<RevokedDataDto> revoke(String leadUUid);

    UserDto getAgent(String assignedUserUuid);

    UserDto getRolesByUserUuid(String userUuid);

}
