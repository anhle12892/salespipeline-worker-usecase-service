package vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadcanconvertibleuc;

public interface ICheckLeadCanConvertibleUc {
    boolean process(String leadUuid);
}
