package vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestage;

public interface IChangeSalesPipelineStageUc {
    void process(ChangeSalesPipelineStageUcReq req);
}
