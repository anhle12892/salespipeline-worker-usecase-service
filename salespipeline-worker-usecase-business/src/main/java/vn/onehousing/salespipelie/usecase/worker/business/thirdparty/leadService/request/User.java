package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    String userUuid;
    String userCode;
    String role;
}
