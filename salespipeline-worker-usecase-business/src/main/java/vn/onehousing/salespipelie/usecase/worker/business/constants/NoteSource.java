package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class NoteSource {
    public static final String BACK_OFFICE = "BACK_OFFICE";
    public static final String AGENT = "AGENT";
    public static final String CUSTOMER = "CUSTOMER";
    public static final String CONTACT_CENTER = "CONTACT_CENTER";

    public static String convertRecordType(String source) {
        switch (source) {
            case AGENT:
                return RecordType.AGENT_NOTE;
            case CUSTOMER:
                return RecordType.CUSTOMER_MSG;
            case CONTACT_CENTER:
                return RecordType.CONTACT_CENTER_NOTE;
            default:
                return null;
        }
    }
}
