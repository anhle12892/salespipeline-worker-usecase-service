package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class RevokedDataDto {
    private String leadUuid;
    private String revokedType;
    private String status;
    private Instant deadline;
    private LeadDataDto data;
    private String revokedReason;
}
