package vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.TimeSlotAssignLead;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Slf4j
@Service
@AllArgsConstructor
public class TimeSlotAssignLeadToAgentUc implements ITimeSlotAssignLeadToAgentUc {
	@Override
	public boolean isAllowAssignLeadToAgentInTimeSlot(String leadUuid, TimeSlotAssignLead timeSlotAssignLead) {
		try {
			LocalTime localTimeNow = timeSlotAssignLead.getDateTimeNow().toLocalTime();

			LocalTime start = LocalTime.parse(timeSlotAssignLead.getStartTime());
			LocalTime stop = LocalTime.parse(timeSlotAssignLead.getEndTime());

			DayOfWeek startWeekDay = DayOfWeek.valueOf(timeSlotAssignLead.getStartDay());
			DayOfWeek endWeekDay = DayOfWeek.valueOf(timeSlotAssignLead.getEndDay());
			DayOfWeek dayOfWeekNow = timeSlotAssignLead.getDateTimeNow().getDayOfWeek();

			return startWeekDay.getValue() <= dayOfWeekNow.getValue()
				&& endWeekDay.getValue() >= dayOfWeekNow.getValue()
				&& localTimeNow.isAfter(start) && localTimeNow.isBefore(stop);
		} catch (Exception ex) {
			log.error("isAllowAssignLeadToAgentInTimeSlot() with lead_uuid: [{}] function error: {}", leadUuid, ex);
			return false;
		}
	}
}
