package vn.onehousing.salespipelie.usecase.worker.business.usertask.model;

import lombok.Data;

@Data
public class RevokeRequest {
    String revokedType;
    String revokedReason;
}
