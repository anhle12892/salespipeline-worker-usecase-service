package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TimeSlotAssignLead {
	String startTime;
	String endTime;
	String startDay;
	String endDay;
	LocalDateTime dateTimeNow;
}
