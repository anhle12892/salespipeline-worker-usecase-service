package vn.onehousing.salespipelie.usecase.worker.business.usertask.factory;

import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.*;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.v2.ContactCenterPreQualifiedLeadHandler;

import java.util.HashMap;
import java.util.Map;

@Service
public class ActivityHandlerFactory {

    private final Map<String, IActivityHandler> activityHandlerMap;

    public ActivityHandlerFactory(
            LeadEnteredHandler leadEnteredHandler,
            ContactCenterQualifiedLeadHandler contactCenterQualifiedLeadHandler,
            CheckDuplicationResponseHandler checkDuplicationResponseHandler,
            SaleBookingTicketHandler saleBookingTicketHandler,
            SaleUpdatedOppSuccessHandler saleUpdatedOppSuccessHandler,
            SaleUpdatedOppStopHandler saleUpdatedOppStopHandler,
            ManualAssignedAgentHandler manualAssignedAgentHandler,
            UpdateLeadInfoHandler updateLeadInfoHandler,
            LeadDeadHandler leadDeadHandler,
            ContactCenterUnqualifiedLeadHandler contactCenterUnqualifiedLeadHandler,
            ContactCenterDistributedFalseHandler contactCenterDistributedFalseHandler,
            OPSApproveAppointmentHandler opsApproveAppointmentHandler,
            OPSBookingTicketHandler opsBookingTicketHandler,
            OPSManualRevokedLeadHandler opsManualRevokedLeadHandler,
            OPSManualRevokedOppHandler opsManualRevokedOppHandler,
            SaleUpdatedLeadStopHandler saleUpdatedLeadStopHandler,
            AgentCompleteStageLeadHandler agentCompleteStageLeadHandler,
            AgentCompleteStageOppHandler agentCompleteStageOppHandler,
            ContactCenterPreQualifiedLeadHandler contactCenterPreQualifiedLeadHandler
    ) {
        activityHandlerMap = new HashMap<>();
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_LEAD_ENTERED, leadEnteredHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CHECK_DUPLICATE_RESPONSE, checkDuplicationResponseHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_SALE_UPDATED_LEAD_STOP, saleUpdatedLeadStopHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_SALE_BOOKING_TICKET, saleBookingTicketHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_SALE_UPDATED_OPP_SUCCESS, saleUpdatedOppSuccessHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_SALE_UPDATED_OPP_STOP, saleUpdatedOppStopHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_MANUAL_ASSIGNED_AGENT, manualAssignedAgentHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_UPDATE_LEAD_INFO, updateLeadInfoHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CC_LEAD_DEAD, leadDeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CC_QUALIFIED_LEAD, contactCenterQualifiedLeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CC_UNQUALIFIED_LEAD, contactCenterUnqualifiedLeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_CC_DISTRIBUTED_FALSE_LEAD, contactCenterDistributedFalseHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_MANUAL_LEAD_ASSIGN_DEAD, leadDeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_OPS_BOOKING_TICKET, opsBookingTicketHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_OPS_APPROVE_APPOINTMENT, opsApproveAppointmentHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_OPS_REVOKED_LEAD, opsManualRevokedLeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_OPS_REVOKED_OPP, opsManualRevokedOppHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_OPS_OPP_BOOKING_TICKET, opsBookingTicketHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_OPS_OPP_APPROVE_APPOINTMENT, opsApproveAppointmentHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_SALE_OPP_BOOKING_TICKET, saleBookingTicketHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_STAGE_COMPLETED_LEAD, agentCompleteStageLeadHandler);
        activityHandlerMap.put(TaskDefinitionKey.ACTIVITY_STAGE_COMPLETED_OPP, agentCompleteStageOppHandler);
    }

    public Map<String, IActivityHandler> getFactory() {
        return this.activityHandlerMap;
    }
}
