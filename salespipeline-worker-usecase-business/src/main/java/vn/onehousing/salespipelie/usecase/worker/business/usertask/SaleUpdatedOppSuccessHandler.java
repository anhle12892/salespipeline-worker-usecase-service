package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.IOpportunityService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Opportunity;
import vn.onehousing.salespipelie.usecase.worker.domain.event.OpportunitySucceedEvent;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class SaleUpdatedOppSuccessHandler implements IActivityHandler {

    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final IWorkflowService workflowService;
    private final ObjectMapper objectMapper;
    private final EventPublisher eventPublisher;
    private final IOpportunityService opportunityService;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper, data);
        Map<String, Variable> variableMap = new HashMap<>();

        Opportunity opportunity = opportunityService.getByLeadUuid(completeTaskData.getLeadUuid());

        var resp = salesPipelineUsecaseService.updateLead(completeTaskData.getLeadUuid(), completeTaskData.getData());

        variableMap.put(VariableName.DROP_STATUS, new Variable(DataType.STRING, opportunity.getStatus()));
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, completeTaskData.getLeadUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        variableMap.put(VariableName.OPPORTUNITY_STATUS, new Variable(DataType.STRING, completeTaskData.getData().getStatus()));

        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));

        eventPublisher.publishEvent(
            new OpportunitySucceedEvent(
                resp.getLeadUuid(),
                resp.getOpportunityUuid(),
                opportunity.getSource(),
                opportunity.getChannel(),
                opportunity.getAssignedUserUuid()
            )
        );

        log.info("completeTask - IActivityHandler: [SaleUpdatedOppSuccessHandler] - Lead_UUID: [{}]", completeTaskData.getLeadUuid());

        return resp;
    }
}
