package vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;

import java.util.List;

public interface IAutoAssignLeadToAgentUc {

    String process(String leadUuid, List<String> policyNames, String assignUserUuid, String expectedAssignUserUuid,
                   String assignedByUser) throws JsonProcessingException;

    void processMultiAgent(String leadUuid, String stage, List<User> expectedAssignUser,
                           List<User> assignedByUser, List<String> saleRoles, String createdByUser) throws JsonProcessingException, Exception;
}
