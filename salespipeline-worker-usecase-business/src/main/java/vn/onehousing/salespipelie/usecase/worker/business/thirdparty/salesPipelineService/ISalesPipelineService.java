package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request.CreateSalesPipelineReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request.UpdateSalesPipelineReq;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;

public interface ISalesPipelineService {
    SalesPipeline create(CreateSalesPipelineReq req);

    void update(String salespipelineUuid, UpdateSalesPipelineReq req);

    
}
