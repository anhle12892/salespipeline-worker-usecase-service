//package vn.onehousing.salespipelie.usecase.worker.business.usecase.sendmessageuc;
//
//import lombok.Builder;
//import lombok.Data;
//import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
//
//import java.util.Map;
//
//@Data
//@Builder
//public class SendMessageReq {
//    String messageName;
//    String businessKey;
//    String processInstanceId;
//    Map<String, Object> processVariables;
//}
