package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Variable {
    String type;
    Object value;
}
