package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class RecordType {
    public static final String AGENT_NOTE = "AGENT_NOTE";
    public static final String CUSTOMER_MSG = "CUSTOMER_MSG";
    public static final String CONTACT_CENTER_NOTE = "CONTACT_CENTER_NOTE";
}
