package vn.onehousing.salespipelie.usecase.worker.business.usecase.bookingrecieveduc;


public interface IBookingReceivedUc {
    void process(String leadUuid, String activity, String status) throws Exception;
}
