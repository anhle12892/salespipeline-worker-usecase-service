package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.Instant;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Task {
    private Long taskId;
    private String taskUuid;
    private String status;
    private String name;
    private String code;
    private String entityType;
    private String entityUuid;
    private String assignedToUserUuid;
    private String assignedEmail;
    private String comments;
    private String description;
    private Instant startDate;
    private Instant dueDate;
    private String priority;
    private Instant completedDate;
    private String taskTypeCode;
    private String taskTypeName;
    private Instant remindDate;
    private Boolean isDeleted;

    private Instant lastModifiedDate;
    private String createdBy;
    private String lastModifiedBy;
    private Instant createdDate;
}
