package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones;

import lombok.Data;

@Data
public class NeedDuplicateCheckingResp {
    String leadUuid;
    boolean needCheck;
}
