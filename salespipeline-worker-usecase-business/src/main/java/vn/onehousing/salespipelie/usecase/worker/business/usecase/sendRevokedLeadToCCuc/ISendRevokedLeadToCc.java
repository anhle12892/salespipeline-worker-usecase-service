package vn.onehousing.salespipelie.usecase.worker.business.usecase.sendRevokedLeadToCCuc;

import java.io.IOException;

public interface ISendRevokedLeadToCc {
    void process(String leadUuid, String dropAssignedUserUuid, String dropStatus, String revokedReason) throws IOException;
}
