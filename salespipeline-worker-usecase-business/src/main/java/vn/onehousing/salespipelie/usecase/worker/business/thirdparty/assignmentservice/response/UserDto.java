package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class UserDto {
  @JsonUnwrapped
  private Map<String, Object> data = new HashMap<>();
  private String id;
  private String name;
  private String uuid;
  private List<Role> roles;
}
