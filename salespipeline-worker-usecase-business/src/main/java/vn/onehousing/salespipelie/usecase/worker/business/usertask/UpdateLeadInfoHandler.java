package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;

import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class UpdateLeadInfoHandler implements IActivityHandler {

    private final IWorkflowService workflowService;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ObjectMapper objectMapper;

    @Override
    public Object handler(String userTask, Task task, Map<String,Object> data) throws Exception {

        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper,data);

        var resp = salesPipelineUsecaseService.updateLead(completeTaskData.getLeadUuid(),completeTaskData.getData());
        workflowService.completeTask(task.getId());
        log.info("completeTask - IActivityHandler: [UpdateLeadInfoHandler] - Lead_UUID: [{}]",completeTaskData.getLeadUuid());

        return resp;
    }
}
