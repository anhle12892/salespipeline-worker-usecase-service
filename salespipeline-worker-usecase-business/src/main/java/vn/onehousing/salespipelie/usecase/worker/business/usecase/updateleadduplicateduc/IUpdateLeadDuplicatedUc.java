package vn.onehousing.salespipelie.usecase.worker.business.usecase.updateleadduplicateduc;

public interface IUpdateLeadDuplicatedUc {
    void process(String leadUuid, String duplicatedLeadUuid, String duplicatedLeadCode);
}
