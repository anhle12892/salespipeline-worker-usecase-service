package vn.onehousing.salespipelie.usecase.worker.business.usecase.changesalespipelinestatusuc;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@AllArgsConstructor
public class ChangeSalesPipelineStatusReq {
    @Getter
    String leadUuid;
    @Getter
    String oppUuid;
    @Getter
    String status;
    //    @Getter
//    Boolean leadQualify;
//    @Getter
//    String source;
//    @Getter
//    String channel;
    Map<String, Object> data;
}
