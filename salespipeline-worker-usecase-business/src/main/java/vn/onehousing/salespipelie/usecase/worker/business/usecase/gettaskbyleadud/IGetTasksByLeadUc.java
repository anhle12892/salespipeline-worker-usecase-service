package vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleadud;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;

import java.util.Map;

public interface IGetTasksByLeadUc {
    Map<String,Task> process(String leadUuid);
}
