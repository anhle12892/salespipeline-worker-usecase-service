package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

import java.util.List;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RevokedResponse {
    private List<RevokedDataDto> data;
    private BaseResponse.Metadata meta = new BaseResponse.Metadata();
}
