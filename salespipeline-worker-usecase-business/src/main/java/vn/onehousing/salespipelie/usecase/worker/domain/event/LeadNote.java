package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Getter
@Builder
@AllArgsConstructor
public class LeadNote {
    // private RecordType recordType;
    // use string
    private String recordType;
    private String email;
    private String content;
    private Instant createTime;
}
