package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming()
public class ProcessInstance {
    String id;

    @JsonProperty("definitionId")
    String definitionId;

    @JsonProperty("businessKey")
    String businessKey;

    @JsonProperty("caseInstanceId")
    String caseInstanceId;

    String ended;
    String suspended;

    @JsonProperty("tenantId")
    String tenantId;
}
