package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class RevokeStatus {
    public static final String REVOKEABLE = "REVOKEABLE";
    public static final String REVOKED = "REVOKED";
    public static final String SURVIVAL = "SURVIVAL";
}
