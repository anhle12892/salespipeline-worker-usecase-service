package vn.onehousing.salespipelie.usecase.worker.business.usecase.createsalepipelineuc;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateSalesPipelineUcReq {
    String leadUuid;
}
