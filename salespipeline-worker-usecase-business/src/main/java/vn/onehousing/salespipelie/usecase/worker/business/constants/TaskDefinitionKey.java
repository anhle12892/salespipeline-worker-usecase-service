package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class TaskDefinitionKey {
    public static final String ACTIVITY_SALE_QUALIFY_LEAD = "Activity_Sale_Qualify_Lead";
    public static final String ACTIVITY_AGENT_WORKING_WITH_LEAD = "Activity_Agent_Working_With_Lead";
    public static final String ACTIVITY_SELLING_PROCESS = "Activity_Selling_Process";
    public static final String ACTIVITY_OPS_UPDATED_LEAD_STOP = "Activity_Ops_Updated_Lead_Stop";
    public static final String ACTIVITY_OPS_UPDATED_LEAD_INFO = "Activity_Ops_Updated_Lead_Info";
    public static final String ACTIVITY_CC_INVALID_CONTACT_LEAD = "Activity_CC_Invalid_Contact_Lead";

    public static final String ACTIVITY_LEAD_ENTERED = "Activity_Lead_Entered";
    public static final String ACTIVITY_CHECK_DUPLICATE_RESPONSE = "Activity_Check_Duplicate_Response";
    public static final String ACTIVITY_SALE_BOOKING_TICKET = "Activity_Sale_Booking_Ticket";
    public static final String ACTIVITY_SALE_UPDATED_LEAD_STOP = "Activity_Sale_Updated_Lead_Stop";
    public static final String ACTIVITY_SALE_UPDATED_OPP_SUCCESS = "Activity_Sale_Updated_Opp_Success";
    public static final String ACTIVITY_SALE_UPDATED_OPP_STOP = "Activity_Sale_Updated_Opp_Stop";
    public static final String ACTIVITY_MANUAL_ASSIGNED_AGENT = "Activity_Manual_Assigned_Agent";
    public static final String ACTIVITY_UPDATE_LEAD_INFO = "Activity_Update_Lead_Info";
    public static final String ACTIVITY_CC_QUALIFIED_LEAD = "Activity_CC_Qualified_Lead";
    public static final String ACTIVITY_CC_UNQUALIFIED_LEAD = "Activity_CC_Unqualified_Lead";
    public static final String ACTIVITY_CC_LEAD_DEAD = "Activity_CC_Lead_Dead";
    public static final String ACTIVITY_MANUAL_LEAD_ASSIGN_DEAD = "Activity_Manual_Lead_Assign_Dead";
    public static final String ACTIVITY_CC_DISTRIBUTED_FALSE_LEAD = "Activity_CC_Distributed_False_Lead";
    public static final String ACTIVITY_OPS_APPROVE_APPOINTMENT = "Activity_OPS_Approve_Appointment";
    public static final String ACTIVITY_OPS_BOOKING_TICKET = "Activity_OPS_Booking_Ticket";
    public static final String ACTIVITY_OPS_REVOKED_LEAD = "Activity_OPS_Revoked_Lead";
    public static final String ACTIVITY_OPS_REVOKED_OPP = "Activity_OPS_Revoked_Opp";
    public static final String ACTIVITY_OPS_OPP_BOOKING_TICKET = "Activity_OPS_Opp_Booking_Ticket";
    public static final String ACTIVITY_OPS_OPP_APPROVE_APPOINTMENT = "Activity_OPS_Opp_Approve_Appointment";
    public static final String ACTIVITY_SALE_OPP_BOOKING_TICKET = "Activity_Sale_Opp_Booking_Ticket";
    public static final String ACTIVITY_STAGE_COMPLETED_LEAD = "Activity_Stage_Completed_Lead";
    public static final String ACTIVITY_STAGE_COMPLETED_OPP = "Activity_Stage_Completed_Opp";

    public static final String ACTIVITY_CC_PRE_QUALIFIED_LEAD = "Activity_CC_Pre_Qualified_Lead";
    public static final String ACTIVITY_CC_PRE_UNQUALIFIED_LEAD = "Activity_CC_Pre_Unqualified_Lead";
}
