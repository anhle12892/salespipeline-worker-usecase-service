package vn.onehousing.salespipelie.usecase.worker.business.usecase.sendtoccuc;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface ISendToCcUc {
    void process(String leadUuid, String type, String dropStatus) throws JsonProcessingException;
}
