package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming()
public class MessageWithProcessInstance {
    ProcessInstance processInstance;
}
