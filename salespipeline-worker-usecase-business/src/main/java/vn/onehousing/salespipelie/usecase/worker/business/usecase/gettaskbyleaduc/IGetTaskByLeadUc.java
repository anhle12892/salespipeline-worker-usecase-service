package vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleaduc;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;

public interface IGetTaskByLeadUc {
    Task process(String leadUuid, String activity);
}
