package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;

@Data
@NoArgsConstructor
@JsonNaming()
@ToString
public class Task {
    String id;
    String name;
    Object assignee;
//    Instant created;
    Object due;
    Object followUp;
    Object delegationState;
    Object description;
    String executionId;
    String owner;
    String parentTaskId;
    int priority;
    String processDefinitionId;
    String processInstanceId;
    String taskDefinitionKey;
    String caseExecutionId;
    String caseInstanceId;
    String caseDefinitionId;
    boolean suspended;
    String formKey;
    String tenantId;
}
