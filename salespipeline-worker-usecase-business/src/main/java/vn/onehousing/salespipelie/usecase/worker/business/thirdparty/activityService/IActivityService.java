package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.activityService;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.Task;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface IActivityService {
    Optional<List<Task>> getTaskBy(String entityUuid, String entityType) throws IOException;
}
