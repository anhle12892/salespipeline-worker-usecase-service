package vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.TimeSlotAssignLead;

public interface ITimeSlotAssignLeadToAgentUc {
	boolean isAllowAssignLeadToAgentInTimeSlot(String leadUuid, TimeSlotAssignLead timeSlotAssignLead);
}
