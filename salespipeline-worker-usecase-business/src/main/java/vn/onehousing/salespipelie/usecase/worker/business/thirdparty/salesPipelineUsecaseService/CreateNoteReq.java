package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CreateNoteReq {
    public String noteCode;
    public String noteUuid;
    public String ownerUuid;
    public String ownerType;
    @NotBlank
    public String parentUuid;
    public String parentType;
    public String title;
    @NotBlank
    public String content;
}
