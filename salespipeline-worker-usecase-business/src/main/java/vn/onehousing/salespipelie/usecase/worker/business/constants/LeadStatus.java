package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class LeadStatus {
    public static final String STOP = "STOP";
    public static final String SUCCESS = "SUCCESS";
    public static final String CONTACT_CENTER_DISTRIBUTION_FAILED = "CONTACT_CENTER_DISTRIBUTION_FAILED";
    public static final String CONTACT_CENTER_QUALIFIED = "CONTACT_CENTER_QUALIFIED";
    public static final String QUALIFIED = "QUALIFIED";
    public static final String CONTACT_CENTER_UNQUALIFIED = "CONTACT_CENTER_UNQUALIFIED";
    public static final String SOFT_BOOKING = "SOFT_BOOKING";
    public static final String BOOKING = "BOOKING";
    public static final String REVOKED = "REVOKED";
}
