package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@Builder
public class UpdateLeadReq {
    public String status;
    boolean isConverted;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    String unqualifiedReason;
    String assignedUserUuid;
    String assignedUserCode;
    String unassignedReason;
    List<User> assignedUsers;
    List<User> expectedUsers;
    Instant closedDate;
}
