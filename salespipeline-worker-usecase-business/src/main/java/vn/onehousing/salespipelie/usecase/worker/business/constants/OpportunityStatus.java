package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class OpportunityStatus {
    //-- Working status
    public static final String ASSIGNED = "ASSIGNED";
    public static final String CONSULTING = "CONSULTING";
    public static final String VISIT = "VISIT";
    public static final String SOFT_BOOKING = "SOFT_BOOKING";
    public static final String BOOKING = "BOOKING";
    public static final String SIGNED = "SIGNED";
    public static final String STOP = "STOP";
    public static final String SUCCESS = "SUCCESS";

}
