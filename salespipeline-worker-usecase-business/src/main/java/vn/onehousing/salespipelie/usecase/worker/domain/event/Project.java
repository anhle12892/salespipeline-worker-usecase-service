package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.Data;

@Data
public class Project {
    private String uuid;
    private String code;
    private String name;
}
