package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class SalesPipeline {
    String id;
    String uuid;
    String leadUuid;
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
    String referredByUserUuid;
    String referredByUserCode;
    String saleStage;
    String source;
    String note;
    String disqualifiedReason;
    String closedLostReason;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    String unassignedReason;
    Instant closedDate;
}
