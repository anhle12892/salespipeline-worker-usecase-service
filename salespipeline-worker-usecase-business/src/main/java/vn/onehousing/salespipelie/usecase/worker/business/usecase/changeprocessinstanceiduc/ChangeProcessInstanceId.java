package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeprocessinstanceiduc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowInstanceHistoryRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstanceHistory;

@Slf4j
@Service
@AllArgsConstructor
public class ChangeProcessInstanceId implements IChangeProcessInstanceId {

    private final ISalesPipelineWorkflowProcessRepository salesPipelineWorkflowProcessRepository;
    private final ISalesPipelineWorkflowInstanceHistoryRepository salesPipelineWorkflowInstanceHistoryRepository;

    @Override
    public void process(String salesPipelineWorkflowId, String newId) {
        var result = salesPipelineWorkflowProcessRepository.getById(salesPipelineWorkflowId);
        String leadUuid = result.getLeadUuid();
        if (result == null) {
            log.warn("ChangeProcessInstanceId - not found process_instance_id: [{}] - lead_uuid: [{}]", result.getProcessInstanceId(), leadUuid);
            return;
        }
        result.setProcessInstanceId(newId);
        salesPipelineWorkflowProcessRepository.update(result);
        salesPipelineWorkflowInstanceHistoryRepository.create(
            SalesPipelineWorkflowInstanceHistory
                .builder()
                .salesPipelineWorkflowInstanceId(result.getId())
                .processInstanceId(result.getProcessInstanceId())
                .build()
        );
        log.info("ChangeProcessInstanceId - update success - old process_instance_id: [{}] - new process_instance_id: [{}] lead_uuid: [{}]", result.getProcessInstanceId(), newId, leadUuid);
    }
}
