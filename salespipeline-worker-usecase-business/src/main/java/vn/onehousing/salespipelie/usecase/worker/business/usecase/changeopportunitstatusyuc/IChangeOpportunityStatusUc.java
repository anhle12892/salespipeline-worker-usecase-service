package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc;

public interface IChangeOpportunityStatusUc {
    void process(ChangeOpportunityStatusUcReq req);
}
