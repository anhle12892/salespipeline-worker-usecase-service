package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request.UpdateLeadReq;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;

public interface ILeadService {
    void update(String leadUuid, UpdateLeadReq req);

    Lead getByUuid(String leadUuid);
}
