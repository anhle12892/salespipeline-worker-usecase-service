package vn.onehousing.salespipelie.usecase.worker.business.usecase.startsalespipelineworkflowuc;

import lombok.AllArgsConstructor;
import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;

@Data
@AllArgsConstructor
public class StartSalePipelineWorkflowUcReq {
    Lead lead;
}
