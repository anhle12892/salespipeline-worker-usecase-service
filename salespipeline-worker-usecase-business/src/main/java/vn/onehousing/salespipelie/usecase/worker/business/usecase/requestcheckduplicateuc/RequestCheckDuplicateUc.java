package vn.onehousing.salespipelie.usecase.worker.business.usecase.requestcheckduplicateuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadCheckDuplicateMessage;

import java.util.Arrays;

@Slf4j
@Service
@AllArgsConstructor
public class RequestCheckDuplicateUc implements IRequestCheckDuplicateUc{

    private final EventPublisher eventPublisher;
    private final ObjectMapper objectMapper;

    @Override
    public void process(String leadUuid, String duplicateRuleCode) throws JsonProcessingException {
        LeadCheckDuplicateMessage duplicateMessage = new LeadCheckDuplicateMessage();
        duplicateMessage.setLeadUuid(leadUuid);
        duplicateMessage.setRuleCodes(Arrays.asList(duplicateRuleCode));
        log.info("RequestCheckDuplicateUc - send message: [{}]",objectMapper.writeValueAsString(duplicateMessage));
        eventPublisher.publishEvent(duplicateMessage);
    }
}
