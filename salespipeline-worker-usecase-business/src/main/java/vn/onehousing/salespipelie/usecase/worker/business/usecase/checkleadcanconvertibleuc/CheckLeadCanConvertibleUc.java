package vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadcanconvertibleuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.ILeadUsecaseService;

@Service
@AllArgsConstructor
public class CheckLeadCanConvertibleUc implements ICheckLeadCanConvertibleUc {

    private final ILeadUsecaseService leadUsecaseService;

    @Override
    public boolean process(String leadUuid) {
        var res = leadUsecaseService.reviewConvert(leadUuid);
        return res.isCanConvert();
    }
}
