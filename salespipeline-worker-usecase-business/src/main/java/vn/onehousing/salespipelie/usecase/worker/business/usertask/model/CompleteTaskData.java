package vn.onehousing.salespipelie.usecase.worker.business.usertask.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;

import java.util.Map;

@Data
@AllArgsConstructor
public class CompleteTaskData {
    String leadUuid;
    UpdateSalesPipelineLeadData data;

    public CompleteTaskData(ObjectMapper objectMapper, Map<String, Object> data){
        this.leadUuid = data.get(VariableName.LEAD_UUID).toString();
        this.data =  objectMapper.convertValue(data,UpdateSalesPipelineLeadData.class);
    }
}
