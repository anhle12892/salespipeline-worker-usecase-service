package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request;

import lombok.Builder;
import lombok.Getter;

@Builder
public class UpdateSalesPipelineReq {
    @Getter
    String saleStage;
}
