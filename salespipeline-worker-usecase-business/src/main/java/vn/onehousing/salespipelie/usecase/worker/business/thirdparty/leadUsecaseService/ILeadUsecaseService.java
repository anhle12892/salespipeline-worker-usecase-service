package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService;


import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.DuplicationLeadResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.NeedDuplicateCheckingResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.ReviewConvertResp;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.ReviewQualifyResp;

import java.util.List;


public interface ILeadUsecaseService {
    void assign(String leadUuid);

    ReviewQualifyResp reviewQualify(String leadUuid);

    ReviewConvertResp reviewConvert(String leadUuid);

    NeedDuplicateCheckingResp needDuplicateChecking(String leadUuid);

    List<DuplicationLeadResp> getListDuplication(String leadUuid);
}
