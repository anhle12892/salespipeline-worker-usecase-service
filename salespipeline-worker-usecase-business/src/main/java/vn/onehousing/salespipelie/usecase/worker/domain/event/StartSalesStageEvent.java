package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.vinid.core.event.AbstractEvent;

@Data
@AllArgsConstructor
public class StartSalesStageEvent extends AbstractEvent {
    private String leadUuid;
    private String stage;
}
