package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineService.request;

import lombok.Data;

@Data
public class CreateSalesPipelineReq {
    String leadUuid;
    String leadCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
    String referredByUserUuid;
    String referredByUserCode;
    String source;
    String note;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
}
