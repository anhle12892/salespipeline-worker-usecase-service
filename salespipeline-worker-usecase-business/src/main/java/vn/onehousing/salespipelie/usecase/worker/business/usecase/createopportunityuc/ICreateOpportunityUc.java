package vn.onehousing.salespipelie.usecase.worker.business.usecase.createopportunityuc;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;

public interface ICreateOpportunityUc {
    SalesPipelineWorkflowInstance process(String leadUuid, String salesPipelineUuid);
}
