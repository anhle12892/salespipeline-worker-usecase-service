package vn.onehousing.salespipelie.usecase.worker.business.usecase.needleadduplicacheckuc;

public interface INeedLeadDuplicateCheckUc {
    boolean process(String leadUuid);
}
