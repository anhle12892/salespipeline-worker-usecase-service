package vn.onehousing.salespipelie.usecase.worker.business.usecase.sendtoccuc;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.SendCcType;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.distributorservice.IDistributorService;

@Service
@AllArgsConstructor
public class SendToCcUc implements ISendToCcUc{

    private final IDistributorService distributorService;

    @Override
    public void process(String leadUuid, String type, String dropStatus) throws JsonProcessingException {
        switch (type){
            case SendCcType.PRE_QUALIFY:
                distributorService.sendToCCForPreQualify(leadUuid);
                break;
            case SendCcType.RE_QUALIFY_SUCCESS:
                distributorService.sendToCCForReQualifySuccess(leadUuid, dropStatus);
                break;
            case SendCcType.RE_QUALIFY_STOP:
                distributorService.sendToCCForReQualifyStop(leadUuid, dropStatus);
                break;
            default:
        }
    }
}
