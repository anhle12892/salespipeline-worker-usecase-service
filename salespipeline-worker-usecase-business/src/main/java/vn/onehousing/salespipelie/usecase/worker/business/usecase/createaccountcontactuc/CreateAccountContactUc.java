package vn.onehousing.salespipelie.usecase.worker.business.usecase.createaccountcontactuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;

@Service
@AllArgsConstructor
public class CreateAccountContactUc implements ICreateAccountContactUc {

    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ISalesPipelineWorkflowProcessRepository salesPipelineWorkflowProcessRepository;

    @Override
    public SalesPipelineWorkflowInstance process(String leadUuid, String salesPipelineUuid) {
        SalesPipelineWorkflowInstance insance = salesPipelineWorkflowProcessRepository.getByLeadUuId(leadUuid);
        if (insance == null)
            throw new ProcessInstanceNotFoundException(String.format("not found SalesPipelineWorkflowInstance with lead_uuid: [%s]", leadUuid));

        salesPipelineUsecaseService.createAccount(leadUuid, salesPipelineUuid);

        return insance;
    }
}
