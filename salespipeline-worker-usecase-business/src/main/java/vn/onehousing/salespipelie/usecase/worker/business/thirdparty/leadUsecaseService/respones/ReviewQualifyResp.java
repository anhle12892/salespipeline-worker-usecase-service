package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReviewQualifyResp {
    String leadUuid;
    @JsonProperty("is_qualified")
    boolean isQualified;
}
