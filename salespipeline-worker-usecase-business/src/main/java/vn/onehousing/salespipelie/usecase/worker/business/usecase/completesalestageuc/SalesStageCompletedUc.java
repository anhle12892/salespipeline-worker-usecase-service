package vn.onehousing.salespipelie.usecase.worker.business.usecase.completesalestageuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.EvaluateReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.ICompleteUserTaskUc;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalespipelineAggregateView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class SalesStageCompletedUc implements ISalesStageCompletedUc {

    private final ICompleteUserTaskUc completeUserTaskUc;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final IWorkflowService workflowService;

    @Override
    public boolean process(String leadUuid, String stage) throws Exception {
        SalespipelineAggregateView data = salesPipelineUsecaseService.getByLeadUuid(leadUuid);

        String activity = LeadType.LEAD.equals(data.getType()) ? TaskDefinitionKey.ACTIVITY_STAGE_COMPLETED_LEAD
                : TaskDefinitionKey.ACTIVITY_STAGE_COMPLETED_OPP;

        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_SOURCE, new Variable(DataType.STRING, data.getSource()));
        variableMap.put(VariableName.LEAD_CHANNEL, new Variable(DataType.STRING, data.getChannel()));
        variableMap.put(VariableName.LEAD_STAGE, new Variable(DataType.STRING, stage));
        EvaluateReq evaluateReq = new EvaluateReq(variableMap);
        List<String> saleRoles = workflowService.getSaleRoles(evaluateReq);

        Map<String, Object> map = new HashMap<>();
        map.put(VariableName.LEAD_UUID, leadUuid);
        map.put(VariableName.LEAD_STAGE, stage);
        map.put(VariableName.SALE_ROLES, saleRoles);
        map.put(VariableName.EXPECTED_USERS, data.getExpectedUsers());
        map.put(VariableName.ASSIGNED_USERS, data.getAssignedUsers());
        completeUserTaskUc.process(activity, map);
        return true;
    }

}
