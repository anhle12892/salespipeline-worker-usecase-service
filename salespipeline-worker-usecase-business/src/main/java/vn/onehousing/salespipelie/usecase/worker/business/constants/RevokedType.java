package vn.onehousing.salespipelie.usecase.worker.business.constants;

import lombok.Getter;

@Getter
public enum RevokedType {
    OPP_OVERDUE_CC_PENDING("Quá hạn chăm sóc"),
    OPP_OVERDUE_CC_QUALIFIED("Quá hạn chăm sóc"),
    OPP_OVERDUE_CC_UNQUALIFIED("Quá hạn chăm sóc"),
    MISSION_OVER_DUE_DATE("Quá hạn nhiệm vụ"),
    FAIL_FIRST_SLA("Không đạt SLA liên hệ lần đầu"),
    AGENT_QUIT_JOB("Agent nghỉ việc"),
    AGENT_STOP("Stop bởi Agent"),
    AGENT_CHANGES_TO_COLLABORATOR("Agent chuyển sang CTV"),
    SALE_MANAGER_REQUESTS("Sales Manager yêu cầu"),
    SYSTEM_ISSUE("Lỗi hệ thống"),
    AUTO_ASSIGNED_FAILED("Không tìm được agent phù hợp"),
    OTHER("Khác");

    private String label;

    RevokedType(String label) {
        this.label = label;
    }
}
