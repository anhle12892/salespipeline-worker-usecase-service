package vn.onehousing.salespipelie.usecase.worker.business.usecase.revokeleadoppafterstopuc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.UpdateSalesPipelineLeadData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalespipelineAggregateView;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadRevokedEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class RevokeLeadOppAfterStopUc implements IRevokeLeadOppAfterStopUc {

    private final EventPublisher eventPublisher;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;

    @Override
    public void process(String leadUuid, String revokedType, String revokedReason) {
        SalespipelineAggregateView salespipelineAggregateView = salesPipelineUsecaseService.getByLeadUuid(leadUuid);
        List<User> assignedUsers = salespipelineAggregateView.getAssignedUsers();
        if (CollectionUtils.isEmpty(assignedUsers)) {
            assignedUsers = Collections.singletonList(
                User.builder()
                    .userUuid(salespipelineAggregateView.getAssignedUserUuid())
                    .userCode(salespipelineAggregateView.getAssignedUserCode())
                    .build()
            );
        }

        UpdateSalesPipelineLeadData salesPipelineLeadData = UpdateSalesPipelineLeadData.builder()
            .unassignedReason(revokedType)
            .assignedUserUuid("")
            .assignedUserCode("")
            .assignedUsers(new ArrayList<>())
            .build();
        salesPipelineUsecaseService.updateLead(leadUuid, salesPipelineLeadData);

        for (User user : assignedUsers) {
            eventPublisher.publishEvent(
                LeadRevokedEvent.builder()
                    .leadUuid(salespipelineAggregateView.getLeadUuid())
                    .opportunityUuid(salespipelineAggregateView.getOpportunityUuid())
                    .revokedReason(revokedReason)
                    .revokedType(revokedType)
                    .channel(salespipelineAggregateView.getChannel())
                    .source(salespipelineAggregateView.getSource())
                    .revokedUserUuid(user.getUserUuid())
                    .build());
        }

    }
}
