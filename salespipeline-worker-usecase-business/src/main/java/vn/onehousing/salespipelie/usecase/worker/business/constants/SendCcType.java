package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class SendCcType {
    public static final String PRE_QUALIFY  = "PRE_QUALIFY";
    public static final String RE_QUALIFY_SUCCESS  = "RE_QUALIFY_SUCCESS";
    public static final String RE_QUALIFY_STOP  = "RE_QUALIFY_STOP";
}
