package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class Opportunity {
    private Long opportunityId;
    private String opportunityUuid;
    private String opportunityCode;
    private String name;
    private String status;
    private String leadUuid;
    private String description;
    private String accountUuid;
    private String accountCode;
    private String productCode;
    private String productUuid;
    private String referredByUserUuid;
    private String referredByUserCode;
    private String assignedUserUuid;
    private String assignedUserCode;
    private String note;
    private String campaignUuid;
    private String campaignCode;
    private Instant closedDate;
    private String attributeSetValue;
    private String channel;
    private String source;
    @JsonProperty("is_sign_contract")
    private Boolean isSignContract;
    Instant lastModifiedDate;
    String createdBy;
    String lastModifiedBy;
    Instant createdDate;
    List<User> assignedUsers;
}
