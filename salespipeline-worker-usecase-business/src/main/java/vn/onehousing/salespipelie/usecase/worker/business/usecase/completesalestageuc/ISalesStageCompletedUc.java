package vn.onehousing.salespipelie.usecase.worker.business.usecase.completesalestageuc;

public interface ISalesStageCompletedUc {
    boolean process(String leadUuid, String stage) throws Exception;
}
