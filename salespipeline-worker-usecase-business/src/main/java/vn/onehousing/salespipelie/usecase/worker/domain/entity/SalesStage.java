package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.Data;

import java.util.List;

@Data
public class SalesStage {
    private String leadUuid;
    private String leadStage;
    private List<String> saleRoles;
}
