package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeleadstatusuc;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IChangeLeadStatusUc {
    void process(ChangeLeadStatusReq req) throws JsonProcessingException;
}
