package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.response;

import lombok.Builder;
import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;


@Data
@Builder
public class LeadServiceResponse extends BaseResponse<Lead> {
}
