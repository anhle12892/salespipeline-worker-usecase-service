package vn.onehousing.salespipelie.usecase.worker.business.usecase.checkleadduplicateduc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.ILeadUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones.DuplicationLeadResp;

@Service
@AllArgsConstructor
public class CheckLeadDuplicatedUc implements ICheckLeadDuplicatedUc {

    private final ILeadUsecaseService leadUsecaseService;

    @Override
    public DuplicationLeadResp process(String leadUuid) {

        var resp = leadUsecaseService.getListDuplication(leadUuid);

        if (resp.size() == 0 || resp == null)
            return null;

        return resp.get(0);
    }
}
