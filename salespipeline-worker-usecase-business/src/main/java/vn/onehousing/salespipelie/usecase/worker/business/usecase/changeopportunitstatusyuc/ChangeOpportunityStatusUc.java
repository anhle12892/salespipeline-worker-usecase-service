package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc;

import lombok.AllArgsConstructor;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.OpportunityStatus;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.IOpportunityService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.request.UpdateOpportunityReq;

import java.time.Instant;

@Service
@AllArgsConstructor
public class ChangeOpportunityStatusUc implements IChangeOpportunityStatusUc {

    private final IOpportunityService opportunityService;
    private final EventPublisher eventPublisher;

    @Override
    public void process(ChangeOpportunityStatusUcReq req) {

        var requestBuilder = UpdateOpportunityReq
            .builder()
            .status(req.getOpportunityStatus());

        if (req.getOpportunityStatus().equals(OpportunityStatus.STOP) ||
            req.getOpportunityStatus().equals(OpportunityStatus.SUCCESS)) {
            requestBuilder.closedDate(Instant.now());
        }


        opportunityService.update(req.getOpportunityUuid(), requestBuilder.build());

//        if (LeadStatus.REVOKED.equals(req.getOpportunityStatus())) {
//            String revokeType = req.getData().get(VariableName.REVOKED_TYPE).toString();
//            String revokeReason = req.getData().get(VariableName.REVOKED_REASON).toString();
//            String assignedUserUuid = req.getData().get(VariableName.LEAD_ASSIGNED_USER_UUID).toString();
//            String source = req.getData().get(VariableName.LEAD_SOURCE).toString();
//            String channel = req.getData().get(VariableName.LEAD_CHANNEL).toString();
//            eventPublisher.publishEvent(
//                LeadRevokedEvent.builder()
//                    .leadUuid(req.getLeadUuid())
//                    .opportunityUuid(req.getOpportunityUuid())
//                    .revokedUserUuid(assignedUserUuid)
//                    .revokedType(revokeType)
//                    .revokedReason(revokeReason)
//                    .source(source)
//                    .channel(channel)
//                    .build()
//            );
//        }
    }
}
