package vn.onehousing.salespipelie.usecase.worker.business.usertask.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;

import java.util.Map;

public interface IActivityHandler {
    Object handler(String userTask, Task task, Map<String,Object> data) throws Exception;
}
