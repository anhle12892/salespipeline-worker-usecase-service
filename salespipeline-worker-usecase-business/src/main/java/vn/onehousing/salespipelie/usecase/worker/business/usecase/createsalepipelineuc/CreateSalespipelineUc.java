package vn.onehousing.salespipelie.usecase.worker.business.usecase.createsalepipelineuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipeline;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;

@Service
@AllArgsConstructor
public class CreateSalespipelineUc implements ICreateSalespipelineUc {

    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final ISalesPipelineWorkflowProcessRepository repository;

    @Override
    public SalesPipeline process(CreateSalesPipelineUcReq req) {

        SalesPipeline resp = salesPipelineUsecaseService.createSalesPipeline(req.leadUuid);

        SalesPipelineWorkflowInstance processInstance = repository.getByLeadUuId(
                req.getLeadUuid()
        );

        if (processInstance == null)
            throw new ProcessInstanceNotFoundException(String.format("not found process instance of lead : [%s]", req.getLeadUuid()));

        processInstance.setSalespipelineUuid(resp.getUuid());
        repository.update(processInstance);

        return resp;
    }
}
