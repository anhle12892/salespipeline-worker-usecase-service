package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class LeadIdDocument {

    private Long id;
    @JsonProperty("id_document_type")
    private String idDocumentType;
    @JsonProperty("id_document_number")
    private String idDocumentNumber;
    @JsonProperty("id_document_issued_date")
    private Instant idDocumentIssuedDate;
    @JsonProperty("id_document_issued_place")
    private String idDocumentIssuedPlace;
}
