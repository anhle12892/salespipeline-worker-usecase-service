package vn.onehousing.salespipelie.usecase.worker.business.usertask;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.IAssignmentService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.assignleadtoagentuc.IAutoAssignLeadToAgentUc;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.InvalidDataException;

import java.util.*;

@Slf4j
@Service
@AllArgsConstructor
public class ManualAssignedAgentHandler implements IActivityHandler {

    private final ObjectMapper objectMapper;
    private final IWorkflowService workflowService;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final IAutoAssignLeadToAgentUc assignLeadToAgentUc;
    private final IAssignmentService assignmentService;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper, data);

        if (!data.containsKey("assigned_user_uuid") && !data.containsKey("assigned_users")) {
            throw new InvalidDataException("missing assigned_user_uuid");
        }
        String assignedUserUuid = (String) data.get("assigned_user_uuid");
        List<User> assignedByMultiUsers = objectMapper.convertValue(data.get("assigned_users"), new TypeReference<>() {
        });
        if (StringUtils.isEmpty(assignedUserUuid) && Objects.isNull(assignedByMultiUsers)) {
            throw new InvalidDataException("assigned_user_uuid is not allow to be null");
        }
        String assignedByUser = (String) data.get("assigned_by_user");
        List<User> assignedUsers = new ArrayList<>(Objects.nonNull(assignedByMultiUsers) ? assignedByMultiUsers
                : Collections.emptyList());
        if (assignedUsers.isEmpty()) {
            var userDto = assignmentService.getRolesByUserUuid(assignedUserUuid);
            var role = (Objects.nonNull(userDto) && !userDto.getRoles().isEmpty()) ? userDto.getRoles().get(0).getName() : "";
            assignedUsers.add(new User(assignedUserUuid, null, role));
        }
        completeTaskData.getData().setUnassignedReason("");
        assignLeadToAgentUc.processMultiAgent(completeTaskData.getLeadUuid(), null,
                Collections.emptyList(), assignedUsers, Collections.emptyList(), assignedByUser);
        var resp = salesPipelineUsecaseService.updateLead(completeTaskData.getLeadUuid(), completeTaskData.getData());

        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, completeTaskData.getLeadUuid()));
        variableMap.put(VariableName.LEAD_ASSIGNED_USER_UUID, new Variable(DataType.STRING, resp.getAssignedUserUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, userTask));
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        log.info("completeTask - IActivityHandler: [ManualAssignedAgentHandler] - Lead_UUID: [{}]", completeTaskData.getLeadUuid());
        return resp;
    }
}
