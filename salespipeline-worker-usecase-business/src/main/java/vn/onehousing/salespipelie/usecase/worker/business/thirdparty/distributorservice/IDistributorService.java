package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.distributorservice;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IDistributorService {
    void sendToCCForPreQualify(String leadUuid) throws JsonProcessingException;
    void sendToCCForReQualifySuccess(String leadUuid, String dropStatus) throws JsonProcessingException;
    void sendToCCForReQualifyStop(String leadUuid, String dropStatus) throws JsonProcessingException;
}
