package vn.onehousing.salespipelie.usecase.worker.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.vinid.core.event.AbstractEvent;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeadQualifyResponseEvent extends AbstractEvent {
    String leadUuid;
    Boolean isQualified;
}
