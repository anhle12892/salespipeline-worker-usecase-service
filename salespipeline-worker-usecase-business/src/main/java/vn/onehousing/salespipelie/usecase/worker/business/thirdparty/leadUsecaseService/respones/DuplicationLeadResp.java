package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.respones;

import lombok.Data;
import lombok.NoArgsConstructor;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadDuplicateStatus;

import java.util.List;

@Data
@NoArgsConstructor
public class DuplicationLeadResp {
    String leadUuid;
    String leadCode;
    List<Lead> data;
    LeadDuplicateStatus status;
}
