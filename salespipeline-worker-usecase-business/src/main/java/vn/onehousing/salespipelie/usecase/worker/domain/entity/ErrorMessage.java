package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class ErrorMessage {
    String id;
    String activity;
    int hashCode;
    Map<String,Object> message;
    Integer retryTimes;
    boolean isDeleted;
}
