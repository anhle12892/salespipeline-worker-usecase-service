package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.EvaluateReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.ProcessInstance;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.tracing.TracingUtils;

import java.util.HashMap;
import java.util.List;


public interface IWorkflowService {
    ProcessInstance start(String definitionKey, CompleteTaskReq req);

    List<Task> getListTaskByProcessInstanceId(String processInstanceId);

    ProcessInstance[] getListProcessInstanceByLeadUuid(String leadUuid);

    ProcessInstance[] getListProcessInstanceByBusinessKey(String businessKey);

    List<String> getSaleRoles(EvaluateReq req) throws Exception;

    void doCompleteTask(String taskId, CompleteTaskReq req) throws Exception;

    default void completeTask(String taskId, CompleteTaskReq req) throws Exception {
        TracingUtils.addTracingContextToVariables(req.getVariables());
        doCompleteTask(taskId, req);
    }

    default void completeTask(String taskId) throws Exception {
        doCompleteTask(taskId, new CompleteTaskReq(new HashMap<>()));
    }


}
