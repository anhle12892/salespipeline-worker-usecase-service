package vn.onehousing.salespipelie.usecase.worker.business.constants;

public class LeadType {
    public static final String LEAD = "LEAD";
    public static final String OPPORTUNITY = "OPPORTUNITY";
}
