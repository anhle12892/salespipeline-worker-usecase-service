package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
public class SalespipelineAggregateView {
    String type;

    //lead
    String name;
    PhoneNumber phoneNumber;
    String email;
    String leadUuid;
    String leadCode;
    String assignedUserUuid;
    String assignedUserCode;
    String campaignUuid;
    String campaignCode;
    String status;
    String referredByUserUuid;
    String referredByUserCode;
    String attributeSetUuid;
    BigDecimal score;
    String partnerLeadUuid;

    @JsonProperty("is_converted")
    Boolean isConverted;
    String priority;
    String unqualifiedReason;
    String leadProfileUuid;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String opportunityUuid;
    String opportunityCode;
    String attributeSetId;
    String source;
    String gender;
    String version;
    String duplicatedLeadUuid;
    String duplicatedLeadCode;
    Instant convertedDate;
    Address address;
    LeadProfile profile;
    Instant assignedDate;

    List<AttributeValue> attributes;

    String channel;
    String expectedAssignedUserCode;
    String expectedAssignUserUuid;
    List<LeadIdDocument> idDocuments;
    String note;
    String unassignedReason;

    //multi sales
    List<User> assignedUsers;
    List<User> expectedUsers;

    //opp
    String description;
    String productCode;
    String productUuid;
    Instant closedDate;
    String attributeSetValue;
    Boolean isSignContract;

    // audit
    Instant recordedDate;

    Instant lastModifiedDate;
    Instant createdDate;
    String createdBy;
    String lastModifiedBy;

    Instant salespipelineLastModifiedDate;
    Instant salespipelineCreatedDate;
    String salespipelineCreatedBy;
    String salespipelineLastModifiedBy;

    // salespipeline
    String salespipeline_uuid;
    String disqualifiedReason;
    String closedLostReason;
    String saleStage;
}
