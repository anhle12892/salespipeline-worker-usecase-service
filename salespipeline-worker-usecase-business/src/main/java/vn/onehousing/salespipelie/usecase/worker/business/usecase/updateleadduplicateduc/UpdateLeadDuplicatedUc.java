package vn.onehousing.salespipelie.usecase.worker.business.usecase.updateleadduplicateduc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request.UpdateLeadReq;

@Service
@AllArgsConstructor
public class UpdateLeadDuplicatedUc implements IUpdateLeadDuplicatedUc {

    private final ILeadService leadService;

    @Override
    public void process(String leadUuid, String duplicatedLeadUuid, String duplicatedLeadCode) {
        leadService.update(
                leadUuid,
                UpdateLeadReq
                        .builder()
                        .duplicatedLeadCode(duplicatedLeadCode)
                        .duplicatedLeadUuid(duplicatedLeadUuid)
                        .unqualifiedReason("DUPLICATED")
                        .build()
        );
    }
}
