package vn.onehousing.salespipelie.usecase.worker.business.usecase.changeopportunitstatusyuc;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@AllArgsConstructor
public class ChangeOpportunityStatusUcReq {
    @Getter
    String leadUuid;
    @Getter
    String opportunityUuid;
    @Getter
    String opportunityStatus;
    @Getter
    Map<String, Object> data;
}
