package vn.onehousing.salespipelie.usecase.worker.business.usecase.startsalespipelineworkflowuc;

import java.util.Map;

public interface IStartSalePipelineWorkflowUc {
    void process(Map<String,Object> data) throws Exception;
}
