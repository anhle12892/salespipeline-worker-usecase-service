package vn.onehousing.salespipelie.usecase.worker.business.usertask.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import vn.onehousing.salespipelie.usecase.worker.business.constants.DataType;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService.ISalesPipelineUsecaseService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.CompleteTaskReq;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.base.IActivityHandler;
import vn.onehousing.salespipelie.usecase.worker.business.usertask.model.CompleteTaskData;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadRevokedEvent;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class ContactCenterPreUnqualifiedLeadHandler implements IActivityHandler {

    private final ObjectMapper objectMapper;
    private final IWorkflowService workflowService;
    private final ISalesPipelineUsecaseService salesPipelineUsecaseService;
    private final EventPublisher eventPublisher;

    @Override
    public Object handler(String userTask, Task task, Map<String, Object> data) throws Exception {

        log.info("[ContactCenterPreUnqualifiedLeadHandler] body = {}", objectMapper.writeValueAsString(data));
        CompleteTaskData completeTaskData = new CompleteTaskData(objectMapper, data);
        var leadView = salesPipelineUsecaseService.getByLeadUuid(completeTaskData.getLeadUuid());

        Map<String, Variable> variableMap = new HashMap<>();
        variableMap.put(VariableName.LEAD_UUID, new Variable(DataType.STRING, completeTaskData.getLeadUuid()));
        variableMap.put(VariableName.USER_TASK, new Variable(DataType.STRING, TaskDefinitionKey.ACTIVITY_CC_PRE_UNQUALIFIED_LEAD));
        variableMap.put(VariableName.LEAD_STATUS, new Variable(DataType.STRING, completeTaskData.getData().getStatus()));

        if (StringUtils.hasText(completeTaskData.getData().getUnassignedReason())) {
            LeadRevokedEvent leadRevokedEvent = LeadRevokedEvent.builder()
                    .leadUuid(completeTaskData.getLeadUuid())
                    .opportunityUuid(leadView.getOpportunityUuid())
                    .revokedType(completeTaskData.getData().getUnassignedReason())
                    .build();
            eventPublisher.publishEvent(leadRevokedEvent);
        }
        workflowService.completeTask(task.getId(), new CompleteTaskReq(variableMap));
        log.info("completeTask - IActivityHandler: [ContactCenterUnqualifiedLeadHandler] - Lead_UUID: [{}]", completeTaskData.getLeadUuid());
        return null;
    }
}
