package vn.onehousing.salespipelie.usecase.worker.business.usecase.requestcheckduplicateuc;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IRequestCheckDuplicateUc {
    void process(String leadUuid,String duplicateRuleCode) throws JsonProcessingException;
}
