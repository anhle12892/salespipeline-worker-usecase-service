package vn.onehousing.salespipelie.usecase.worker.business.usecase.revokedlead;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Revoke;

public interface IAutoRevokedLeadUsecase {
    Revoke revokedLead(String leadUuid) throws JsonProcessingException;
}
