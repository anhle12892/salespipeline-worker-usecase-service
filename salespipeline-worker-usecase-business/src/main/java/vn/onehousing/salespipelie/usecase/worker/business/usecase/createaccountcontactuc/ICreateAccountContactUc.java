package vn.onehousing.salespipelie.usecase.worker.business.usecase.createaccountcontactuc;

import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;

public interface ICreateAccountContactUc {
    SalesPipelineWorkflowInstance process(String leadUuid, String salesPipelineUuid);
}
