package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.salesPipelineUsecaseService;

import lombok.Data;

@Data
public class CreateAccountResp {
    String id;
    String uuid;
    String leadUuid;
    String leadCode;
    String accountUuid;
    String accountCode;
    String contactUuid;
    String contactCode;
    String campaignUuid;
    String campaignCode;
    String opportunityUuid;
    String opportunityCode;
}
