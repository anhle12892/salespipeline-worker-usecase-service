package vn.onehousing.salespipelie.usecase.worker.business.usecase.revokedlead;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import vn.onehousing.salespipelie.usecase.worker.business.constants.RevokeStatus;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.IAssignmentService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.assignmentservice.response.RevokedDataDto;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.ILeadService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadService.request.UpdateLeadReq;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Revoke;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.User;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadRevokedEvent;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static vn.onehousing.salespipelie.usecase.worker.business.constants.RevokedType.OPP_OVERDUE_CC_PENDING;

@Slf4j
@Service
@AllArgsConstructor
public class AutoRevokedLeadUsecase implements IAutoRevokedLeadUsecase {

    private final ILeadService leadService;
    private final IAssignmentService assignmentService;
    private final EventPublisher eventPublisher;

    @Override
    public Revoke revokedLead(String leadUUid) {
        List<RevokedDataDto> revokedDataList = assignmentService.revoke(leadUUid);
        if (revokedDataList == null || revokedDataList.isEmpty()) {
            return new Revoke(false, "", "", "");
        }

        /* a Lead contains multiple agents, when those agents are all hit deadline, the lead will mark as revoked */
        var isRevoked = isAllAgentsHitDeadline(revokedDataList);
        if (isRevoked) {
            String revokedType = revokedDataList.get(0).getRevokedType();
            String revokedReason = revokedDataList.get(0).getRevokedReason();

            if (!StringUtils.hasText(revokedType)) {
                revokedType = OPP_OVERDUE_CC_PENDING.name();
            }
            if (!StringUtils.hasText(revokedReason)) {
                revokedReason = OPP_OVERDUE_CC_PENDING.getLabel();
            }

            Lead lead = leadService.getByUuid(leadUUid);
            List<User> assignedUsers = lead.getAssignedUsers();
            if (CollectionUtils.isEmpty(assignedUsers)) {
                assignedUsers = Collections.singletonList(
                    User.builder()
                        .userUuid(lead.getAssignedUserUuid())
                        .userCode(lead.getAssignedUserCode())
                        .build()
                );
            }
            leadService.update(leadUUid, UpdateLeadReq.builder()
                .assignedUserUuid("")
                .assignedUserCode("")
                .assignedUsers(new ArrayList<>())
                .unassignedReason(revokedType)
                .build());
            String dropStatus = lead.getStatus();

            for (User user : assignedUsers) {
                eventPublisher.publishEvent(
                    LeadRevokedEvent.builder()
                        .leadUuid(lead.getLeadUuid())
                        .revokedReason(revokedReason)
                        .revokedType(revokedType)
                        .channel(lead.getChannel())
                        .source(lead.getSource())
                        .revokedUserUuid(user.getUserUuid())
                        .build());
            }

            return new Revoke(true, revokedType, revokedReason, dropStatus);
        }

        return new Revoke(false, "", "", "");
    }

    private boolean isAllAgentsHitDeadline(List<RevokedDataDto> revokedDataList) {
        if (revokedDataList == null || revokedDataList.isEmpty())
            return false;

        for (RevokedDataDto revokedDataDto : revokedDataList) {
            if (!RevokeStatus.REVOKEABLE.equals(revokedDataDto.getStatus())
                || !Instant.now().isAfter(revokedDataDto.getDeadline()))
                return false;
        }
        return true;
    }
}
