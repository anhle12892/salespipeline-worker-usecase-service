package vn.onehousing.salespipelie.usecase.worker.domain.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.Valid;
import java.util.List;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumber {
    String phoneType;

    String number;

    @Valid
    List<AdditionalPhone> additionalPhones;
}
