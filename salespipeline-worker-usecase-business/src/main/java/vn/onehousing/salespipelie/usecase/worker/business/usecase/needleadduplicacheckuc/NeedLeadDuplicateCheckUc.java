package vn.onehousing.salespipelie.usecase.worker.business.usecase.needleadduplicacheckuc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.leadUsecaseService.ILeadUsecaseService;

@Service
@AllArgsConstructor
public class NeedLeadDuplicateCheckUc implements INeedLeadDuplicateCheckUc {

    private final ILeadUsecaseService leadUsecaseService;

    @Override
    public boolean process(String leadUuid) {
        var res = leadUsecaseService.needDuplicateChecking(leadUuid);
        return res.isNeedCheck();
    }
}
