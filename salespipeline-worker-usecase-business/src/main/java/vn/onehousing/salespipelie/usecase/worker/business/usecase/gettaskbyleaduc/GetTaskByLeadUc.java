package vn.onehousing.salespipelie.usecase.worker.business.usecase.gettaskbyleaduc;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.IWorkflowService;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.ProcessInstance;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Task;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.changeprocessinstanceiduc.IChangeProcessInstanceId;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.ISalesPipelineWorkflowProcessRepository;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.SalesPipelineWorkflowInstance;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.ProcessInstanceNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class GetTaskByLeadUc implements IGetTaskByLeadUc {

    private final IWorkflowService workflowService;
    private final IChangeProcessInstanceId changeProcessInstanceId;
    private final ISalesPipelineWorkflowProcessRepository repository;

    @Override
    public Task process(String leadUuid, String activity) {

        SalesPipelineWorkflowInstance salesPipelineWorkflowInstance = repository.getByLeadUuId(leadUuid);
        if (salesPipelineWorkflowInstance == null) {
            throw new ProcessInstanceNotFoundException(
                    String.format("not found SalesPipelineWorkflowInstance with lead_uuid: [%s]", leadUuid)
            );
        }

        final String processInstanceId = salesPipelineWorkflowInstance.getProcessInstanceId();
        List<Task> tasks = workflowService.getListTaskByProcessInstanceId(processInstanceId);
        if (!tasks.isEmpty()) {
            log.info("Found {} tasks of process instance {} for lead_uuid {}", tasks, processInstanceId, leadUuid);
        }

        Task task = getTaskInList(activity, tasks);
        if (task != null) {
            return task;
        }

        ProcessInstance[] allProcessInstance = workflowService.getListProcessInstanceByLeadUuid(leadUuid);
        if (allProcessInstance == null || allProcessInstance.length == 0) {
            return null;
        }

        tasks = new ArrayList<>();

        for (ProcessInstance processInstance : allProcessInstance) {

            List<Task> taskResult = workflowService.getListTaskByProcessInstanceId(
                    processInstance.getId()
            );
            if (taskResult == null || taskResult.size() == 0) {
                continue;
            }
            tasks.addAll(taskResult);
        }

        log.info(
                "GetTasksByLeadUc.process - lead_uuid: [{}] - task size: [{}], tasks [{}]",
                leadUuid,
                tasks.size(),
                tasks
        );

        task = getTaskInList(activity, tasks);
        if (task == null) {
            return null;
        }
        changeProcessInstanceId.process(
                salesPipelineWorkflowInstance.getId(),
                task.getProcessInstanceId()
        );
        return task;
    }

    private Task getTaskInList(String activity, List<Task> tasks) {
        for (Task t : tasks) {
            if (!t.getTaskDefinitionKey().equals(activity)) {
                continue;
            }
            return t;
        }
        return null;
    }
}
