package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.workflowService.model.Variable;

import java.util.Map;

@Data
@Builder
@JsonNaming()
public class StartMessageReq {
    String messageName;
    String businessKey;
    String processInstanceId;
    boolean resultEnabled = true;
    Map<String, Variable> processVariables;
}
