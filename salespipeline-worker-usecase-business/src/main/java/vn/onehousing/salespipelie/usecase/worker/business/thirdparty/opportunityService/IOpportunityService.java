package vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService;

import vn.onehousing.salespipelie.usecase.worker.business.thirdparty.opportunityService.request.UpdateOpportunityReq;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Opportunity;

public interface IOpportunityService {
    void update(String uuid, UpdateOpportunityReq req);

    Opportunity getByUuid(String uuid);
    Opportunity getByLeadUuid(String leadUuid);
}
