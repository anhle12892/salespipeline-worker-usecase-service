#!/usr/bin/env bash
if [[ -z $IMAGE_TAG ]]; then
  echo "IMAGE_TAG is empty"
  exit 1
fi

if [[ "$CI_COMMIT_TAG" != "" ]]; then
      export IMAGE_TAG=$CI_COMMIT_TAG
fi

envsubst < config.$ENVIRONMENT.yml > k8s-config.yml && kubectl apply -f k8s-config.yml --namespace=${NAMESPACE}
if [ -f "main.$ENVIRONMENT.yml" ]; then
    envsubst < main.$ENVIRONMENT.yml > k8s-main.yml && kubectl apply -f k8s-main.yml --namespace=${NAMESPACE}
else
    envsubst < main.yml > k8s-main.yml && kubectl apply -f k8s-main.yml --namespace=${NAMESPACE}
fi

if [[ $? != 0 ]]; then exit 1; fi

kubectl rollout status deployments/$SERVICE_NAME --namespace=${NAMESPACE}

if [[ $? != 0 ]]; then
    kubectl logs --namespace=${NAMESPACE} $(kubectl get pods --sort-by=.metadata.creationTimestamp --namespace=${NAMESPACE} | grep "$SERVICE_NAME" | awk '{print $1}' | tac | head -1 ) --tail=20 && exit 1;
fi