package vn.onehousing.salespipelie.usecase.worker.application.event;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LeadLite {

    String leadId;

    String leadUuid;

    String leadCode;

    String assignedUserUuid;

    String assignedUserCode;

    String status;

    Instant lastModifiedDate;
}
