package vn.onehousing.salespipelie.usecase.worker.application.event;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import net.vinid.core.event.AbstractEvent;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadDuplicateStatus;

import java.util.List;

@ToString(callSuper = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class LeadCheckDuplicateResponseEvent extends AbstractEvent {
    String leadUuid;
    List<LeadLite> data;
    LeadDuplicateStatus status;
}
