package vn.onehousing.salespipelie.usecase.worker.application;

import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventPublisher;
import org.assertj.core.util.Lists;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import vn.onehousing.salespipelie.usecase.worker.application.event.LeadCheckDuplicateResponseEvent;
import vn.onehousing.salespipelie.usecase.worker.application.event.LeadLite;
import vn.onehousing.salespipelie.usecase.worker.business.constants.LeadDuplicateStatus;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.camundaService.CamundaService;

import java.io.IOException;
import java.time.Instant;

@Slf4j
@Ignore
public class CheckLeadDuplicateEventHandlerIT extends IntegrationTest {

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private RestHighLevelClient esClient;

    @Autowired
    private CamundaService camundaService;

    @Before
    public void cleanDb() throws IOException {
//        final var deleteRequest = new DeleteByQueryRequest();
//        deleteRequest.setQuery(QueryBuilders.matchAllQuery());
//        deleteRequest.indices(index);
//        deleteRequest.setRefresh(true);
//        esClient.deleteByQuery(deleteRequest, DEFAULT);
    }

    @Test
    public void sendLeadCheckDuplicateResponseEvent() throws InterruptedException {
        // given
        final LeadCheckDuplicateResponseEvent event = new LeadCheckDuplicateResponseEvent();
        event.setLeadUuid("123456789");
        event.setStatus(LeadDuplicateStatus.LEAD_DUPLICATED);
        final LeadLite leadLite = new LeadLite();
        leadLite.setStatus("CC_QUALIFIED");
        leadLite.setLeadUuid("987654321");
        leadLite.setLeadCode("987654321");
        leadLite.setLeadId("987654321");
        leadLite.setLastModifiedDate(Instant.now().minusSeconds(5));
        event.setData(Lists.newArrayList(leadLite));

        // when
        eventPublisher.publishEvent(event);

        //

        Thread.sleep(50000);
    }

}
