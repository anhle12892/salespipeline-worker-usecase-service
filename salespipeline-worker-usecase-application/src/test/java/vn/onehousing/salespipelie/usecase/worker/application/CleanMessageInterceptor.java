package vn.onehousing.salespipelie.usecase.worker.application;

import lombok.Data;
import net.vinid.messagebus.kafka.consumer.KafkaMessageInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;

@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Component
public class CleanMessageInterceptor implements KafkaMessageInterceptor {

    private boolean ignore = true;

    @Override
    public void doBeforeHandle(Message<?> message, @Nullable ConsumerRecord<?, ?> consumerRecord, @Nullable String groupId) {
        if (ignore) {
            throw new IgnoredMessageException(consumerRecord.offset());
        }
    }

    @Override
    public void afterHandle(Message<?> message, @Nullable ConsumerRecord<?, ?> consumerRecord, @Nullable String groupId, @Nullable Exception exception) {

    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    @Data
    public static class IgnoredMessageException extends RuntimeException {
        public IgnoredMessageException(long offset) {
            super(String.format("Ignore message with offset %s", offset));
        }
    }
}
