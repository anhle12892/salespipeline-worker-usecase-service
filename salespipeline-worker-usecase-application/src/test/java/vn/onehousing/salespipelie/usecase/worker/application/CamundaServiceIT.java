package vn.onehousing.salespipelie.usecase.worker.application;

import com.github.tomakehurst.wiremock.client.WireMock;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import vn.onehousing.salespipelie.usecase.worker.infrastructure.adapter.thirdparty.camundaService.CamundaService;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.CannotFindTaskException;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.RequestThirdPartyException;

@Slf4j
public class CamundaServiceIT extends IntegrationTest {

    @Autowired
    private CamundaService camundaService;

    @Test
    public void complete_task_must_return_ok() throws Exception {
        wireMockRule.stubFor(WireMock.post(WireMock.urlEqualTo("/salespipeline-worker-usecase-workflow/engine-rest/task/864e26b3-3b00-11ec-858b-fade12f39097/complete"))
                .willReturn(WireMock.aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withStatus(204)
                ));
        camundaService.completeTask("864e26b3-3b00-11ec-858b-fade12f39097");
    }

    @Test
    public void complete_task_must_return_fail_with_task_not_found_exception() throws Exception {
        wireMockRule.stubFor(WireMock.post(WireMock.urlEqualTo("/salespipeline-worker-usecase-workflow/engine-rest/task/864e26b3-3b00-11ec-858b-fade12f39097/complete"))
                .willReturn(WireMock.aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withStatus(500)
                        .withBody("{\n" +
                                "    \"type\": \"RestException\",\n" +
                                "    \"message\": \"Cannot complete task 864e26b3-3b00-11ec-858b-fade12f39097: Cannot find task with id 864e26b3-3b00-11ec-858b-fade12f39097: task is null\"\n" +
                                "}")
                ));
        try {
            camundaService.completeTask("864e26b3-3b00-11ec-858b-fade12f39097");
            Assertions.fail("Must throws CannotFindTaskException");
        } catch (CannotFindTaskException e) {
            //
        }
    }

    @Test
    public void complete_task_must_return_fail_with_other_error() throws Exception {
        wireMockRule.stubFor(WireMock.post(WireMock.urlEqualTo("/salespipeline-worker-usecase-workflow/engine-rest/task/864e26b3-3b00-11ec-858b-fade12f39097/complete"))
                .willReturn(WireMock.aResponse()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withStatus(500)
                        .withBody("{\n" +
                                "    \"type\": \"RestException\",\n" +
                                "    \"message\": \"Another error\"\n" +
                                "}")
                ));
        try {
            camundaService.completeTask("864e26b3-3b00-11ec-858b-fade12f39097");
            Assertions.fail("Must throws RequestThirdPartyException");
        } catch (RequestThirdPartyException e) {
            //
        }
    }
}
