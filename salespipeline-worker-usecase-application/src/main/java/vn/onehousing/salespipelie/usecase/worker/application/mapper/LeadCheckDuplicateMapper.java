package vn.onehousing.salespipelie.usecase.worker.application.mapper;

import org.mapstruct.Mapper;
import vn.onehousing.salespipelie.usecase.worker.domain.entity.Lead;
import vn.onehousing.salespipelie.usecase.worker.domain.event.LeadCheckDuplicateMessage;

@Mapper(componentModel = "spring")
public interface LeadCheckDuplicateMapper {
    LeadCheckDuplicateMessage from(Lead lead);
}
