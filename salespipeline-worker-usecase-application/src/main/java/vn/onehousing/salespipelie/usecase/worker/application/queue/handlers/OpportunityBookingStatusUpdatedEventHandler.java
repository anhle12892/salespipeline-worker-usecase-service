package vn.onehousing.salespipelie.usecase.worker.application.queue.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.bookingrecieveduc.IBookingReceivedUc;
import vn.onehousing.salespipelie.usecase.worker.domain.event.OpportunityBookingStatusUpdatedEvent;

@Slf4j
@Component
@AllArgsConstructor
public class OpportunityBookingStatusUpdatedEventHandler {

    private final ObjectMapper mapper;
    private final IBookingReceivedUc bookingReceivedUc;

    @KafkaListener(
        topics = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.topic}",
        autoStartup = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.enable}",
        groupId = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.groupId}",
        concurrency = "#{OpportunityBookingStatusUpdatedEvent_TopicListener.concurrency}"
    )
    public void handle(final EventMessage message) throws Exception {
        log.info("[OpportunityBookingStatusUpdatedEventHandler] receive message {}", mapper.writeValueAsString(message));
        OpportunityBookingStatusUpdatedEvent data = mapper.convertValue(message.getPayload(), OpportunityBookingStatusUpdatedEvent.class);
        bookingReceivedUc.process(data.getLeadUuid(), data.getOpportunityStatus(), data.getActivity());
    }
}
