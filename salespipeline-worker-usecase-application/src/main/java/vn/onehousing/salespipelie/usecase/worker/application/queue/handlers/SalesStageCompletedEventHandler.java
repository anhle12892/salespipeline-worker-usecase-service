package vn.onehousing.salespipelie.usecase.worker.application.queue.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completesalestageuc.ISalesStageCompletedUc;
import vn.onehousing.salespipelie.usecase.worker.domain.event.StartSalesStageEvent;

@Slf4j
@Component
public class SalesStageCompletedEventHandler {
    private final ISalesStageCompletedUc salesStageCompletedUc;
    private final ObjectMapper mapper;

    public SalesStageCompletedEventHandler(ISalesStageCompletedUc salesStageCompletedUc, ObjectMapper mapper) {
        this.salesStageCompletedUc = salesStageCompletedUc;
        this.mapper = mapper;
    }

    @KafkaListener(
            topics = "#{StartSalesStageEvent_TopicListener.topic}",
            autoStartup = "#{StartSalesStageEvent_TopicListener.enable}",
            groupId = "#{StartSalesStageEvent_TopicListener.groupId}",
            concurrency = "#{StartSalesStageEvent_TopicListener.concurrency}"
    )
    public void handle(final EventMessage message) throws Exception {
        log.info("[SalesStageCompletedEvent] receive message {}", mapper.writeValueAsString(message));
        StartSalesStageEvent stageCompletedEvent = mapper.convertValue(message.getPayload(), StartSalesStageEvent.class);
        salesStageCompletedUc.process(stageCompletedEvent.getLeadUuid(), stageCompletedEvent.getStage());
    }
}
