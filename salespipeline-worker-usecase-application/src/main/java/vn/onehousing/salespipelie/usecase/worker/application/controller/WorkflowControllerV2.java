package vn.onehousing.salespipelie.usecase.worker.application.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.v2.ICompleteUserTaskV2Uc;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/v2")
public class WorkflowControllerV2 {

    private final ICompleteUserTaskV2Uc completeUserTaskUc;

    @PutMapping("/workflow/leads/{leadUuid}/activities/{activity}/completed")
    public BaseResponse completeTask(
            @PathVariable("leadUuid") String leadUuid,
            @PathVariable("activity") String activity,
            @RequestBody Map<String, Object> request
    ) throws Exception {
        request.put(VariableName.LEAD_UUID, leadUuid);
        return BaseResponse.ofSucceeded(completeUserTaskUc.process(activity, request));
    }

}
