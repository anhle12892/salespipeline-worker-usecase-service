package vn.onehousing.salespipelie.usecase.worker.application.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.onehousing.salespipelie.usecase.worker.business.constants.VariableName;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.ICompleteUserTaskUc;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;

import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/v1")
public class WorkflowController {

    private final ICompleteUserTaskUc completeUserTaskUc;

    @PutMapping("/workflow/leads/{leadUuid}/activities/{activity}/completed")
    public BaseResponse completeTask(
            @PathVariable("leadUuid") String leadUuid,
            @PathVariable("activity") String activity,
            @RequestBody Map<String, Object> request
    ) throws Exception {
        request.put(VariableName.LEAD_UUID, leadUuid);
        return BaseResponse.ofSucceeded(completeUserTaskUc.process(activity, request));
    }

}
