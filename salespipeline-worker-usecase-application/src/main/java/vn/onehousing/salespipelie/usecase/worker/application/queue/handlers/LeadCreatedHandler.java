package vn.onehousing.salespipelie.usecase.worker.application.queue.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.startsalespipelineworkflowuc.IStartSalePipelineWorkflowUc;

@Slf4j
@Component
public class LeadCreatedHandler {

    private final IStartSalePipelineWorkflowUc startSalePipelineWorkflowUc;
    private final ObjectMapper mapper;

    public LeadCreatedHandler(IStartSalePipelineWorkflowUc startSalePipelineWorkflowUc, ObjectMapper mapper) {
        this.startSalePipelineWorkflowUc = startSalePipelineWorkflowUc;
        this.mapper = mapper;
    }

    @KafkaListener(
            topics = "#{LeadCreatedEvent_TopicListener.topic}",
            autoStartup = "#{LeadCreatedEvent_TopicListener.enable}",
            groupId = "#{LeadCreatedEvent_TopicListener.groupId}",
            concurrency = "#{LeadCreatedEvent_TopicListener.concurrency}"
    )
    public void handle(final EventMessage message) throws Exception {
        log.info("[LeadCreatedHandler] receive message {}", mapper.writeValueAsString(message));
        startSalePipelineWorkflowUc.process(
               message.getPayload()
        );
    }

}
