package vn.onehousing.salespipelie.usecase.worker.application.queue.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.event.EventMessage;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import vn.onehousing.salespipelie.usecase.worker.business.constants.TaskDefinitionKey;
import vn.onehousing.salespipelie.usecase.worker.business.usecase.completeusertaskuc.ICompleteUserTaskUc;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.CannotFindTaskException;

@Slf4j
@Component
public class CheckLeadDuplicateEventHandler {

    private final ObjectMapper mapper;
    private final ICompleteUserTaskUc completeUserTaskUc;

    public CheckLeadDuplicateEventHandler(ObjectMapper mapper, ICompleteUserTaskUc completeUserTaskUc) {
        this.mapper = mapper;
        this.completeUserTaskUc = completeUserTaskUc;
    }

    @KafkaListener(
            topics = "#{LeadCheckDuplicateResponseEvent_TopicListener.topic}",
            autoStartup = "#{LeadCheckDuplicateResponseEvent_TopicListener.enable}",
            groupId = "#{LeadCheckDuplicateResponseEvent_TopicListener.groupId}",
            concurrency = "#{LeadCheckDuplicateResponseEvent_TopicListener.concurrency}"
    )
    @Retryable(
            include = { CannotFindTaskException.class },
            maxAttemptsExpression = "${workflow.duplicate.retry.maxAttempts:5}",
            backoff = @Backoff(delayExpression = "${workflow.duplicate.retry.maxDelay:2000}")
    )
    public void handle(final EventMessage message) throws Exception {
        log.info("[CheckLeadDuplicateEventHandler] receive message {}", mapper.writeValueAsString(message));
        String activity = TaskDefinitionKey.ACTIVITY_CHECK_DUPLICATE_RESPONSE;
        completeUserTaskUc.process(activity, message.getPayload());
    }
}
