package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class CannotFindTaskException extends HousingException {
    public CannotFindTaskException(String message) {
        super(HousingErrors.CANNOT_FIND_TASK_TO_COMPLETE, message);
    }
}
