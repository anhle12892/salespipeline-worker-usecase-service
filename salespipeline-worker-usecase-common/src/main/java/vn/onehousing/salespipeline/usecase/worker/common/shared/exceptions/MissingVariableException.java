package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class MissingVariableException extends HousingException {
    public MissingVariableException(String message) {
        super(HousingErrors.MISSING_VARIABLE, message);
    }
}
