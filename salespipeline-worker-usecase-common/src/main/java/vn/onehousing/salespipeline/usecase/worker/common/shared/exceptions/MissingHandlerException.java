package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class MissingHandlerException extends HousingException {
    public MissingHandlerException(String message) {
        super(HousingErrors.MISSING_HANDLER, message);
    }
}
