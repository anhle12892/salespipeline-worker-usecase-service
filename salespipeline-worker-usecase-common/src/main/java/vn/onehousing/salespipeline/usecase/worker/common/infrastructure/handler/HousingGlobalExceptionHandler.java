package vn.onehousing.salespipeline.usecase.worker.common.infrastructure.handler;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import net.vinid.core.web.exception.ControllerAdviceWebExceptionHandler;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.annotation.Order;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ServerWebInputException;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.contant.RequestHeaderConstant;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.BaseResponse;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest.FieldViolation;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.HousingBusinessError;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.HousingErrors;
import vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions.HousingException;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;


@Slf4j
@Order(9)
@RestControllerAdvice
public class HousingGlobalExceptionHandler implements ControllerAdviceWebExceptionHandler {
    private Optional<BuildProperties> buildProperties;
    private final ObjectMapper objectMapper;

    @Autowired
    public HousingGlobalExceptionHandler(Optional<BuildProperties> buildProperties, ObjectMapper objectMapper) {
        this.buildProperties = buildProperties;
        this.objectMapper = objectMapper;
    }

    @ExceptionHandler(HousingException.class)
    public ResponseEntity<BaseResponse<Void>> handleBusinessException(HousingException exception, WebRequest request) {
        var data = this.getFailedResponse(request, exception);
        return this.getResponseEntity(request, exception.getErrorCode(), data, exception);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<BaseResponse<Void>> handleBusinessException(MethodArgumentNotValidException exception, WebRequest request) {
        var errors = this.getFieldViolations(exception.getBindingResult());
        var errorCode = HousingErrors.INVALID_PARAMETERS;
        var errorMessage = "Invalid parameters of object: " + exception.getBindingResult().getObjectName();
        var data = this.getFailedResponse(request, errorCode, errorMessage, errors);

        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(CompletionException.class)
    public ResponseEntity<BaseResponse<Void>> handleCompletionException(HttpMessageNotReadableException exception, WebRequest request) {
        var cause = exception.getCause();
        if (cause instanceof HousingException) {
            return handleBusinessException((HousingException) cause, request);
        }
        return handleException((Exception) cause, request);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<BaseResponse<Void>> handleHttpMessageNotReadableException(HttpMessageNotReadableException exception, WebRequest request) {
        var errorMessage = this.getNormalizeErrorMessage(exception);
        var errorCode = HousingErrors.INVALID_PARAMETERS;
        var data = this.getFailedResponse(request, errorCode, errorMessage);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<BaseResponse<Void>> handleHttpMessageNotReadableException(HttpRequestMethodNotSupportedException exception, WebRequest request) {
        var errorCode = HousingErrors.METHOD_NOT_ALLOWED;
        var data = this.getFailedResponse(request, errorCode);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<BaseResponse<Void>> handleHttpMessageNotReadableException(HttpMediaTypeNotSupportedException exception, WebRequest request) {
        var errorCode = HousingErrors.UNSUPPORTED_MEDIA_TYPE;
        var data = this.getFailedResponse(request, errorCode);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseResponse<Void>> handleException(Exception exception, WebRequest request) {
        var errorCode = HousingErrors.INTERNAL_SERVER_ERROR;
        var data = this.getFailedResponse(request, errorCode);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<BaseResponse<Void>> handleIllegalArgumentException(Exception exception, WebRequest request) {
        var errorCode = HousingErrors.INVALID_PARAMETERS;
        var data = this.getFailedResponse(request, errorCode);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<BaseResponse<Void>> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception, WebRequest request) {
        return handleBusinessException(resolve(exception), request);
    }

    private HousingException resolve(MethodArgumentTypeMismatchException exception) {
        return new HousingException(HousingErrors.INVALID_PARAMETERS, exception.getMessage(), exception);
    }

    private String getNormalizeErrorMessage(HttpMessageNotReadableException exception) {
        var errorMessage = exception.getMessage();
        if (errorMessage == null) return Strings.EMPTY;
        int idx = errorMessage.indexOf(':');
        return idx > 0 ? errorMessage.substring(0, idx) : errorMessage;
    }


    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<BaseResponse<Void>> handleAccessDeniedException(AccessDeniedException exception, WebRequest request) {
        var errorCode = HousingErrors.FORBIDDEN_ERROR;
        var data = this.getFailedResponse(request, errorCode);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<BaseResponse<Void>> handleException(WebExchangeBindException exception, WebRequest request) {
        var errors = this.getFieldViolations(exception.getBindingResult());
        var errorCode = HousingErrors.INVALID_PARAMETERS;
        var errorMessage = "Invalid parameters of object: " + exception.getBindingResult().getObjectName();
        var data = this.getFailedResponse(request, errorCode, errorMessage, errors);
        return this.getResponseEntity(request, errorCode, data, exception);

    }

    private List<FieldViolation> getFieldViolations(BindingResult bindingResult) {
        return bindingResult.getFieldErrors().stream()
                .map(e -> new FieldViolation(CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, e.getField()), e.getDefaultMessage()))
                .collect(Collectors.toList());
    }

    @ExceptionHandler(ServerWebInputException.class)
    public ResponseEntity<BaseResponse<Void>> handleException(ServerWebInputException exception, WebRequest request) {

        var errorCode = HousingErrors.INVALID_PARAMETERS;
        var data = this.getFailedResponse(request, errorCode, exception.getCause().getMessage());
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<BaseResponse<Void>> handleVConstraintViolationException(final ConstraintViolationException exception, WebRequest request) {
        var errorCode = HousingErrors.INVALID_PARAMETERS;
        var data = this.getFailedResponse(request, errorCode, exception.getMessage());
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    @ExceptionHandler(DecodingException.class)
    public ResponseEntity<BaseResponse<Void>> handleDecodingException(DecodingException exception,
                                                                      WebRequest request) {
        var errorCode = HousingErrors.MAX_FILE_SIZE;
        var data = this.getFailedResponse(request, errorCode);
        return this.getResponseEntity(request, errorCode, data, exception);
    }

    private ResponseEntity<BaseResponse<Void>> getResponseEntity(WebRequest request,
                                                                 HousingBusinessError errorCode,
                                                                 BaseResponse<Void> data,
                                                                 Throwable exception) {
        var status = errorCode.getHttpStatus();

        log.error("exception: {} | detail: {} | code: {} | message: {}", exception.getClass().getSimpleName(), data.getMeta(), status, request, exception);
        return new ResponseEntity<>(data, status);
    }

    private BaseResponse<Void> getFailedResponse(WebRequest request, HousingBusinessError errorCode, String errorMessage, List<FieldViolation> errors) {
        var data = BaseResponse.ofFailed(errorCode, errorMessage, errors);
        return this.addRequestId(request, data);
    }

    private BaseResponse<Void> getFailedResponse(WebRequest request, HousingBusinessError errorCode) {
        var data = BaseResponse.ofFailed(errorCode, errorCode.getMessage());
        return this.addRequestId(request, data);
    }

    private BaseResponse<Void> getFailedResponse(WebRequest request, HousingException exception) {
        var data = BaseResponse.ofFailed(exception.getErrorCode(), exception.getMessage());
        return this.addRequestId(request, data);
    }

    private BaseResponse<Void> getFailedResponse(WebRequest request, HousingBusinessError errorCode, String
            errorMessage) {
        var data = BaseResponse.ofFailed(errorCode, errorMessage);
        return this.addRequestId(request, data);
    }

    private BaseResponse<Void> addRequestId(WebRequest request, BaseResponse<Void> data) {
        data.getMeta().setRequestId(this.getRequestId(request));
        return data;
    }

    private String getRequestId(WebRequest request) {
        return request.getHeader(RequestHeaderConstant.X_REQUEST_ID);
    }

}
