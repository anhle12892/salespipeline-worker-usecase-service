package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class RequestThirdPartyException extends HousingException {

    public RequestThirdPartyException(String service, String action, String message) {
        super(
                HousingErrors.REQUEST_THIRDPARTY_EXCEPTION,
                String.format("Service: [%s] - Action: [%s] - Response: [%s]", service, action, message)
        );
    }
}
