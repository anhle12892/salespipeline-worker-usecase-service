package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class InvalidDataException  extends HousingException {
    public InvalidDataException(String message) {
        super(
            HousingErrors.INVALID_DATA,
            message
        );
    }
}
