package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class ProcessInstanceNotFoundException extends HousingException {
    public ProcessInstanceNotFoundException(String message) {
        super(
                HousingErrors.PROCESS_INSTANCE_NOT_FOUND_EXCEPTION,
                message
        );
    }
}
