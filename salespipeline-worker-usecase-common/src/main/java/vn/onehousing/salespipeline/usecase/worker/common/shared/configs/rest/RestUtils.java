package vn.onehousing.salespipeline.usecase.worker.common.shared.configs.rest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import vn.onehousing.salespipeline.usecase.worker.common.shared.configs.contant.RequestHeaderConstant;

import java.util.UUID;

public final class RestUtils {

    private RestUtils() {
    }

    public static HttpHeaders createHeadersWithBasicAuth(String username, String password) {
        var headers = new HttpHeaders();
        headers.setBasicAuth(username, password);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add(RequestHeaderConstant.X_REQUEST_ID, UUID.randomUUID().toString());
        return headers;
    }
}
