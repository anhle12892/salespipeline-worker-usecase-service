package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

public class LeadHaveBeenProcessedException extends HousingException {
    public LeadHaveBeenProcessedException(String message) {
        super(
                HousingErrors.LEAD_HAVE_BEEN_PROCESSED_EXCEPTION,
                message
        );
    }
}
