package vn.onehousing.salespipeline.usecase.worker.common.shared.exceptions;

import org.springframework.http.HttpStatus;


public class HousingErrors {

    /**
     * 400
     */
    public static final HousingBusinessError INVALID_PARAMETERS = new HousingBusinessError(400004, "Invalid parameters", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError MAX_FILE_SIZE = new HousingBusinessError(400, "File size exceeded the maximum size", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError SALE_PIPELINE_NOT_FOUND = new HousingBusinessError(400, "sale pipeline not found", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError REQUEST_THIRDPARTY_EXCEPTION = new HousingBusinessError(400, "error occur when request to thirdparty", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError PROCESS_INSTANCE_NOT_FOUND_EXCEPTION = new HousingBusinessError(400, "process not found exception", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError LEAD_HAVE_BEEN_PROCESSED_EXCEPTION = new HousingBusinessError(400, "lead have been process before", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError MISSING_VARIABLE = new HousingBusinessError(400, "missing variable", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError LEAD_NOT_EXIST = new HousingBusinessError(400002, "Lead is not exist", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError CANNOT_FIND_TASK_TO_COMPLETE = new HousingBusinessError(400003, "cannot find task to completed", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError MISSING_HANDLER = new HousingBusinessError(400005, "missing handler", HttpStatus.BAD_REQUEST);
    public static final HousingBusinessError INVALID_DATA = new HousingBusinessError(400, "invalid data", HttpStatus.BAD_REQUEST);

    /**
     * 401
     */
    public static final HousingBusinessError USER_NOT_UNAUTHORIZED = new HousingBusinessError(401, "User is unauthorized", HttpStatus.UNAUTHORIZED);

    /**
     * 403
     */
    public static final HousingBusinessError FORBIDDEN_ERROR = new HousingBusinessError(403003, "You don not have any permissions to access this resource", HttpStatus.FORBIDDEN);

    /**
     * 404
     */
    public static final HousingBusinessError STORAGE_OBJECT_NOT_FOUND = new HousingBusinessError(404001, "Storage Object is not found", HttpStatus.NOT_FOUND);

    /**
     * 405
     */
    public static final HousingBusinessError METHOD_NOT_ALLOWED = new HousingBusinessError(405, "Method Not Allowed", HttpStatus.METHOD_NOT_ALLOWED);

    /**
     * 415
     */
    public static final HousingBusinessError UNSUPPORTED_MEDIA_TYPE = new HousingBusinessError(415, "Unsupported Media Type", HttpStatus.UNSUPPORTED_MEDIA_TYPE);

    /**
     * 500
     */
    public static final HousingBusinessError INTERNAL_SERVER_ERROR = new HousingBusinessError(500002, "Internal server error", HttpStatus.INTERNAL_SERVER_ERROR);

    private HousingErrors() {
    }

}
